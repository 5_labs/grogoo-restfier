<?php

namespace GrogooRestfier\Helpers;

use Illuminate\Support\Facades\Hash;

class TestHelper
{

    public static function beforeEachDefault()
    {

        $company = \App\Modules\Company\Models\Company::factory()->create([
            'name' => 'Unit Tests Company'
        ]);

        $file = \App\Modules\Application\Models\File::factory()->create([
            'company_id' => $company->id
        ]);

        $profile = \App\Modules\Company\Models\Profile::factory()
            ->create(['name' => 'Testes Unitários', 'company_id' => $company->id])
            ->forceFill(['is_root' => true]);
        $profile->save();
        $profile->refresh();

        $user = \App\Modules\Company\Models\User::factory()->create([
            'name' => 'Tester PhpPest',
            'email' => 'tester-pest@local.com',
            'password' => Hash::make('1234567890'),
            'file_id' => $file->id
        ]);

        $company_user = \App\Modules\Company\Models\UserProfile::factory()->create([
            'user_id' => $user->id,
            'profile_id' => $profile->id
        ]);

        // Login to get token
        $modelLogin = [
            'email' => $user->email,
            'password' => '1234567890',
            'company_id' => $company->id
        ];

        $modelLogin = [
            'email' => $user->email,
            'password' => '1234567890',
            'company_id' => $company->id
        ];

        return [$company, $profile, $user, $company_user, $modelLogin];
    }

    public static function checkJsonError($response, $testName, $payload)
    {
        // See errors
        $error = $response->json('meta.error');
        if (false !== $error) {
            $error = [
                'test' => $testName,
                'payload' => $payload,
                'meta' => $response->json('meta')
            ];

            $error_string = json_encode($error, JSON_PRETTY_PRINT);
            
            throw new \Exception("Response Error: \n $error_string");
        }
    }
}
