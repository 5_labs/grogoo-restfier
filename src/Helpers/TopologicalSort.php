<?php

namespace GrogooRestfier\Helpers;

class TopologicalSort
{

    public static function sort($tables)
    {
        // // Exemplo de entrada
        // $tables = [
        //     "table1" => [],
        //     "table2" => ["table1"],
        //     "table3" => ["table1"],
        //     "table4" => ["table2", "table3"],
        // ];

        $graph = self::buildGraph($tables);
        return self::topologicalSort($graph);
    }

    private static function buildGraph(array $tables)
    {
        $graph = [];
        foreach ($tables as $table => $dependencies) {
            $graph[$table] = $dependencies;
        }
        return $graph;
    }

    private static function topologicalSort(array $graph)
    {
        $sorted = [];
        $visited = [];

        foreach (array_keys($graph) as $node) {
            self::visitNode($node, $graph, $sorted, $visited);
        }

        return $sorted;
    }

    private static function visitNode($node, array $graph, array &$sorted, array &$visited)
    {
        if (!isset($visited[$node])) {
            $visited[$node] = true;
            if (isset($graph[$node])) {
                foreach ($graph[$node] as $dependency) {
                    self::visitNode($dependency, $graph, $sorted, $visited);
                }
            }
            $sorted[] = $node;
        }
    }
}
