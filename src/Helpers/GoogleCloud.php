<?php

namespace GrogooRestfier\Helpers;

use Google\Cloud\Storage\StorageClient;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class GoogleCloud
{

    public static function setCors(
        array $method = ['GET', 'OPTIONS'],
        array $responseHeaders = ['Content-Type', 'Access-Control-Allow-Origin'],
        array $origin = ['*'],
        int $maxAge = 3600
    ) {
        if (!file_exists(env('GOOGLE_CLOUD_KEY_FILE', 'no-file-found'))) {
            throw new FileNotFoundException("Auth file to GCloud not found");
        }

        $storageClient = new StorageClient([
            'projectId' => env('GOOGLE_CLOUD_PROJECT_ID', 'no-project-defined'),
            'keyFilePath' => env('GOOGLE_CLOUD_KEY_FILE', 'no-file-found'),
        ]);
        $bucket = $storageClient->bucket(env('GOOGLE_CLOUD_STORAGE_BUCKET'));
        $bucket->update([
            'cors' => [
                [
                    'method' => $method,
                    'origin' => $origin,
                    'responseHeader' => $responseHeaders,
                    'maxAgeSeconds' => $maxAge,
                ],
            ],
        ]);
        return true;
    }
}
