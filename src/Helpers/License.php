<?php

namespace GrogooRestfier\Helpers;

use GrogooRestfier\Helpers\Crypto;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class License
{

    private $crypto;

    /**
     * Classe para criação e leiturade licenças codificadas
     * @param type $chave: String para codificação. Mínimo de 16 digitos.
     */
    public function __construct($chave)
    {
        $this->crypto = new Crypto($chave);
    }

    /**
     * Le o arquivo origem, codifica e salva no destino
     *
     * @param string $filenameOrigem
     * @param string $filenameDestino
     * @return void
     */
    public function create(string $filenameOrigem, string $filenameDestino)
    {
        $origem = realpath($filenameOrigem);
        $destino = $filenameDestino;

        if (!file_exists($filenameOrigem)) {
            throw new FileNotFoundException('File config not found!');
        }
        // carga do conteudo
        $content = file_get_contents($origem);
        $content = str_replace('<?php', '', $content);

        // crypto
        $pre = $this->crypto->getHash($content);
        // save
        $toSave = $this->crypto->encrypt($pre . $content);
        // return
        return Helper::saveFile($destino, $toSave, 'SOBREPOR');
    }


    /**
     * Le o arquivo de licenca e retorna decodificado
     *
     * @param [type] $licenseFile
     * @param boolean $dieIfNotExists
     * @return void
     */
    public function read($licenseFile, $dieIfNotExists = true)
    {
        if (!file_exists((string) $licenseFile)) {
            $filename = Helper::fileSearchRecursive($licenseFile, __DIR__, 20);
            if (!file_exists((string) $filename)) {
                if (!$dieIfNotExists) {
                    return null;
                }
                die("NsLicense: Arquivo não localizado: $licenseFile");
            }
            return $this->read($filename, $dieIfNotExists = true);
        }


        // decodificar config
        $string = file_get_contents((string) $licenseFile);
        $config = $this->crypto->decrypt((string) $string);
        $md5 = substr((string) $config, 0, 64);
        $code = substr((string) $config, 64);
        $pre = $this->crypto->getHash((string) $code);

        // validação que o código não foi alterado
        if ($pre !== $md5) {
            die('NSLicence: invalid or tampered file');
        }
        return $code;
    }

    public static function readFromIoncube($licenseName)
    {
        if (function_exists('ioncube_license_properties')) {
            return \ioncube_license_properties()[$licenseName]['value'];
        } else {
            die('Obrigatório utilização do Ioncube (NS88)');
        }
    }
}
