<?php

namespace GrogooRestfier\Helpers;

use GrogooRestfier\Helpers\Helper;

class UpdateEnvFile
{
    private $config = [];
    private $envFilePath;

    public function __construct($env = '.env')
    {
        $this->envFilePath = Helper::getPathApp() . '/' . $env;

        if (!file_exists($this->envFilePath)) {
            Helper::saveFile($this->envFilePath, '');
            chmod($this->envFilePath, 0777);
        }

        $this->config = file($this->envFilePath, FILE_IGNORE_NEW_LINES);
    }

    public function update(array $updated): void
    {
        $newContent = [];
        $lastKey = '';
        foreach ($updated as $key => $newValue) {

            $keyFound = false;
            // Verifica se newValue contém espaços ou é uma variável referenciada
            if (strpos($newValue, ' ') !== false || strpos($newValue, '$') !== false) {
                $newValue = '"' . $newValue . '"';
            }

            foreach ($this->config as $index => $line) {
                // Ignora linhas de comentários
                if (strpos(trim($line), '#') === 0 || empty(trim($line))) {
                    continue;
                }

                if (strpos($line, $key . '=') === 0) {
                    $this->config[$index] = $key . '=' . $newValue;
                    $keyFound = true;
                    break;
                }
            }

            if (!$keyFound) {
                $this->config[] = $key . '=' . $newValue;
            }
        }

        // adicionar uma linha entre as configurações parecidas
        $this->config = array_filter($this->config, fn ($item) => strlen($item) > 0);
        sort($this->config);
        $tempArray = [];
        $lastPrefix = '';
        foreach ($this->config as $item) {
            $prefix = explode('_', $item)[0];
            if ($lastPrefix !== '' && $prefix !== $lastPrefix) {
                $tempArray[] = "";
            }

            $tempArray[] = $item;
            $lastPrefix = $prefix;
        }

        $newContent = implode("\n", $tempArray);

        file_put_contents($this->envFilePath, $newContent);
    }
}
