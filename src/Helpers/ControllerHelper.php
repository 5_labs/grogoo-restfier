<?php

namespace GrogooRestfier\Helpers;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;

class ControllerHelper
{

    private Request $request;
    private string $table;
    private array $filterable;
    private array $conditions;
    private array $sortable;
    private int $routerID;

    public function __construct(Request $request, string &$table, array &$filterable, array &$conditions, array &$sortable)
    {
        $this->request = $request;
        $this->table = &$table;
        $this->filterable = &$filterable;
        $this->conditions = &$conditions;
        $this->sortable = &$sortable;
        $this->routerID = (int) $this->request->route(Helper::singularize($this->table));
    }


    public function setFiltrabled(): self
    {
        // only on the list route
        if ($this->request->method() === 'GET' && $this->routerID === 0) {
            $validator = Validator::make($this->request->all(), $this->filterable);
            if ($validator->fails()) {
                response()->json(['error' => $validator->errors()], 422);
            }
            $data = $validator->validated();

            foreach ($data as $key => $value) {
                $field = $this->table . '.' . $key;

                switch (true) {
                        // between
                    case null !== $this->request->input($key . '_end'):
                        $dataEnd = Validator::make($this->request->all(), [$key . '_end' => $this->filterable[$key]])->validated()[$key . '_end'] ?? null;
                        if ($dataEnd !== null) {
                            $this->conditions[$field] =  ['between', $value, $dataEnd];
                        }
                        break;
                        // date with no hour
                    case Helper::isDate($value):
                        $this->conditions[$field] =  ['date', '=', $value];
                        break;
                    default:
                        $this->conditions[$field] =  [($this->request->input($key . '_op') ?? '='), $value];
                }
            }
        }

        return $this;
    }

    public function setSearchabled(array $searchable): ?Closure
    {
        $search = null;

        // only on the list route
        if (
            $this->request->method() === 'GET'
            && $this->routerID === 0
            && null !== $this->request->input('search')
        ) {
            $search = $this->request->input('search');
            return function ($list) use ($search, $searchable) {
                foreach ($searchable as $index => $field) {
                    if ($index === 0) {
                        $list->where($this->table . ".$field", '~*', $search);
                    } else {
                        $list->orWhere($this->table . ".$field", '~*', $search);
                    }
                }
            };
        }


        return $search;
    }

    public function clearConditionsIfNull(): self
    {
        foreach ($this->conditions as $key => $val) {
            if (null === $val) {
                unset($this->conditions[$key]);
            }
        }

        return $this;
    }

    public function where(Builder $query): Builder
    {

        // if ($this->request->method() === 'GET' && $this->routerID === 0) {

        foreach ($this->conditions as $field => $condition) {
            if (is_array($condition)) {

                switch ($condition[0]) {
                    case 'between':
                        $query->whereDate($field, '>=', $condition[1]);
                        $query->whereDate($field, '<=', $condition[2]);
                        break;
                    case 'date':
                        $query->whereDate($field, $condition[1], $condition[2]);
                        break;
                    default:
                        $query->where($field, $condition[0], $condition[1]);
                        break;
                }
            } else {
                $query->where($field, '=', $condition);
            }
        }
        // }

        return $query;
    }

    public function setOrder(Builder &$list): void
    {
        if ($this->request->get('order')) {
            if (!in_array($this->request->get('order'), $this->sortable)) {
                throw new \Exception(__tr('The requested field is not enabled for sorting'), 400);
            }
            $this->request->get('sort') === 'desc'
                ? $list->orderByDesc($this->request->get('order'))
                : $list->orderBy($this->request->get('order'));
        }
    }
}
