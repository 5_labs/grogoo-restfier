<?php

namespace GrogooRestfier\Helpers;

class ConfigPersisted
{

    private $file;

    public function __construct($file)
    {
        $this->file = '/tmp/' . $file . '.log';
    }

    public function init(array $content = [])
    {
        $this->remove();
        $this->save(json_encode($content));
    }

    public function add($key, $value)
    {
        $this->save(
            json_encode(
                array_merge(
                    $this->load(),
                    [$key => $value]
                )
            )
        );
    }

    public function get($key = null)
    {
        if (null === $key) {
            return $this->load();
        } else {
            return $this->load()[$key] ?? null;
        }
    }

    private function save($content)
    {
        Helper::saveFile($this->file, $content, 'SOBREPOR');
    }

    private function load(): array
    {
        if (!file_exists($this->file)) {
            $this->save('{}');
        }
        return json_decode(file_get_contents($this->file), true);
    }

    public function remove()
    {
        if (file_exists($this->file)) {
            unlink($this->file);
        }
    }
}
