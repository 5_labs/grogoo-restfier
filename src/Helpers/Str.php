<?php

namespace GrogooRestfier\Helpers;

class Str
{

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int  $length
     * @return string
     */
    public static function random($length = 16)
    {
        return (function ($length) {
            $string = '';

            while (($len = strlen($string)) < $length) {
                $size = $length - $len;

                $bytesSize = (int) ceil($size / 3) * 3;

                $bytes = random_bytes($bytesSize);

                $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
            }

            return $string;
        })($length);
    }

    public static function hash($content, $size = 64): string
    {
        $content = hash('sha256', $content);

        switch ($size) {
            case 32:
                return hash('md5', $content);
            case 40:
                return hash('sha1', $content);
            case 96:
                return hash('sha384', $content);
            case 128:
                return hash('sha512', $content);
            default:
                return hash('sha256', $content);
        }
    }
}
