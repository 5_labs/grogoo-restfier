<?php

namespace GrogooRestfier\Helpers;

use DateTime;
use Illuminate\Support\Facades\Log;

class Helper
{

    public static $DBERRORS = [
        '01000' => 'Warning: General warning',
        '01004' => 'Warning: String data, right-truncated',
        '01006' => 'Warning: Privilege not revoked',
        '01007' => 'Warning: Privilege not granted',
        '01S00' => 'Invalid connection string attribute',
        '07001' => 'Warning: Wrong number of parameters',
        '07002' => 'Warning: Count field incorrect',
        '07005' => 'Warning: Prepared statement not executed',
        '07006' => 'Warning: Restricted data type attribute violation',
        '07009' => 'Warning: Invalid descriptor index',
        '08001' => 'Client unable to establish connection',
        '08002' => 'Connection name in use',
        '08003' => 'Connection does not exist',
        '08004' => 'Server rejected the connection',
        '08006' => 'Connection failure',
        '08S01' => 'Communication link failure',
        '21S01' => 'Insert value list does not match column list',
        '22001' => 'String data right truncation',
        '22002' => 'Indicator variable required but not supplied',
        '22003' => 'Numeric value out of range',
        '22007' => 'Invalid datetime format',
        '22008' => 'Datetime field overflow',
        '22012' => 'Division by zero',
        '22015' => 'Interval field overflow',
        '22018' => 'Invalid character value for cast specification',
        '22025' => 'Invalid escape character',
        '23000' => 'Integrity constraint violation',
        '24000' => 'Invalid cursor state',
        '28000' => 'Invalid authorization specification',
        '34000' => 'Invalid cursor name',
        '3D000' => 'Invalid catalog name',
        '40001' => 'Serialization failure',
        '40003' => 'Statement completion unknown',
        '42000' => 'Syntax error or access violation',
        '42S01' => 'Base table or view already exists',
        '42S02' => 'Base table or view not found',
        '42S11' => 'Index already exists',
        '42S12' => 'Index not found',
        '42S21' => 'Column already exists',
        '42S22' => 'Column not found',
        'HY000' => 'General error',
        'HY001' => 'Memory allocation error',
        'HY004' => 'Invalid SQL data type',
        'HY008' => 'Operation canceled',
        'HY009' => 'Invalid use of null pointer',
        'HY010' => 'Function sequence error',
        'HY011' => 'Attribute cannot be set now',
        'HYT00' => 'Timeout expired',
        'IM001' => 'Driver does not support this function',
        'IM017' => 'Polling is disabled',
        '23502' => 'Null value not allowed - check constraint violation',
        '23503' => 'Foreign key violation',
        '23505' => 'Unique constraint violation',
        '23514' => 'Check constraint violation',
        '24000' => 'Invalid cursor state',
        '40002' => 'Transaction rollback',
        '42000' => 'Syntax error or access violation',
        '42S02' => 'Table not found',
        '42S22' => 'Column not found',
        'HY000' => 'General error',
        '25P02' => 'Transaction aborted due to conflicts with concurrent transaction',
        '42703' => 'Column not found'
    ];

    public const IF_NOT_UPDATE = 'IF_NOT_UPDATED';
    public const OVERWRITE = 'SOBREPOR';

    /**
     * Retorna a string no formato camelCase
     * @param string|array $string
     * @param array $prefixo
     * @return string|array
     */
    public static function name2CamelCase($string, $prefixo = false)
    {
        $prefixo = array('mem_', 'sis_', 'anz_', 'aux_', 'app_');
        if (is_array($string)) {
            foreach ($string as $key => $value) {
                $out[self::name2CamelCase($key)] = $value;
            }
            return $out;
        }
        if (is_array($prefixo)) {
            foreach ($prefixo as $val) {
                $string = str_replace($val, "", $string);
            }
        }

        $string = str_replace('_', ' ', $string);
        $string = str_replace('-', ' ', $string);
        //        $out = str_replace(' ', '', ucwords($string));
        $out = lcfirst(str_replace(' ', '', ucwords($string)));
        return $out;
    }

    /**
     * Revert a string camelCase para camel_case
     * @param string $string
     * @return string
     */
    public static function reverteName2CamelCase($string): string
    {
        $out = '';
        for ($i = 0; $i < strlen((string) $string); $i++) {
            if ($string[$i] === mb_strtoupper((string)$string[$i]) && $string[$i] !== '.') {
                $out .= (($i > 0) ? '_' : '');
                $string[$i] = mb_strtolower($string[$i]);
            }
            $out .= $string[$i];
        }
        return (string) $out;
    }

    public static function name2KebabCase($string, $prefixo = false)
    {
        if (stripos($string, '_') === false) {
            $string = self::reverteName2CamelCase($string);
        }

        return str_replace('_', '-', $string);
    }

    public static function compareString($str1, $str2, $case = false)
    {
        if (!$case) {
            return (mb_strtoupper((string)$str1) === mb_strtoupper((string)$str2));
        } else {
            return ($str1 === $str2);
        }
    }

    public static function  formatBytes($bytes, $precision = 2) {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        $power = $bytes > 0 ? floor(log($bytes, 1024)) : 0;

        return number_format($bytes / (1024 ** $power), $precision, '.', ',') . ' ' . $units[$power];
    }

    /**
     * Ira buscar o path da aplicação, antes da path /vendor
     */
    public static function getPathApp()
    {
        $parts = explode('/vendor/', __DIR__);
        return $parts[0];
    }

    public static function mkdir($path, $perm = 0777): void
    {
        if (!is_dir($path) && !is_file($path)) {
            @mkdir($path, $perm, true);
        }
    }

    /**
     * Cria a arvore de diretorios
     * @param string $filename
     * @return object
     */
    public static function createTreeDir($filename)
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, (string) $filename);
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        $file = array_pop($parts);
        $dir = implode(DIRECTORY_SEPARATOR, $parts);
        self::mkdir($dir, 0777);
        // @mkdir($dir, 0777, true);
        return (object) ['path' => $dir, 'name' => $file];
    }

    /**
     * Salva um arquivo no disco
     *
     * @param string $filename
     * @param string $template
     * @param string $mode
     * @return void
     */
    public static function saveFile(string $filename, string $template = '', string $mode = ''): bool
    {
        if (file_exists($filename)) {

            if ($mode === 'IF_NOT_UPDATED') {
                $criacao = filectime($filename);
                $atualizacao = filemtime($filename);
                if ($atualizacao <= $criacao) {
                    // Sem alteração, sobrepor
                    return self::saveFile($filename, $template, 'SOBREPOR');
                }
            }

            $filename = dirname($filename)
                . DIRECTORY_SEPARATOR
                . ($mode !== 'SOBREPOR' ? '__NEW__' : '')
                . basename($filename);

            // $mode !== 'SOBREPOR' ? Log::debug($filename) : null;
        } else {
            self::createTreeDir($filename);
        }

        file_put_contents($filename, $template);
        return file_exists($filename);
    }

    public static  function singularize($word)
    {
        $pluralEndings = [
            '/(alias|address)es$/i' => '\1',
            '/([^aeiouy])ies$/i' => '\1y',
            '/(ss)$/i' => 'ss',
            '/(n)ews$/i' => '\1ews',
            '/(r)ice$/i' => '\1ice',
            '/(children)$/i' => 'child',
            '/(m)en$/i' => '\1an',
            '/(t)eeth$/i' => '\1ooth',
            '/(f)eet$/i' => '\1oot',
            '/(g)eese$/i' => '\1oose',
            '/(m)ice$/i' => '\1ouse',
            '/(x|ch|ss|sh)es$/i' => '\1',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([lr])ves$/i' => '\1f',
            '/(tive)s$/i' => '\1',
            '/(hive)s$/i' => '\1',
            '/(pri)ces$/i' => '\1ce',
            '/(b)uses$/i' => '\1us',
            '/(shoe)s$/i' => '\1',
            '/(o)es$/i' => '\1',
            '/(ax|test)es$/i' => '\1is',
            '/(octop|vir)i$/i' => '\1us',
            '/(status)$/i' => '\1',
            '/(alias)es$/i' => '\1',
            '/s$/i' => '',
        ];

        foreach ($pluralEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return $word;
    }

    public static function pluralize($word)
    {
        $singularEndings = [
            '/(quiz)$/i' => '\1zes',
            '/(matr|vert|ind)ix|ex$/i' => '\1ices',
            '/(x|ch|ss|sh)$/i' => '\1es',
            '/(r|t|h|s|z)$/i' => '\1es',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/(n)ews$/i' => '\1ews',
            '/(child)$/i' => '\1ren',
            '/(bus)$/i' => '\1es',
            '/(woman)$/i' => '\1women',
            '/(man)$/i' => '\1men',
            '/(tooth)$/i' => '\1teeth',
            '/(foot)$/i' => '\1feet',
            '/(person)$/i' => '\1people',
            '/(goose)$/i' => '\1geese',
            '/(mouse)$/i' => '\1mice',
            '/(cactus)$/i' => '\1cacti',
            '/(knife)$/i' => '\1knives',
            '/(leaf)$/i' => '\1leaves',
            '/(life)$/i' => '\1lives',
            '/(wife)$/i' => '\1wives',
            '/(hero)$/i' => '\1heroes',
            '/(potato)$/i' => '\1potatoes',
            '/(tomato)$/i' => '\1tomatoes',
            '/(buffalo)$/i' => '\1buffaloes',
            '/(index)$/i' => '\1indices',
            '/(alias)$/i' => '\1aliases',
            '/(status)$/i' => '\1status',
            '/(radius)$/i' => '\1radii',
            '/(syllabus)$/i' => '\1syllabi',
            '/(focus)$/i' => '\1foci',
            '/(fungus)$/i' => '\1fungi',
            '/(datum)$/i' => '\1data',
            '/(appendix)$/i' => '\1appendices',
            '/(bacterium)$/i' => '\1bacteria',
            '/(curriculum)$/i' => '\1curricula',
            '/^(compan)y$/i' => '\1ies', // adicionado padrão para "company" -> "companies"

        ];

        foreach ($singularEndings as $pattern => $replacement) {
            if (preg_match($pattern, $word)) {
                return preg_replace($pattern, $replacement, $word);
            }
        }

        return $word . 's';
    }


    /**
     * Delete a dir full
     *
     * @param string $path
     * @return bool
     */
    public static function deleteDirectory(string $path): bool
    {
        $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
        if (!is_dir($path)) {
            return true;
        }

        $iterator = new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS);
        $rec_iterator = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($rec_iterator as $file) {
            $file->isFile() ? unlink($file->getPathname()) : rmdir($file->getPathname());
        }

        rmdir($path);
        return is_dir($path);
    }

    public static function fileSearchRecursive($file_name, $dir_init, $deep = 10)
    {
        $dirarray = explode(DIRECTORY_SEPARATOR, $dir_init);
        $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;
        $count = 0;
        while (!@file_exists($filename) && $count < $deep) { // paths acima
            array_pop($dirarray);
            $filename = implode(DIRECTORY_SEPARATOR, $dirarray) . DIRECTORY_SEPARATOR . $file_name;
            $count++;
        }
        $filename = @realpath($filename);
        if (!@file_exists($filename)) {
            return false;
        } else {
            return $filename;
        }
    }

    public static function generateCode($string)
    {
        return strtoupper(substr($string, 0, 1) . substr($string, -1) . substr(md5($string), 0, 2));
    }

    public static function shellExec($command)
    {

        $descriptorspec = [
            0 => ['pipe', 'r'], // stdin is a pipe that the child will read from
            1 => ['pipe', 'w'], // stdout is a pipe that the child will write to
            2 => ['pipe', 'w'], // stderr is a pipe that the child will write to
        ];

        $process = proc_open($command, $descriptorspec, $pipes);

        if (is_resource($process)) {
            fclose($pipes[0]); // Close the stdin pipe, since we won't be using it

            // Read the output from stdout and stderr pipes
            $output = '';
            while (($buffer = fgets($pipes[1])) !== false || ($buffer = fgets($pipes[2])) !== false) {
                $output .= $buffer;
                echo $buffer; // Display the output in real-time
                flush(); // Force the output to be sent to the browser

                // If PHP output buffering is enabled, force it to be sent to the browser
                if (ob_get_length() > 0) {
                    ob_flush();
                }
            }

            fclose($pipes[1]);
            fclose($pipes[2]);

            $return_value = proc_close($process);

            // echo "Command returned: $return_value\n";
        }
    }

    public static function commandPrintHeader($text, $size = 60)
    {
        $cmd = "header=\"$text\" && width=$size && padding=\$(((\$width - \${#header}) / 2)) && printf '%*s\n' \"\${COLUMNS:-40}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\" && printf \"|%*s%s%*s|\n\" \$padding \"\" \"\$header\" \$padding \"\" && printf '%*s\n' \"\${COLUMNS:-80}\" \"\" | tr ' ' '-' | cut -c 1-\"\${width}\"";
        self::shellExec($cmd);
    }

    public static function copyDir($source, $destination, array $ignore = [])
    {
        // Verifica se o diretório de origem existe
        if (!file_exists($source)) {
            return false;
        }

        // Cria o diretório de destino se ele não existir
        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        // Faz um loop pelos arquivos e pastas dentro do diretório de origem
        $dir = opendir($source);
        while ($file = readdir($dir)) {
            if ($file !== '.' && $file !== '..' && array_search($file, $ignore) === false) {
                $srcFile = $source . '/' . $file;
                $destFile = $destination . '/' . $file;

                // Copia arquivos ou pastas recursivamente
                if (is_dir($srcFile)) {
                    self::copyDir($srcFile, $destFile);
                } else {
                    !file_exists($destFile) ? copy($srcFile, $destFile) : null;
                }
            }
        }
        closedir($dir);
        return true;
    }

    public static function getEnv($key)
    {
        $envFilePath = Helper::getPathApp() . '/.env';
        if (file_exists($envFilePath . '.' . getenv('APP_ENV'))) {
            $envFilePath .= '.' . getenv('APP_ENV');
        }

        $arr = [];
        if (file_exists($envFilePath)) {
            $arr = parse_ini_file($envFilePath);
            return $arr[$key] ?? null;
        } else {
            return getenv($key) !== false ? getenv($key) : null;
        }
    }

    public static function isDate($dateString, $dateFormat = "Y-m-d"): bool
    {
        $dateTime = DateTime::createFromFormat($dateFormat, $dateString);

        return $dateTime && $dateTime->format($dateFormat) === $dateString;
    }

    public static function getRequestIp(): string {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && strlen($_SERVER['HTTP_X_FORWARDED_FOR']) > 0) {
            $parts = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            // Ajuste para pegar o primeiro IP da lista, que normalmente é o IP original do cliente
            $IP = trim($parts[0]);
        } else {
            $IP = $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['REMOTE_ADDR'] ?? '-';
        }

        return $IP;
    }

    public static function getMimeType(string $extension): string
    {
        $extensionToMimeType = [
            'ez' => 'application/andrew-inset',
            'aw' => 'application/applixware',
            'atom' => 'application/atom+xml',
            'atomcat' => 'application/atomcat+xml',
            'atomsvc' => 'application/atomsvc+xml',
            'ccxml' => 'application/ccxml+xml',
            'cu' => 'application/cu-seeme',
            'davmount' => 'application/davmount+xml',
            'ecma' => 'application/ecmascript',
            'emma' => 'application/emma+xml',
            'epub' => 'application/epub+zip',
            'pfr' => 'application/font-tdpfr',
            'gz' => 'application/gzip',
            'tgz' => 'application/gzip',
            'stk' => 'application/hyperstudio',
            'jar' => 'application/java-archive',
            'ser' => 'application/java-serialized-object',
            'class' => 'application/java-vm',
            'json' => 'application/json',
            'lostxml' => 'application/lost+xml',
            'hqx' => 'application/mac-binhex40',
            'cpt' => 'application/mac-compactpro',
            'mrc' => 'application/marc',
            'ma' => 'application/mathematica',
            'mb' => 'application/mathematica',
            'nb' => 'application/mathematica',
            'mathml' => 'application/mathml+xml',
            'mml' => 'application/mathml+xml',
            'mbox' => 'application/mbox',
            'mscml' => 'application/mediaservercontrol+xml',
            'mp4s' => 'application/mp4',
            'doc' => 'application/msword',
            'dot' => 'application/msword',
            'wiz' => 'application/msword',
            'mxf' => 'application/mxf',
            'a' => 'application/octet-stream',
            'bin' => 'application/octet-stream',
            'bpk' => 'application/octet-stream',
            'deploy' => 'application/octet-stream',
            'dist' => 'application/octet-stream',
            'distz' => 'application/octet-stream',
            'dmg' => 'application/octet-stream',
            'dms' => 'application/octet-stream',
            'dump' => 'application/octet-stream',
            'elc' => 'application/octet-stream',
            'lha' => 'application/octet-stream',
            'lrf' => 'application/octet-stream',
            'lzh' => 'application/octet-stream',
            'o' => 'application/octet-stream',
            'obj' => 'application/octet-stream',
            'pkg' => 'application/octet-stream',
            'so' => 'application/octet-stream',
            'oda' => 'application/oda',
            'opf' => 'application/oebps-package+xml',
            'ogx' => 'application/ogg',
            'onepkg' => 'application/onenote',
            'onetmp' => 'application/onenote',
            'onetoc' => 'application/onenote',
            'onetoc2' => 'application/onenote',
            'xer' => 'application/patch-ops-error+xml',
            'pdf' => 'application/pdf',
            'pgp' => 'application/pgp-encrypted',
            'asc' => 'application/pgp-signature',
            'sig' => 'application/pgp-signature',
            'prf' => 'application/pics-rules',
            'p10' => 'application/pkcs10',
            'p7c' => 'application/pkcs7-mime',
            'p7m' => 'application/pkcs7-mime',
            'p7s' => 'application/pkcs7-signature',
            'cer' => 'application/pkix-cert',
            'crl' => 'application/pkix-crl',
            'pkipath' => 'application/pkix-pkipath',
            'pki' => 'application/pkixcmp',
            'pls' => 'application/pls+xml',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            'prql' => 'application/prql',
            'cww' => 'application/prs.cww',
            'rdf' => 'application/rdf+xml',
            'rif' => 'application/reginfo+xml',
            'rnc' => 'application/relax-ng-compact-syntax',
            'rl' => 'application/resource-lists+xml',
            'rld' => 'application/resource-lists-diff+xml',
            'rs' => 'application/rls-services+xml',
            'rsd' => 'application/rsd+xml',
            'rss' => 'application/rss+xml',
            'xml' => 'application/rss+xml',
            'rtf' => 'application/rtf',
            'sbml' => 'application/sbml+xml',
            'scq' => 'application/scvp-cv-request',
            'scs' => 'application/scvp-cv-response',
            'spq' => 'application/scvp-vp-request',
            'spp' => 'application/scvp-vp-response',
            'sdp' => 'application/sdp',
            'setpay' => 'application/set-payment-initiation',
            'setreg' => 'application/set-registration-initiation',
            'shf' => 'application/shf+xml',
            'smi' => 'application/smil+xml',
            'smil' => 'application/smil+xml',
            'rq' => 'application/sparql-query',
            'srx' => 'application/sparql-results+xml',
            'gram' => 'application/srgs',
            'grxml' => 'application/srgs+xml',
            'ssml' => 'application/ssml+xml',
            'plb' => 'application/vnd.3gpp.pic-bw-large',
            'psb' => 'application/vnd.3gpp.pic-bw-small',
            'pvb' => 'application/vnd.3gpp.pic-bw-var',
            'tcap' => 'application/vnd.3gpp2.tcap',
            'pwn' => 'application/vnd.3m.post-it-notes',
            'aso' => 'application/vnd.accpac.simply.aso',
            'imp' => 'application/vnd.accpac.simply.imp',
            'acu' => 'application/vnd.acucobol',
            'acutc' => 'application/vnd.acucorp',
            'atc' => 'application/vnd.acucorp',
            'air' => 'application/vnd.adobe.air-application-installer-package+zip',
            'xdp' => 'application/vnd.adobe.xdp+xml',
            'xfdf' => 'application/vnd.adobe.xfdf',
            'azf' => 'application/vnd.airzip.filesecure.azf',
            'azs' => 'application/vnd.airzip.filesecure.azs',
            'azw' => 'application/vnd.amazon.ebook',
            'acc' => 'application/vnd.americandynamics.acc',
            'ami' => 'application/vnd.amiga.ami',
            'apk' => 'application/vnd.android.package-archive',
            'cii' => 'application/vnd.anser-web-certificate-issue-initiation',
            'fti' => 'application/vnd.anser-web-funds-transfer-initiation',
            'atx' => 'application/vnd.antix.game-component',
            'mpkg' => 'application/vnd.apple.installer+xml',
            'swi' => 'application/vnd.arastra.swi',
            'aep' => 'application/vnd.audiograph',
            'mpm' => 'application/vnd.blueice.multipass',
            'bmi' => 'application/vnd.bmi',
            'rep' => 'application/vnd.businessobjects',
            'cdxml' => 'application/vnd.chemdraw+xml',
            'mmd' => 'application/vnd.chipnuts.karaoke-mmd',
            'cdy' => 'application/vnd.cinderella',
            'cla' => 'application/vnd.claymore',
            'c4d' => 'application/vnd.clonk.c4group',
            'c4f' => 'application/vnd.clonk.c4group',
            'c4g' => 'application/vnd.clonk.c4group',
            'c4p' => 'application/vnd.clonk.c4group',
            'c4u' => 'application/vnd.clonk.c4group',
            'csp' => 'application/vnd.commonspace',
            'cdbcmsg' => 'application/vnd.contact.cmsg',
            'cmc' => 'application/vnd.cosmocaller',
            'clkx' => 'application/vnd.crick.clicker',
            'clkk' => 'application/vnd.crick.clicker.keyboard',
            'clkp' => 'application/vnd.crick.clicker.palette',
            'clkt' => 'application/vnd.crick.clicker.template',
            'clkw' => 'application/vnd.crick.clicker.wordbank',
            'wbs' => 'application/vnd.criticaltools.wbs+xml',
            'pml' => 'application/vnd.ctc-posml',
            'ppd' => 'application/vnd.cups-ppd',
            'car' => 'application/vnd.curl.car',
            'pcurl' => 'application/vnd.curl.pcurl',
            'rdz' => 'application/vnd.data-vision.rdz',
            'deb' => 'application/vnd.debian.binary-package',
            'udeb' => 'application/vnd.debian.binary-package',
            'fe_launch' => 'application/vnd.denovo.fcselayout-link',
            'dna' => 'application/vnd.dna',
            'mlp' => 'application/vnd.dolby.mlp',
            'dpg' => 'application/vnd.dpgraph',
            'dfac' => 'application/vnd.dreamfactory',
            'geo' => 'application/vnd.dynageo',
            'mag' => 'application/vnd.ecowin.chart',
            'nml' => 'application/vnd.enliven',
            'esf' => 'application/vnd.epson.esf',
            'msf' => 'application/vnd.epson.msf',
            'qam' => 'application/vnd.epson.quickanime',
            'slt' => 'application/vnd.epson.salt',
            'ssf' => 'application/vnd.epson.ssf',
            'es3' => 'application/vnd.eszigno3+xml',
            'et3' => 'application/vnd.eszigno3+xml',
            'ez2' => 'application/vnd.ezpix-album',
            'ez3' => 'application/vnd.ezpix-package',
            'fdf' => 'application/vnd.fdf',
            'mseed' => 'application/vnd.fdsn.mseed',
            'dataless' => 'application/vnd.fdsn.seed',
            'seed' => 'application/vnd.fdsn.seed',
            'gph' => 'application/vnd.flographit',
            'ftc' => 'application/vnd.fluxtime.clip',
            'book' => 'application/vnd.framemaker',
            'fm' => 'application/vnd.framemaker',
            'frame' => 'application/vnd.framemaker',
            'maker' => 'application/vnd.framemaker',
            'fnc' => 'application/vnd.frogans.fnc',
            'ltf' => 'application/vnd.frogans.ltf',
            'fsc' => 'application/vnd.fsc.weblaunch',
            'oas' => 'application/vnd.fujitsu.oasys',
            'oa2' => 'application/vnd.fujitsu.oasys2',
            'oa3' => 'application/vnd.fujitsu.oasys3',
            'fg5' => 'application/vnd.fujitsu.oasysgp',
            'bh2' => 'application/vnd.fujitsu.oasysprs',
            'ddd' => 'application/vnd.fujixerox.ddd',
            'xdw' => 'application/vnd.fujixerox.docuworks',
            'xbd' => 'application/vnd.fujixerox.docuworks.binder',
            'fzs' => 'application/vnd.fuzzysheet',
            'txd' => 'application/vnd.genomatix.tuxedo',
            'ggb' => 'application/vnd.geogebra.file',
            'ggt' => 'application/vnd.geogebra.tool',
            'gex' => 'application/vnd.geometry-explorer',
            'gre' => 'application/vnd.geometry-explorer',
            'gbr' => 'application/vnd.gerber',
            'gmx' => 'application/vnd.gmx',
            'kml' => 'application/vnd.google-earth.kml+xml',
            'kmz' => 'application/vnd.google-earth.kmz',
            'gqf' => 'application/vnd.grafeq',
            'gqs' => 'application/vnd.grafeq',
            'gac' => 'application/vnd.groove-account',
            'ghf' => 'application/vnd.groove-help',
            'gim' => 'application/vnd.groove-identity-message',
            'grv' => 'application/vnd.groove-injector',
            'gtm' => 'application/vnd.groove-tool-message',
            'tpl' => 'application/vnd.groove-tool-template',
            'vcg' => 'application/vnd.groove-vcard',
            'zmm' => 'application/vnd.handheld-entertainment+xml',
            'hbci' => 'application/vnd.hbci',
            'les' => 'application/vnd.hhe.lesson-player',
            'hpgl' => 'application/vnd.hp-hpgl',
            'hpid' => 'application/vnd.hp-hpid',
            'hps' => 'application/vnd.hp-hps',
            'jlt' => 'application/vnd.hp-jlyt',
            'pcl' => 'application/vnd.hp-pcl',
            'pclxl' => 'application/vnd.hp-pclxl',
            'sfd-hdstx' => 'application/vnd.hydrostatix.sof-data',
            'x3d' => 'application/vnd.hzn-3d-crossword',
            'mpy' => 'application/vnd.ibm.minipay',
            'afp' => 'application/vnd.ibm.modcap',
            'list3820' => 'application/vnd.ibm.modcap',
            'listafp' => 'application/vnd.ibm.modcap',
            'irm' => 'application/vnd.ibm.rights-management',
            'sc' => 'application/vnd.ibm.secure-container',
            'icc' => 'application/vnd.iccprofile',
            'icm' => 'application/vnd.iccprofile',
            'igl' => 'application/vnd.igloader',
            'ivp' => 'application/vnd.immervision-ivp',
            'ivu' => 'application/vnd.immervision-ivu',
            'xpw' => 'application/vnd.intercon.formnet',
            'xpx' => 'application/vnd.intercon.formnet',
            'qbo' => 'application/vnd.intu.qbo',
            'qfx' => 'application/vnd.intu.qfx',
            'rcprofile' => 'application/vnd.ipunplugged.rcprofile',
            'irp' => 'application/vnd.irepository.package+xml',
            'xpr' => 'application/vnd.is-xpr',
            'jam' => 'application/vnd.jam',
            'rms' => 'application/vnd.jcp.javame.midlet-rms',
            'jisp' => 'application/vnd.jisp',
            'joda' => 'application/vnd.joost.joda-archive',
            'ktr' => 'application/vnd.kahootz',
            'ktz' => 'application/vnd.kahootz',
            'karbon' => 'application/vnd.kde.karbon',
            'chrt' => 'application/vnd.kde.kchart',
            'kfo' => 'application/vnd.kde.kformula',
            'flw' => 'application/vnd.kde.kivio',
            'kon' => 'application/vnd.kde.kontour',
            'kpr' => 'application/vnd.kde.kpresenter',
            'kpt' => 'application/vnd.kde.kpresenter',
            'ksp' => 'application/vnd.kde.kspread',
            'kwd' => 'application/vnd.kde.kword',
            'kwt' => 'application/vnd.kde.kword',
            'htke' => 'application/vnd.kenameaapp',
            'kia' => 'application/vnd.kidspiration',
            'kne' => 'application/vnd.kinar',
            'knp' => 'application/vnd.kinar',
            'skd' => 'application/vnd.koan',
            'skm' => 'application/vnd.koan',
            'skp' => 'application/vnd.koan',
            'skt' => 'application/vnd.koan',
            'sse' => 'application/vnd.kodak-descriptor',
            'lbd' => 'application/vnd.llamagraphics.life-balance.desktop',
            'lbe' => 'application/vnd.llamagraphics.life-balance.exchange+xml',
            '123' => 'application/vnd.lotus-1-2-3',
            'apr' => 'application/vnd.lotus-approach',
            'pre' => 'application/vnd.lotus-freelance',
            'nsf' => 'application/vnd.lotus-notes',
            'org' => 'application/vnd.lotus-organizer',
            'scm' => 'application/vnd.lotus-screencam',
            'lwp' => 'application/vnd.lotus-wordpro',
            'portpkg' => 'application/vnd.macports.portpkg',
            'mcd' => 'application/vnd.mcd',
            'mc1' => 'application/vnd.medcalcdata',
            'cdkey' => 'application/vnd.mediastation.cdkey',
            'mwf' => 'application/vnd.mfer',
            'mfm' => 'application/vnd.mfmp',
            'flo' => 'application/vnd.micrografx.flo',
            'igx' => 'application/vnd.micrografx.igx',
            'mif' => 'application/vnd.mif',
            'daf' => 'application/vnd.mobius.daf',
            'dis' => 'application/vnd.mobius.dis',
            'mbk' => 'application/vnd.mobius.mbk',
            'mqy' => 'application/vnd.mobius.mqy',
            'msl' => 'application/vnd.mobius.msl',
            'plc' => 'application/vnd.mobius.plc',
            'txf' => 'application/vnd.mobius.txf',
            'mpn' => 'application/vnd.mophun.application',
            'mpc' => 'application/vnd.mophun.certificate',
            'xul' => 'application/vnd.mozilla.xul+xml',
            'cil' => 'application/vnd.ms-artgalry',
            'cab' => 'application/vnd.ms-cab-compressed',
            'xla' => 'application/vnd.ms-excel',
            'xlb' => 'application/vnd.ms-excel',
            'xlc' => 'application/vnd.ms-excel',
            'xlm' => 'application/vnd.ms-excel',
            'xls' => 'application/vnd.ms-excel',
            'xlt' => 'application/vnd.ms-excel',
            'xlw' => 'application/vnd.ms-excel',
            'xlam' => 'application/vnd.ms-excel.addin.macroenabled.12',
            'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroenabled.12',
            'xlsm' => 'application/vnd.ms-excel.sheet.macroenabled.12',
            'xltm' => 'application/vnd.ms-excel.template.macroenabled.12',
            'eot' => 'application/vnd.ms-fontobject',
            'chm' => 'application/vnd.ms-htmlhelp',
            'ims' => 'application/vnd.ms-ims',
            'lrm' => 'application/vnd.ms-lrm',
            'cat' => 'application/vnd.ms-pki.seccat',
            'stl' => 'application/vnd.ms-pki.stl',
            'pot' => 'application/vnd.ms-powerpoint',
            'ppa' => 'application/vnd.ms-powerpoint',
            'pps' => 'application/vnd.ms-powerpoint',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pwz' => 'application/vnd.ms-powerpoint',
            'ppam' => 'application/vnd.ms-powerpoint.addin.macroenabled.12',
            'pptm' => 'application/vnd.ms-powerpoint.presentation.macroenabled.12',
            'sldm' => 'application/vnd.ms-powerpoint.slide.macroenabled.12',
            'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
            'potm' => 'application/vnd.ms-powerpoint.template.macroenabled.12',
            'mpp' => 'application/vnd.ms-project',
            'mpt' => 'application/vnd.ms-project',
            'docm' => 'application/vnd.ms-word.document.macroenabled.12',
            'dotm' => 'application/vnd.ms-word.template.macroenabled.12',
            'wcm' => 'application/vnd.ms-works',
            'wdb' => 'application/vnd.ms-works',
            'wks' => 'application/vnd.ms-works',
            'wps' => 'application/vnd.ms-works',
            'wpl' => 'application/vnd.ms-wpl',
            'xps' => 'application/vnd.ms-xpsdocument',
            'mseq' => 'application/vnd.mseq',
            'mus' => 'application/vnd.musician',
            'msty' => 'application/vnd.muvee.style',
            'nlu' => 'application/vnd.neurolanguage.nlu',
            'nnd' => 'application/vnd.noblenet-directory',
            'nns' => 'application/vnd.noblenet-sealer',
            'nnw' => 'application/vnd.noblenet-web',
            'ngdat' => 'application/vnd.nokia.n-gage.data',
            'n-gage' => 'application/vnd.nokia.n-gage.symbian.install',
            'rpst' => 'application/vnd.nokia.radio-preset',
            'rpss' => 'application/vnd.nokia.radio-presets',
            'edm' => 'application/vnd.novadigm.edm',
            'edx' => 'application/vnd.novadigm.edx',
            'ext' => 'application/vnd.novadigm.ext',
            'odc' => 'application/vnd.oasis.opendocument.chart',
            'otc' => 'application/vnd.oasis.opendocument.chart-template',
            'odb' => 'application/vnd.oasis.opendocument.database',
            'odf' => 'application/vnd.oasis.opendocument.formula',
            'odft' => 'application/vnd.oasis.opendocument.formula-template',
            'odg' => 'application/vnd.oasis.opendocument.graphics',
            'otg' => 'application/vnd.oasis.opendocument.graphics-template',
            'odi' => 'application/vnd.oasis.opendocument.image',
            'oti' => 'application/vnd.oasis.opendocument.image-template',
            'odp' => 'application/vnd.oasis.opendocument.presentation',
            'otp' => 'application/vnd.oasis.opendocument.presentation-template',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            'ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
            'odt' => 'application/vnd.oasis.opendocument.text',
            'otm' => 'application/vnd.oasis.opendocument.text-master',
            'ott' => 'application/vnd.oasis.opendocument.text-template',
            'oth' => 'application/vnd.oasis.opendocument.text-web',
            'xo' => 'application/vnd.olpc-sugar',
            'dd2' => 'application/vnd.oma.dd2+xml',
            'oxt' => 'application/vnd.openofficeorg.extension',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
            'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'dp' => 'application/vnd.osgi.dp',
            'oprc' => 'application/vnd.palm',
            'pdb' => 'application/vnd.palm',
            'pqa' => 'application/vnd.palm',
            'str' => 'application/vnd.pg.format',
            'ei6' => 'application/vnd.pg.osasli',
            'efif' => 'application/vnd.picsel',
            'plf' => 'application/vnd.pocketlearn',
            'pbd' => 'application/vnd.powerbuilder6',
            'box' => 'application/vnd.previewsystems.box',
            'mgz' => 'application/vnd.proteus.magazine',
            'qps' => 'application/vnd.publishare-delta-tree',
            'ptid' => 'application/vnd.pvi.ptid1',
            'qwd' => 'application/vnd.quark.quarkxpress',
            'qwt' => 'application/vnd.quark.quarkxpress',
            'qxb' => 'application/vnd.quark.quarkxpress',
            'qxd' => 'application/vnd.quark.quarkxpress',
            'qxl' => 'application/vnd.quark.quarkxpress',
            'qxt' => 'application/vnd.quark.quarkxpress',
            'rar' => 'application/vnd.rar',
            'mxl' => 'application/vnd.recordare.musicxml',
            'musicxml' => 'application/vnd.recordare.musicxml+xml',
            'cod' => 'application/vnd.rim.cod',
            'rm' => 'application/vnd.rn-realmedia',
            'link66' => 'application/vnd.route66.link66+xml',
            'see' => 'application/vnd.seemail',
            'sema' => 'application/vnd.sema',
            'semd' => 'application/vnd.semd',
            'semf' => 'application/vnd.semf',
            'ifm' => 'application/vnd.shana.informed.formdata',
            'itp' => 'application/vnd.shana.informed.formtemplate',
            'iif' => 'application/vnd.shana.informed.interchange',
            'ipk' => 'application/vnd.shana.informed.package',
            'twd' => 'application/vnd.simtech-mindmapper',
            'twds' => 'application/vnd.simtech-mindmapper',
            'mmf' => 'application/vnd.smaf',
            'teacher' => 'application/vnd.smart.teacher',
            'sdkd' => 'application/vnd.solent.sdkm+xml',
            'sdkm' => 'application/vnd.solent.sdkm+xml',
            'dxp' => 'application/vnd.spotfire.dxp',
            'sfs' => 'application/vnd.spotfire.sfs',
            'db' => 'application/vnd.sqlite3',
            'sqlite' => 'application/vnd.sqlite3',
            'sqlite3' => 'application/vnd.sqlite3',
            'db-wal' => 'application/vnd.sqlite3',
            'sqlite-wal' => 'application/vnd.sqlite3',
            'db-shm' => 'application/vnd.sqlite3',
            'sqlite-shm' => 'application/vnd.sqlite3',
            'sdc' => 'application/vnd.stardivision.calc',
            'sda' => 'application/vnd.stardivision.draw',
            'sdd' => 'application/vnd.stardivision.impress',
            'smf' => 'application/vnd.stardivision.math',
            'sdw' => 'application/vnd.stardivision.writer',
            'vor' => 'application/vnd.stardivision.writer',
            'sgl' => 'application/vnd.stardivision.writer-global',
            'sxc' => 'application/vnd.sun.xml.calc',
            'stc' => 'application/vnd.sun.xml.calc.template',
            'sxd' => 'application/vnd.sun.xml.draw',
            'std' => 'application/vnd.sun.xml.draw.template',
            'sxi' => 'application/vnd.sun.xml.impress',
            'sti' => 'application/vnd.sun.xml.impress.template',
            'sxm' => 'application/vnd.sun.xml.math',
            'sxw' => 'application/vnd.sun.xml.writer',
            'sxg' => 'application/vnd.sun.xml.writer.global',
            'stw' => 'application/vnd.sun.xml.writer.template',
            'sus' => 'application/vnd.sus-calendar',
            'susp' => 'application/vnd.sus-calendar',
            'svd' => 'application/vnd.svd',
            'sis' => 'application/vnd.symbian.install',
            'sisx' => 'application/vnd.symbian.install',
            'xsm' => 'application/vnd.syncml+xml',
            'bdm' => 'application/vnd.syncml.dm+wbxml',
            'xdm' => 'application/vnd.syncml.dm+xml',
            'tao' => 'application/vnd.tao.intent-module-archive',
            'tmo' => 'application/vnd.tmobile-livetv',
            'tpt' => 'application/vnd.trid.tpt',
            'mxs' => 'application/vnd.triscape.mxs',
            'tra' => 'application/vnd.trueapp',
            'ufd' => 'application/vnd.ufdl',
            'ufdl' => 'application/vnd.ufdl',
            'utz' => 'application/vnd.uiq.theme',
            'umj' => 'application/vnd.umajin',
            'unityweb' => 'application/vnd.unity',
            'uoml' => 'application/vnd.uoml+xml',
            'vcx' => 'application/vnd.vcx',
            'vsd' => 'application/vnd.visio',
            'vss' => 'application/vnd.visio',
            'vst' => 'application/vnd.visio',
            'vsw' => 'application/vnd.visio',
            'vsdx' => 'application/vnd.visio',
            'vssx' => 'application/vnd.visio',
            'vstx' => 'application/vnd.visio',
            'vssm' => 'application/vnd.visio',
            'vstm' => 'application/vnd.visio',
            'vis' => 'application/vnd.visionary',
            'vsf' => 'application/vnd.vsf',
            'sic' => 'application/vnd.wap.sic',
            'slc' => 'application/vnd.wap.slc',
            'wbxml' => 'application/vnd.wap.wbxml',
            'wmlc' => 'application/vnd.wap.wmlc',
            'wmlsc' => 'application/vnd.wap.wmlscriptc',
            'wtb' => 'application/vnd.webturbo',
            'wpd' => 'application/vnd.wordperfect',
            'wqd' => 'application/vnd.wqd',
            'stf' => 'application/vnd.wt.stf',
            'xar' => 'application/vnd.xara',
            'xfdl' => 'application/vnd.xfdl',
            'hvd' => 'application/vnd.yamaha.hv-dic',
            'hvs' => 'application/vnd.yamaha.hv-script',
            'hvp' => 'application/vnd.yamaha.hv-voice',
            'osf' => 'application/vnd.yamaha.openscoreformat',
            'osfpvg' => 'application/vnd.yamaha.openscoreformat.osfpvg+xml',
            'saf' => 'application/vnd.yamaha.smaf-audio',
            'spf' => 'application/vnd.yamaha.smaf-phrase',
            'cmp' => 'application/vnd.yellowriver-custom-menu',
            'zir' => 'application/vnd.zul',
            'zirz' => 'application/vnd.zul',
            'zaz' => 'application/vnd.zzazz.deck+xml',
            'vxml' => 'application/voicexml+xml',
            'wasm' => 'application/wasm',
            'hlp' => 'application/winhlp',
            'wsdl' => 'application/wsdl+xml',
            'wspolicy' => 'application/wspolicy+xml',
            '7z' => 'application/x-7z-compressed',
            'abw' => 'application/x-abiword',
            'zabw' => 'application/x-abiword',
            'abw.gz' => 'application/x-abiword',
            'ace' => 'application/x-ace-compressed',
            'aab' => 'application/x-authorware-bin',
            'u32' => 'application/x-authorware-bin',
            'vox' => 'application/x-authorware-bin',
            'x32' => 'application/x-authorware-bin',
            'aam' => 'application/x-authorware-map',
            'aas' => 'application/x-authorware-seg',
            'bcpio' => 'application/x-bcpio',
            'torrent' => 'application/x-bittorrent',
            'bz' => 'application/x-bzip',
            'boz' => 'application/x-bzip2',
            'bz2' => 'application/x-bzip2',
            'vcd' => 'application/x-cdlink',
            'chat' => 'application/x-chat',
            'pgn' => 'application/x-chess-pgn',
            'cpio' => 'application/x-cpio',
            'csh' => 'application/x-csh',
            'cct' => 'application/x-director',
            'cst' => 'application/x-director',
            'cxt' => 'application/x-director',
            'dcr' => 'application/x-director',
            'dir' => 'application/x-director',
            'dxr' => 'application/x-director',
            'fgd' => 'application/x-director',
            'swa' => 'application/x-director',
            'w3d' => 'application/x-director',
            'wad' => 'application/x-doom',
            'ncx' => 'application/x-dtbncx+xml',
            'dtb' => 'application/x-dtbook+xml',
            'res' => 'application/x-dtbresource+xml',
            'dvi' => 'application/x-dvi',
            'bdf' => 'application/x-font-bdf',
            'gsf' => 'application/x-font-ghostscript',
            'psf' => 'application/x-font-linux-psf',
            'otf' => 'application/x-font-otf',
            'pcf' => 'application/x-font-pcf',
            'snf' => 'application/x-font-snf',
            'ttc' => 'application/x-font-ttf',
            'ttf' => 'application/x-font-ttf',
            'afm' => 'application/x-font-type1',
            'pfa' => 'application/x-font-type1',
            'pfb' => 'application/x-font-type1',
            'pfm' => 'application/x-font-type1',
            'spl' => 'application/x-futuresplash',
            'gnumeric' => 'application/x-gnumeric',
            'gtar' => 'application/x-gtar',
            'hdf' => 'application/x-hdf',
            'iso' => 'application/x-iso9660-image',
            'isoimg' => 'application/x-iso9660-image',
            'cdr' => 'application/x-iso9660-image',
            'jnlp' => 'application/x-java-jnlp-file',
            'kil' => 'application/x-killustrator',
            'kra' => 'application/x-krita',
            'krz' => 'application/x-krita',
            'latex' => 'application/x-latex',
            'mobi' => 'application/x-mobipocket-ebook',
            'prc' => 'application/x-mobipocket-ebook',
            'application' => 'application/x-ms-application',
            'wmd' => 'application/x-ms-wmd',
            'wmz' => 'application/x-ms-wmz',
            'xbap' => 'application/x-ms-xbap',
            'mdb' => 'application/x-msaccess',
            'obd' => 'application/x-msbinder',
            'crd' => 'application/x-mscardfile',
            'clp' => 'application/x-msclip',
            'bat' => 'application/x-msdownload',
            'com' => 'application/x-msdownload',
            'dll' => 'application/x-msdownload',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'm13' => 'application/x-msmediaview',
            'm14' => 'application/x-msmediaview',
            'mvb' => 'application/x-msmediaview',
            'wmf' => 'application/x-msmetafile',
            'mny' => 'application/x-msmoney',
            'pub' => 'application/x-mspublisher',
            'scd' => 'application/x-msschedule',
            'trm' => 'application/x-msterminal',
            'wri' => 'application/x-mswrite',
            'cdf' => 'application/x-netcdf',
            'nc' => 'application/x-netcdf',
            'pm' => 'application/x-perl',
            'pl' => 'application/x-perl',
            'p12' => 'application/x-pkcs12',
            'pfx' => 'application/x-pkcs12',
            'p7b' => 'application/x-pkcs7-certificates',
            'spc' => 'application/x-pkcs7-certificates',
            'p7r' => 'application/x-pkcs7-certreqresp',
            'pyc' => 'application/x-python-code',
            'pyo' => 'application/x-python-code',
            'rpa' => 'application/x-redhat-package-manager',
            'rpm' => 'application/x-rpm',
            'sh' => 'application/x-sh',
            'shar' => 'application/x-shar',
            'shellscript' => 'application/x-shellscript',
            'swf' => 'application/x-shockwave-flash',
            'xap' => 'application/x-silverlight-app',
            'sit' => 'application/x-stuffit',
            'sitx' => 'application/x-stuffitx',
            'sv4cpio' => 'application/x-sv4cpio',
            'sv4crc' => 'application/x-sv4crc',
            'tar' => 'application/x-tar',
            'tcl' => 'application/x-tcl',
            'tex' => 'application/x-tex',
            'tfm' => 'application/x-tex-tfm',
            'texi' => 'application/x-texinfo',
            'texinfo' => 'application/x-texinfo',
            'ustar' => 'application/x-ustar',
            'src' => 'application/x-wais-source',
            'crt' => 'application/x-x509-ca-cert',
            'der' => 'application/x-x509-ca-cert',
            'fig' => 'application/x-xfig',
            'xpi' => 'application/x-xpinstall',
            'zip' => 'application/x-zip-compressed',
            'xenc' => 'application/xenc+xml',
            'xht' => 'application/xhtml+xml',
            'xhtml' => 'application/xhtml+xml',
            'xpdl' => 'application/xml',
            'xsl' => 'application/xml',
            'dtd' => 'application/xml-dtd',
            'xop' => 'application/xop+xml',
            'xslt' => 'application/xslt+xml',
            'xspf' => 'application/xspf+xml',
            'mxml' => 'application/xv+xml',
            'xhvml' => 'application/xv+xml',
            'xvm' => 'application/xv+xml',
            'xvml' => 'application/xv+xml',
            'yaml' => 'application/yaml',
            'yml' => 'application/yaml',
            '3g2' => 'audio/3gpp2',
            'aac' => 'audio/aac',
            'm4a' => 'audio/aac',
            'aacp' => 'audio/aacp',
            'adp' => 'audio/adpcm',
            'aiff' => 'audio/aiff',
            'aif' => 'audio/aiff',
            'aff' => 'audio/aiff',
            'au' => 'audio/basic',
            'snd' => 'audio/basic',
            'flac' => 'audio/flac',
            'kar' => 'audio/midi',
            'mid' => 'audio/midi',
            'midi' => 'audio/midi',
            'rmi' => 'audio/midi',
            'mp4' => 'audio/mp4',
            'm4b' => 'audio/mp4',
            'm4p' => 'audio/mp4',
            'm4r' => 'audio/mp4',
            'm4v' => 'audio/mp4',
            'mp4v' => 'audio/mp4',
            '3gp' => 'audio/mp4',
            '3ga' => 'audio/mp4',
            '3gpa' => 'audio/mp4',
            '3gpp' => 'audio/mp4',
            '3gpp2' => 'audio/mp4',
            '3gp2' => 'audio/mp4',
            'm2a' => 'audio/mpeg',
            'm3a' => 'audio/mpeg',
            'mp2' => 'audio/mpeg',
            'mp2a' => 'audio/mpeg',
            'mp3' => 'audio/mpeg',
            'mpga' => 'audio/mpeg',
            'oga' => 'audio/ogg',
            'ogg' => 'audio/ogg',
            'spx' => 'audio/ogg',
            'opus' => 'audio/opus',
            'eol' => 'audio/vnd.digital-winds',
            'dts' => 'audio/vnd.dts',
            'dtshd' => 'audio/vnd.dts.hd',
            'lvp' => 'audio/vnd.lucent.voice',
            'pya' => 'audio/vnd.ms-playready.media.pya',
            'ecelp4800' => 'audio/vnd.nuera.ecelp4800',
            'ecelp7470' => 'audio/vnd.nuera.ecelp7470',
            'ecelp9600' => 'audio/vnd.nuera.ecelp9600',
            'wav' => 'audio/vnd.wav',
            'weba' => 'audio/webm',
            'mka' => 'audio/x-matroska',
            'm3u' => 'audio/x-mpegurl',
            'wax' => 'audio/x-ms-wax',
            'wma' => 'audio/x-ms-wma',
            'ra' => 'audio/x-pn-realaudio',
            'ram' => 'audio/x-pn-realaudio',
            'rmp' => 'audio/x-pn-realaudio-plugin',
            'cdx' => 'chemical/x-cdx',
            'cif' => 'chemical/x-cif',
            'cmdf' => 'chemical/x-cmdf',
            'cml' => 'chemical/x-cml',
            'csml' => 'chemical/x-csml',
            'xyz' => 'chemical/x-xyz',
            'woff' => 'font/woff',
            'woff2' => 'font/woff2',
            'gcode' => 'gcode',
            'avif' => 'image/avif',
            'avifs' => 'image/avif',
            'bmp' => 'image/bmp',
            'cgm' => 'image/cgm',
            'g3' => 'image/g3fax',
            'gif' => 'image/gif',
            'heif' => 'image/heic',
            'heic' => 'image/heic',
            'ief' => 'image/ief',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'pjpg' => 'image/jpeg',
            'jfif' => 'image/jpeg',
            'jfif-tbnl' => 'image/jpeg',
            'jif' => 'image/jpeg',
            'jfi' => 'image/pjpeg',
            'png' => 'image/png',
            'btif' => 'image/prs.btif',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            'tif' => 'image/tiff',
            'tiff' => 'image/tiff',
            'psd' => 'image/vnd.adobe.photoshop',
            'djv' => 'image/vnd.djvu',
            'djvu' => 'image/vnd.djvu',
            'dwg' => 'image/vnd.dwg',
            'dxf' => 'image/vnd.dxf',
            'fbs' => 'image/vnd.fastbidsheet',
            'fpx' => 'image/vnd.fpx',
            'fst' => 'image/vnd.fst',
            'mmr' => 'image/vnd.fujixerox.edmics-mmr',
            'rlc' => 'image/vnd.fujixerox.edmics-rlc',
            'mdi' => 'image/vnd.ms-modi',
            'npx' => 'image/vnd.net-fpx',
            'wbmp' => 'image/vnd.wap.wbmp',
            'xif' => 'image/vnd.xiff',
            'webp' => 'image/webp',
            'dng' => 'image/x-adobe-dng',
            'cr2' => 'image/x-canon-cr2',
            'crw' => 'image/x-canon-crw',
            'ras' => 'image/x-cmu-raster',
            'cmx' => 'image/x-cmx',
            'erf' => 'image/x-epson-erf',
            'fh' => 'image/x-freehand',
            'fh4' => 'image/x-freehand',
            'fh5' => 'image/x-freehand',
            'fh7' => 'image/x-freehand',
            'fhc' => 'image/x-freehand',
            'raf' => 'image/x-fuji-raf',
            'icns' => 'image/x-icns',
            'ico' => 'image/x-icon',
            'k25' => 'image/x-kodak-k25',
            'kdc' => 'image/x-kodak-kdc',
            'mrw' => 'image/x-minolta-mrw',
            'nef' => 'image/x-nikon-nef',
            'orf' => 'image/x-olympus-orf',
            'raw' => 'image/x-panasonic-raw',
            'rw2' => 'image/x-panasonic-raw',
            'rwl' => 'image/x-panasonic-raw',
            'pcx' => 'image/x-pcx',
            'pef' => 'image/x-pentax-pef',
            'ptx' => 'image/x-pentax-pef',
            'pct' => 'image/x-pict',
            'pic' => 'image/x-pict',
            'pnm' => 'image/x-portable-anymap',
            'pbm' => 'image/x-portable-bitmap',
            'pgm' => 'image/x-portable-graymap',
            'ppm' => 'image/x-portable-pixmap',
            'rgb' => 'image/x-rgb',
            'x3f' => 'image/x-sigma-x3f',
            'arw' => 'image/x-sony-arw',
            'sr2' => 'image/x-sony-sr2',
            'srf' => 'image/x-sony-srf',
            'xbm' => 'image/x-xbitmap',
            'xpm' => 'image/x-xpixmap',
            'xwd' => 'image/x-xwindowdump',
            'eml' => 'message/rfc822',
            'mht' => 'message/rfc822',
            'mhtml' => 'message/rfc822',
            'mime' => 'message/rfc822',
            'nws' => 'message/rfc822',
            'iges' => 'model/iges',
            'igs' => 'model/iges',
            'mesh' => 'model/mesh',
            'msh' => 'model/mesh',
            'silo' => 'model/mesh',
            'dwf' => 'model/vnd.dwf',
            'gdl' => 'model/vnd.gdl',
            'gtw' => 'model/vnd.gtw',
            'mts' => 'model/vnd.mts',
            'vtu' => 'model/vnd.vtu',
            'vrml' => 'model/vrml',
            'wrl' => 'model/vrml',
            'test' => 'test/mimetype',
            'ics' => 'text/calendar',
            'ifb' => 'text/calendar',
            'css' => 'text/css',
            'csv' => 'text/csv',
            'htm' => 'text/html',
            'html' => 'text/html',
            'js' => 'text/javascript',
            'md' => 'text/markdown',
            'markdown' => 'text/markdown',
            'mdown' => 'text/markdown',
            'markdn' => 'text/markdown',
            'conf' => 'text/plain',
            'def' => 'text/plain',
            'diff' => 'text/plain',
            'in' => 'text/plain',
            'ksh' => 'text/plain',
            'list' => 'text/plain',
            'log' => 'text/plain',
            'text' => 'text/plain',
            'txt' => 'text/plain',
            'dsc' => 'text/prs.lines.tag',
            'rtx' => 'text/richtext',
            'sgm' => 'text/sgml',
            'sgml' => 'text/sgml',
            'tsv' => 'text/tab-separated-values',
            'man' => 'text/troff',
            'me' => 'text/troff',
            'ms' => 'text/troff',
            'roff' => 'text/troff',
            't' => 'text/troff',
            'tr' => 'text/troff',
            'uri' => 'text/uri-list',
            'uris' => 'text/uri-list',
            'urls' => 'text/uri-list',
            'curl' => 'text/vnd.curl',
            'dcurl' => 'text/vnd.curl.dcurl',
            'mcurl' => 'text/vnd.curl.mcurl',
            'scurl' => 'text/vnd.curl.scurl',
            'fly' => 'text/vnd.fly',
            'flx' => 'text/vnd.fmi.flexstor',
            'gv' => 'text/vnd.graphviz',
            '3dml' => 'text/vnd.in3d.3dml',
            'spot' => 'text/vnd.in3d.spot',
            'jad' => 'text/vnd.sun.j2me.app-descriptor',
            'si' => 'text/vnd.wap.si',
            'sl' => 'text/vnd.wap.sl',
            'wml' => 'text/vnd.wap.wml',
            'wmls' => 'text/vnd.wap.wmlscript',
            'asm' => 'text/x-asm',
            's' => 'text/x-asm',
            'c' => 'text/x-c',
            'cc' => 'text/x-c',
            'cpp' => 'text/x-c',
            'cxx' => 'text/x-c',
            'dic' => 'text/x-c',
            'h' => 'text/x-c',
            'hh' => 'text/x-c',
            'f' => 'text/x-fortran',
            'f77' => 'text/x-fortran',
            'f90' => 'text/x-fortran',
            'for' => 'text/x-fortran',
            'java' => 'text/x-java-source',
            'p' => 'text/x-pascal',
            'pas' => 'text/x-pascal',
            'pp' => 'text/x-pascal',
            'inc' => 'text/x-pascal',
            'py' => 'text/x-python',
            'etx' => 'text/x-setext',
            'uu' => 'text/x-uuencode',
            'vcs' => 'text/x-vcalendar',
            'vcf' => 'text/x-vcard',
            'h261' => 'video/h261',
            'h263' => 'video/h263',
            'h264' => 'video/h264',
            'jpgv' => 'video/jpeg',
            'jpgm' => 'video/jpm',
            'jpm' => 'video/jpm',
            'mj2' => 'video/mj2',
            'mjp2' => 'video/mj2',
            'ts' => 'video/mp2t',
            'mpg4' => 'video/mp4',
            'm1v' => 'video/mpeg',
            'm2v' => 'video/mpeg',
            'mpa' => 'video/mpeg',
            'mpe' => 'video/mpeg',
            'mpeg' => 'video/mpeg',
            'mpg' => 'video/mpeg',
            'ogv' => 'video/ogg',
            'mov' => 'video/quicktime',
            'qt' => 'video/quicktime',
            'fvt' => 'video/vnd.fvt',
            'm4u' => 'video/vnd.mpegurl',
            'mxu' => 'video/vnd.mpegurl',
            'pyv' => 'video/vnd.ms-playready.media.pyv',
            'viv' => 'video/vnd.vivo',
            'webm' => 'video/webm',
            'f4v' => 'video/x-f4v',
            'fli' => 'video/x-fli',
            'flv' => 'video/x-flv',
            'mkv' => 'video/x-matroska',
            'asf' => 'video/x-ms-asf',
            'asx' => 'video/x-ms-asf',
            'wm' => 'video/x-ms-wm',
            'wmv' => 'video/x-ms-wmv',
            'wmx' => 'video/x-ms-wmx',
            'wvx' => 'video/x-ms-wvx',
            'avi' => 'video/x-msvideo',
            'movie' => 'video/x-sgi-movie',
            'ice' => 'x-conference/x-cooltalk'
        ];

        return $extensionToMimeType[$extension] ?? 'application/octet-stream';
    }
}
