<?php

use Illuminate\Support\Facades\Log;
use GrogooRestfier\Helpers\Helper;
use Illuminate\Support\Facades\Cache;

if (!function_exists('__tr')) {
    function __tr($key, $replace = [], $locale = null)
    {
        if (!is_string($key)) {
            return $key;
        }
        $currentLocale = app()->getLocale();

        $cacheKey = "{$currentLocale}_{$key}";

        $key = Cache::rememberForever($cacheKey, function () use ($key, $currentLocale) {
            $file = Helper::getPathApp() . "/lang/{$currentLocale}.json";
            $data = file_exists($file) ? json_decode(file_get_contents($file), true) ?? [] : [];
            $translate = $data[$key] ?? null;
            if (null !== $translate) {
                return $translate;
            }

            if (is_writable($file)) {
                $data[$key] = $key;
                ksort($data);
                Helper::saveFile($file, json_encode($data, JSON_UNESCAPED_SLASHES), Helper::OVERWRITE);
            } else {
                Log::error('Translate file is not writable: ' . $file);
            }

            return $key;
        });

        return $key;
    }
}
