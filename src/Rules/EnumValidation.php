<?php

namespace GrogooRestfier\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class EnumValidation implements ValidationRule
{

    private $allowedValues;

    public function __construct(array $allowedValues)
    {
        $this->allowedValues = $allowedValues;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!in_array($value, $this->allowedValues, true)) {
            $fail(__tr('The value is not allowed'));
        }
    }
}
