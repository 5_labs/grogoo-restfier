<?php

    namespace GrogooRestfier\Rules;

    use Illuminate\Contracts\Validation\Rule;

    class CreditCardValidation
    {
        public function validate($attribute, $value, $parameters, $validator)
        {
           // Remove any spaces or non-digit characters from the input
            $value = preg_replace('/\D/', '', $value);

            // Check if the input is empty or not a numeric string
            if (empty($value) || !is_numeric($value)) {
                return false;
            }

            // Reverse the string and convert it to an array of digits
            $digits = str_split(strrev($value));

            $sum = 0;
            $alternate = false;

            foreach ($digits as $digit) {
                $num = intval($digit);

                if ($alternate) {
                    $num *= 2;
                    if ($num > 9) {
                        $num -= 9;
                    }
                }

                $sum += $num;
                $alternate = !$alternate;
            }

            // The card number is valid if the sum is a multiple of 10
            return ($sum % 10) === 0;
        }

        public function message()
        {
            return 'The :attribute is not a valid credit card number.';
        }
    }
