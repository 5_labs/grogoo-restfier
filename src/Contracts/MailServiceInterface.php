<?php

namespace GrogooRestfier\Contracts;


interface MailServiceInterface
{
    public function send(string $toAddress, string $toName, string $subject, string $body, array $config = []): object;
}
