<?php

namespace GrogooRestfier\Builder;

use GrogooRestfier\Helpers\Helper;

class LoadCommands
{

    public static function load(): array
    {
        return [
            Helper::getPathApp() . '/app/Console/Commands',
            realpath(__DIR__ . '/../../app/Console/Commands')
        ];
    }
}
