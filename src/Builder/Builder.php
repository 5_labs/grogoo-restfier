<?php

namespace GrogooRestfier\Builder;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use GrogooRestfier\Builder\Creators\Postman;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Helpers\ConfigPersisted;
use GrogooRestfier\Helpers\ConsoleTable;
use GrogooRestfier\Helpers\Helper;
use SebastianBergmann\Diff\ConfigurationException;

class Builder
{

    public static $config = [];

    public static function run()
    {

        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }

        $config = self::getConfig();

        Helper::commandPrintHeader('Grogoo RESTfier');

        $data = new Data();
        self::setNamespaceToModel($config, $data);
        // var_export($data->getData()->namespaces);
        $tables = array_values(array_map(fn($item) => $item->table, (array) $data->getData()->items));

        // Remover arquivos mapeados como sempre default
        self::alwaysDefault($config->alwaysDefault);

        // Inicar a documentacao postman
        $postman = new ConfigPersisted('postman');
        $initial = json_decode(file_get_contents(__DIR__ . '/Creators/Postman/collection.json'), true);
        // unset($initial['info']);
        $postman->init(['collection' => $initial]);


        // Builder
        $creators = [
            \GrogooRestfier\Builder\Creators\DefaultCommands::class,
            \GrogooRestfier\Builder\Creators\Request::class,
            \GrogooRestfier\Builder\Creators\Postman::class,
            // \GrogooRestfier\Builder\Creators\Migration::class,
            \GrogooRestfier\Builder\Creators\Provider::class,
            \GrogooRestfier\Builder\Creators\Model::class,
            \GrogooRestfier\Builder\Creators\Resource::class,
            \GrogooRestfier\Builder\Creators\Controller::class,
            \GrogooRestfier\Builder\Creators\Router::class,
            \GrogooRestfier\Builder\Creators\Translate::class,
            \GrogooRestfier\Builder\Creators\Tests::class,
            \GrogooRestfier\Builder\Creators\Factory::class,
            \GrogooRestfier\Builder\Creators\Seeder::class,
            \GrogooRestfier\Builder\Creators\VerifyBelongToRule::class,
            \GrogooRestfier\Builder\Creators\RulesJsonPrepared::class,
            \GrogooRestfier\Builder\Creators\CreateEvent::class,

        ];

        // Rodar pelos modules
        $filesTotal = 0;
        foreach ($config->modules as $item) {
            echo "\n";
            echo "\r- Module $item->module: ";
            $line = [$item->module];
            $data->set('module', ucwords(Helper::name2CamelCase($item->module)));
            $data->set('ignoreFieldsToFillable', $item->ignoreFieldsToFillable ?? null);
            $data->set('filterable', $item->filterable ?? []);
            $data->set('seedIgnore', $item->seedIgnore ?? []);

            // Ignore routers
            $ignoreRoutersModule = array_merge(
                (array) ($item->ignoreRouters ?? []),
                [
                    'companies' => array_merge((array) ($item->ignoreRouters->companies ?? []), ['store', 'destroy']),
                    'users' => array_merge((array) ($item->ignoreRouters->users ?? []), ['store', 'destroy'])
                ]
            );

            // Ignore Controllers
            $ignoreController = array_merge(
                array_values(array_filter($tables, fn($i) => array_search($i, $item->tables) === false)),
                ['jobs', 'failed_jobs', 'migrations', 'password_reset_tokens', 'personal_access_tokens', 'jwt_tokens', 'spatial_ref_sys']
            );

            // Execution in module
            $moduleTotalFiles = 0;
            foreach ($creators as $key => $creatorClass) {
                $creatorInstance = new $creatorClass();

                if ($creatorInstance instanceof \GrogooRestfier\Builder\Creators\Router) {
                    $files = $creatorInstance->handle($data, $ignoreController, $ignoreRoutersModule);
                } else {
                    $files = $creatorInstance->handle($data, $ignoreController);
                }
                $line[] = $files . ' files';
                $moduleTotalFiles += $files;

                // Execution
                echo "\r- Module $item->module: " . round(($key + 1) / count($creators) * 100) . '%';
            }
            $filesTotal += $moduleTotalFiles;
            echo "\r- Module $item->module: $moduleTotalFiles files";
            // $console->addRow($line);
        }

        // gerar documentação postman
        $saved = Helper::getPathApp() . '/.postman.collection.json';
        Helper::saveFile(
            $saved,
            str_replace(
                [
                    'API-NAME',
                    'http://localhost:8000/api'
                ],
                [
                    ucwords(getenv('APP_NAME')) . ' - API',
                    "http://localhost:" . getenv('APP_PORT_EXPOSED') . "/api"
                ],
                json_encode(
                    $postman->get()['collection'],
                    JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                )
            ),
            'SOBREPOR'
        );
        $postman->remove();

        // Gerar o HTML
        echo "\n- Generate HTML from API Documentation:";
        echo str_replace('Generating output... ', ' ', Postman::generateHTML($saved));

        echo "\n";
        echo "\nTotal: $filesTotal files in modules";
        echo "\n";
        // $console->display();
        echo "\n ";
    }


    private static function alwaysDefault($files): void
    {
        // Recriar o arquivo de mapeamento do grogoo. É recriado na Create/Model
        // unlink(Helper::getPathApp() . '/config/grogoo.php');
        Helper::saveFile(Helper::getPathApp() . '/config/grogoo.php', "<?php\n\n // This content is generated automatic. Do not edit.\n\n return [];", Helper::OVERWRITE);

        // Remover os arquivos enviados como padrão
        $files[] = '/app/Generated';
        array_map(function ($item) {
            $item = Helper::getPathApp()
                . DIRECTORY_SEPARATOR
                . ($item[0] === '/' ? substr($item, 1) : $item);

            if (is_dir($item)) {
                Helper::deleteDirectory($item);
            } elseif (file_exists($item)) {
                unlink($item);
            } else {
                return false;
            }
            return true;
        }, $files);
    }

    private static function getConfig($extras = []): object
    {
        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }
        $config = json_decode(file_get_contents($fileconfig), true);

        // Defaults
        $config['modules'][] =  [
            "module" => "Company",
            "ignoreRouters" => [
                "companies" => [
                    "store",
                    "destroy"
                ],
                "permissions" => true,
                "users" => [
                    "store",
                    "destroy"
                ],
                "profile_permissions" => ["update"]
            ],
            "tables" => [
                "companies",
                "permissions",
                "profile_permissions",
                "profiles",
                "user_profile",
                "users",
                "invites",
                "config",
                "customers",
                "apikeys"
            ],
            "ignoreFieldsToFillable" => [
                "profiles" => ['is_root'],
                "apikeys" => [
                    "user_id",
                    "last_usage_date",
                    "last_usage_ip",
                    "usage_hits"
                ]
            ],
            "filterable" => [
                'customers' => [
                    'name' => 'nullable|string',
                    'email' => 'nullable|string',
                ]
            ],
            "seedIgnore" => [
                // 'table' => ['fields']
            ]
        ];

        $config['modules'][] =   [
            "module" => "Subscription",
            "tables" => [
                "plans",
                "subscriptions"
            ]
        ];

        $config['modules'][] = [
            "module" => "Application",
            "tables" => [
                "files",
                "addresses",
                "webhooks",
                "queues"
            ],
            "ignoreRouters" => [
                'webhooks' => ['show', 'index', 'update', 'destroy'],
                'queues' => true
            ],
            "ignoreFieldsToFillable" => [
                "files" => ['content']
            ],
            "seedIgnore" => ['queues', 'webhoooks']
        ];

        $config = json_decode(json_encode($config));


        // validate
        $error = 'ERROR: The configuration file is invalid: ';
        switch (true) {
            case !isset($config->alwaysDefault) || !is_array($config->alwaysDefault):
                throw new Exception($error . "key alwaysDefault is not found or not is an array");
            case !isset($config->modules) || !is_array($config->modules):
                throw new Exception($error . "key 'modules' is not found or not is an array");
            case !isset($config->modules[0]->module):
                throw new Exception($error . "key 'modules->module' is not found");
            case !isset($config->modules[0]->tables) || !is_array($config->modules[0]->tables):
                throw new Exception($error . "key 'modules->tables' is not found or not is an array");
        }

        self::$config = $config;
        return $config;
    }

    /**
     * Ira aplicar o namespace nos dados obtidos por data e conforme configurações
     *
     * @param object $config
     * @param Data $data
     * @return void
     */
    private static function setNamespaceToModel(object $config, Data $data): void
    {
        // echo "\n- Preparing namespaces";
        $reference = [];
        foreach ($data->getData()->items as $key => $item) {
            // echo $item->table . ": ";
            $module = array_values(
                array_filter(
                    $config->modules,
                    fn($i) => array_search($item->table, $i->tables) !== false
                )
            );

            if (isset($module[0])) {
                $reference[$item->table] = $module[0]->module;
            }
            // echo PHP_EOL;
        }
        $data->set('namespaces', $reference);
    }
}
