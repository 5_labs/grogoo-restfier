<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class CreateEvent implements CreateInterface
{


    private $template = "<?php

    namespace App\Generated\Modules\{module}\Events\Create;

    use App\Modules\{module}\Models\{model};

    use Illuminate\Queue\SerializesModels;
    use Illuminate\Foundation\Events\Dispatchable;
    use Illuminate\Broadcasting\InteractsWithSockets;
    {importAbstractEvent}

    abstract class Abstract{model}CreateEvent
    {
        use Dispatchable, InteractsWithSockets, SerializesModels{usesAudit};

        public {model} \${model_lower};

        /**
         * Create a new event instance.
         */
        public function __construct({constructParam})
        {
            \$this->{model_lower} = \${model_lower};
            {setAuditAtribute}
        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Events\Create;

    use App\Generated\Modules\{module}\Events\Create\Abstract{model}CreateEvent;

    final class {model}CreateEvent extends Abstract{model}CreateEvent
    {
    }
    ";

    public function handle(Data $data, array $ignore = []): int
    {
        $module = $data->getData()->module;

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            $model_lower = mb_strtolower($item->model);
            $auditEnabled = $this->auditEnabled();

            $update = [
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'model_lower' => $model_lower,
                'constructParam' => $auditEnabled ? "$item->model \$$model_lower, \$user_dispatcher = null" : "$item->model \$$model_lower",
                'usesAudit' => $auditEnabled ? ", AbstractEvent" : "",
                'importAbstractEvent' => $auditEnabled ? "    use GrogooRestfier\Modules\Audit\Events\AbstractEvent;" : "",
                'setAuditAtribute' => $auditEnabled ? "\$this->user_dispatcher = \$user_dispatcher;" : "",
            ];

            SaveFile::save(
                self::class,
                $module,
                $item->model,
                new Template($this->template, $update),
                new Template($this->templateModule, $update)
            );
        }

        return SaveFile::countGeneratedFiles(self::class, $module);
    }

    public function auditEnabled(): bool
    {
        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }
        $config = json_decode(file_get_contents($fileconfig), true);

        if (!isset($config['audit'])) {
            return false;
        }

        return $config['audit'];
    }
}
