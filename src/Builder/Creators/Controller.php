<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Controller implements CreateInterface
{

    private ?array $data = null;
    private $template = '';
    private $templateModule = "<?php

    namespace App\Modules\{module}\Http\Controllers;

    use App\Generated\Modules\{module}\Http\Controllers\Abstract{model}Controller;

    /**
     * Class {model}Controller
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}Controller extends Abstract{model}Controller
    {
        /*
        public function __construct() {

            // example of function to treat the data before cloning
            // \$this->cloneRules = function (...\$params) : array {
            //     \$originalData = \$params[0];
            //     \$request = \$params[1];
            //     return \$originalData;
            // };

            parent::__construct();
        }
        */
    }    
    ";

    private function setData(Data $data)
    {
        if ($this->data === null) {
            $this->data = json_decode(json_encode($data->getData()), true);
        }
    }
    private function checkIfHasSoftdelete(Data $data, string $table)
    {
        $this->setData($data);

        // obter dataset
        $dataset = array_values(array_filter($this->data['items'], fn($item) => $item['table'] === $table));

        // obter campos que se aplicam
        $softdeletes = array_filter((array) $dataset[0]['fields'], fn($item) => ($item['column_name'] ?? '-') === 'deleted_at');

        return !empty($softdeletes);
    }


    public function handle(Data $data, array $ignore = []): int
    {

        $module = $data->getData()->module;

        $typesNotSortable = ['text', 'json', 'jsonb', 'bool', 'boolean'];
        $fieldsNotSortable = ['deleted_at', 'password'];
        $fieldNotSearchable = ['password'];
        $fieldsFilterable = (array) $data->getData()->filterable ?? [];

        foreach ($data->getData()->items as $item) {
            $searchable = $sortable = $joins = $softDeletes = [];
            $conditions = [];
            $getConditions = [];
            $companyIdIsFinded = false;
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = ['relations' => [], 'items' => [], 'loaders' => []];


            // Caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table);
            if (count($routers['router']) > 0) {
                $baseCondition = null;
                foreach ($routers['router'] as $router) {
                    $relations = explode('.', $router);
                    array_pop($relations);
                    list($table, $cpoID, $cpoRel) = explode('|', end($relations));


                    $rels = array_values(array_filter($relations, fn($item) => $item !== 'companies'));
                    $last = explode('|', array_pop($rels));
                    $prev = explode('|', array_pop($rels));
                    $tbl = strlen($prev[0]) > 0 ? $prev[0] : $item->table;
                    $check = HelperCreator::checkIfFieldIsNullable($data, $tbl, $last[2]);

                    if ($check->exists) {
                        if ($check->nullable === false) {
                            $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->input('jwt_company_id');";
                        }
                    } else {
                        $conditions["$table.company_id"] = "\$this->conditions['$table.company_id'] = (int) \$request->input('jwt_company_id');";
                    }

                    $lastTable = $item->table;

                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') !== false) {
                            list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                            if (!isset($joins[$table])) {
                                $check = HelperCreator::checkIfFieldIsNullable($data, $item->table, $cpoRel);
                                $joinType = $check->exists && $check->nullable ? 'leftJoin' : 'join';
                                if ($joinType === 'join') {
                                    $joins[$table] = "->$joinType('$table', '$table.$cpoID', '=', '$lastTable.$cpoRel')";
                                    // Soft delete check: se existir o campo deleted_at, precisa ser incluido na verficação pois a cadeia de exclusao do banco não ira remover
                                    if ($this->checkIfHasSoftdelete($data, $table)) {
                                        $softDeletes[$table . '_softDeletes'] = "->whereNull('$table.deleted_at')";
                                    }
                                } else {
                                    $joins[$table] = "->$joinType('$table', fn(\$join) => \$join->on('$table.$cpoID', '=', '$lastTable.$cpoRel')"
                                        . ($this->checkIfHasSoftdelete($data, $table)
                                            ? "->whereNull('$table.deleted_at')"
                                            : '')
                                        . ')';
                                }
                            }
                            $tableSingular = Helper::name2KebabCase(Helper::singularize($table));
                            $filter = Helper::singularize($table) . '_id';
                            $getConditions[$tableSingular] = "\$this->conditions['$table.$cpoID'] = \$request->input('$filter') ?? \$request->route('$tableSingular') ?? null;  ";
                            $lastTable = $table;
                        }
                    }
                }
            }


            // foreach ($item->hasMany as $hasMany) {
            //     // $tableItens['loaders'][$hasMany->table] = "'" . Helper::name2CamelCase($hasMany->table) . "'";
            // }

            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    if (!isset($conditions['company_id']) && $field->field_origin === 'company_id') {
                        $tbl = $field->table === 'companies' ? $item->table : $field->table;
                        $conditions['company_id'] =
                            "\$this->conditions['$tbl.company_id'] = (int) \$request->input('jwt_company_id');";
                        $companyIdIsFinded = true;
                    }

                    // Join com a relação direta desta tabela
                    if (!isset($joins[$field->table]) && $field->model !== 'Company') {

                        $joinType = $field->nullable ? 'leftJoin' : 'join';

                        if ($joinType === 'join') {
                            $joins[$field->table] = "->$joinType('$field->table', '$field->table.$field->field_relation', '=', '$item->table.$field->field_origin')";
                            // Soft delete check: se existir o campo deleted_at, precisa ser incluido na verficação pois a cadeia de exclusao do banco não ira remover
                            if ($this->checkIfHasSoftdelete($data, $field->table)) {
                                $softDeletes[$field->table . '_softDeletes'] = "->whereNull('$field->table.deleted_at')";
                            }
                        } else {
                            $joins[$field->table] = "->$joinType('$field->table', fn(\$join) => \$join->on('$field->table.$field->field_relation', '=', '$item->table.$field->field_origin')"
                                . ($this->checkIfHasSoftdelete($data, $field->table)
                                    ? "->whereNull('$field->table.deleted_at')"
                                    : '')
                                . ')';
                        }


                        // $joins[$field->table] = "->$joinType('$field->table', '$field->table.$field->field_relation', '=', '$item->table.$field->field_origin')";
                    }

                    if ($field->model !== 'Company') {
                        $tableSingular = Helper::name2KebabCase(Helper::singularize($field->table));
                        $filter = Helper::singularize($field->table) . '_id';
                        $getConditions[$tableSingular] = "\$this->conditions['{$field->table}.id'] = \$request->input('$filter') ?? \$request->route('$tableSingular') ?? null;  ";
                    }


                    $tableItens['relations_relation'][$field->field_origin] = '$' . "{$field->model}List = \App\\Http\\Modules\\" . $module . "\\Resources\\{$field->model}Resource::collection(\$this->$field->model);";
                    $tableItens['items_relation'][$field->field_origin] = '$' . "{$field->model}List";

                    if ($field->model !== 'Company') {
                        $tableItens['loaders'][lcfirst($field->model)] = "'" . lcfirst($field->model) . "'";
                    }
                    continue;
                }

                // sortable
                if (
                    array_search(LoadDB::$config[$field->type][1], $typesNotSortable) === false
                    && array_search($field->column_name, $fieldsNotSortable) === false
                    && stripos($field->type, 'tsvector') === false
                ) {
                    $sortable[] = "'$field->column_name'";
                }

                // searchable
                if (
                    (LoadDB::$config[$field->type][1] === 'string' || LoadDB::$config[$field->type][1] === 'text')
                    && array_search($field->column_name, $fieldsNotSortable) === false
                ) {
                    $searchable[] = "'$field->column_name'";
                }


                if ($field->isPrimaryKey || stripos($field->column_name, 'createtime') !== false || stripos($field->column_name, 'created_at') !== false) {
                    continue;
                }

                // items
                $tableItens['items'][$field->column_name] = '$this->' . $field->column_name;
            }

            $tableItens['relations'] = array_merge($tableItens['relations'], ($tableItens['relations_relation'] ?? []));
            $tableItens['items'] = array_merge($tableItens['items'], ($tableItens['items_relation'] ?? []));

            // Propria Company
            if ($item->model === 'Company') {
                $conditions[] = "\$this->conditions['id'] = (int) \$request->input('jwt_company_id');";
            }
            // Proprio usuario
            if ($item->model === 'User') {
                $conditions[] = "\$this->conditions['users.id'] = (int) \Illuminate\Support\Facades\Auth::user()->id;";
            }

            // Caso não tenha loaders, remvoer as referencias
            $CODEERROR = Helper::generateCode($item->model) . '300';

            $update = [
                'construct' => '',
                'sortable' => implode(", ", $sortable),
                'searchable' => implode(", ", $searchable),
                'filterable' => HelperCreator::arrayToString((array) ($fieldsFilterable[$item->table] ?? [])),
                'joins' => implode("\n", array_merge($joins, $softDeletes)),
                'conditions' => implode("\n", $conditions),
                'getConditions' => implode("\n", $getConditions),
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'route' => Helper::name2KebabCase(Helper::singularize($item->table)),
                'routeID' => Helper::singularize($item->table),
                'relations' => implode("\n", array_map(fn($key, $item) => $item, array_keys($tableItens['relations']), array_values($tableItens['relations']))),
                'items' => implode(",\n", array_map(fn($item, $key) => "'$key'=>$item", array_values($tableItens['items']), array_keys($tableItens['items']))),
                'loaders' => implode(",", $tableItens['loaders']),
                'errorCode' => $CODEERROR
            ];


            // Definição do template
            $include = __DIR__ . "/Templates/Controller/$item->model.php";
            $this->template = file_exists($include) ? include $include : include __DIR__ . "/Templates/Controller/Default.php";
            $this->template = str_replace(['//{extras}', '//{extrasUses}'], ['', ''], $this->template);

            $template = count($tableItens['loaders']) === 0
                ? str_replace(
                    [
                        "\$item->load({loaders});",
                        "\$list->with({loaders});",
                        "{joins}\n",
                        "->with({loaders})"
                    ],
                    [
                        '',
                        '',
                        '',
                        ''
                    ],
                    $this->template
                )
                : $this->template;

            $template = count($conditions) === 0 && $item->table !== 'permissions'
                ? str_replace(
                    ["{construct}"],
                    [
                        "//Table not linked to a company. API access not available.\n
                    if (!\Illuminate\Support\Facades\App::runningInConsole()) {
                       \$this->hasPermissionToResource = false;\n
                    }"
                    ],
                    $template
                )
                : $template;

            // New process to save
            SaveFile::save(self::class, $module, $item->model, new Template($template, $update), new Template($this->templateModule, $update));
        }
        return SaveFile::countGeneratedFiles(self::class, $module);

        // return count(scandir($pathToSave)) - 2;
    }
}
