<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class VerifyBelongToRule implements CreateInterface
{

    private $template = "<?php

    namespace App\Generated\Modules\{module}\Rules\{model};
    
    use Illuminate\Http\Request;
    use GrogooRestfier\Helpers\Helper;
    
    abstract class Abstract{model}VerifyBelongToRule
    {
        protected \$request;

        public function __invoke(Request \$request): Request
        {
            \$this->request = \$request;
            \$this->inject();
            \$this->rules();
            return \$this->request;
        }

        protected function rules() : void {
            {rules}
        }

        protected function inject() : void {
            \$merge = [];
            {injection}
            \$this->merge(\$merge);
        }

        protected function merge(array \$merge) : void {
            \$toMerge = array_filter(
                \$merge,
                function (\$item, \$key) {
                    // Verificar se o parâmetro de rota foi enviado
                    \$route = str_replace(['_id', '_'], ['', '-'], \$key);
                    \$routeParamExists = \$this->request->route(\$route) !== null;

                    return \$routeParamExists && (\$this->request->method() === 'GET'
                        ? \$item >= 0
                        : \$item > 0
                    );
                },
                ARRAY_FILTER_USE_BOTH
            );

            if (count(\$toMerge) > 0) {
                \$this->request->merge(\$toMerge);
            }
        }
    }

    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Rules\{model};

    use App\Generated\Modules\{module}\Rules\{model}\Abstract{model}VerifyBelongToRule;
    use Illuminate\Http\Request;

    /**
     * Class {model}VerifyBelongToRule
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}VerifyBelongToRule extends Abstract{model}VerifyBelongToRule
    {
        public function __invoke(Request \$request): Request
        {
            return parent::__invoke(\$request);
        }
    }
    ";


    private function getTest($model, $table, $cpoid, $joins, $coderror, $tableToCompany, $checkJoin)
    {
        $checkjoin = implode(",\n", $checkJoin);
        return "
        // Relation field: $cpoid
        \$checkJoin = [$checkjoin];
        \$exists = $model::where('$table.id', (int) \$this->request->input('$cpoid'))
        $joins
        ->select('$table.id')
        ->where('$tableToCompany.company_id', '=', \$this->request->input('jwt_company_id'))
        ;

        foreach(\$checkJoin as \$check) {
            if ((int) \$this->request->input(\$check[1])>0) {
                \$exists->where(\$check[0], '=', \$this->request->input(\$check[1]));
            }
        }

        if (null !== \$this->request->input('$cpoid') && !\$exists->exists())
        {
            throw new \Exception('The $cpoid is not valid. (VLT-$coderror)', 422);
        }
         ";
    }

    private function getTestNullable($model, $table, $cpoid, $joins, $coderror, $tableToCompany, $checkJoin)
    {
        $checkjoin = implode(",\n", $checkJoin);
        return "
        // Relation field: $cpoid
        if (null !== \$this->request->input('$cpoid')) {
        \$checkJoin = [$checkjoin];
        \$exists = $model::where('$table.id', (int) \$this->request->input('$cpoid'))
        $joins
        ->select('$table.id')
        ->where('$tableToCompany.company_id', '=', \$this->request->input('jwt_company_id'))
        ;

        foreach(\$checkJoin as \$check) {
            if ((int) \$this->request->input(\$check[1])>0) {
                \$exists->where(\$check[0], '=', \$this->request->input(\$check[1]));
            }
        }

        if (null !== \$this->request->input('$cpoid') && !\$exists->exists())
        {
            throw new \Exception('The $cpoid is not valid. (VLT-$coderror)', 422);
        }
    }
         ";
    }

    private function getTestLastNode($coderror)
    {
        return "
        if (null !== \$this->request->input('company_id') 
            && !\App\Modules\Company\Models\Company::where('id', (int) \$this->request->input('company_id')
        )
            ->select('companies.id')
            ->where('companies.id', '=', \$this->request->input('jwt_company_id'))
            ->exists()) {
            throw new \Exception('The company_id is not valid. (VLT-$coderror)', 422);
        }

";
    }



    public function handle(Data $data, array $ignore = []): int
    {
        $middlewares = [];
        $module = $data->getData()->module;

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $conditions = [];

            // Injection by router
            $conditions = [];
            $routers = HelperCreator::getRoutes($data, $item->table);
            if (count($routers['router']) > 0) {
                $baseCondition = null;
                foreach ($routers['router'] as $router) {
                    $relations = explode('.', $router);
                    array_pop($relations);
                    list($table, $cpoID, $cpoRel) = explode('|', end($relations));
                    $lastTable = $item->table;
                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') === false) {
                            continue;
                        }
                        list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                        $tableSingular = Helper::singularize($table);
                        $conditions[$tableSingular] = "\$merge['$cpoRel'] = (int) \$this->request->route('$tableSingular');";
                        $lastTable = $table;
                    }
                }
            }

            // belongTo - campos notnull
            $rules = [];
            if (count($routers['router']) > 0) {
                foreach ($routers['router'] as $key =>  $router) {
                    $relations = explode('.', $router);

                    // Obter a tabela relacionada com company. Avaliar se o campo relacionado é nullable e ignorar caso seja
                    array_pop($relations);
                    list($tableA, $cpoIDA, $cpoRelA) = explode('|', end($relations));

                    // Obter a primeira tabela para buscar o ID
                    list($tableWhere, $cpoIDWhere, $cpoRelWhere) = explode('|', $relations[0]);
                    array_shift($relations);

                    $lastTable = $tableWhere;
                    $joins = [];
                    $checkJoin = [];
                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') === false) {
                            continue;
                        }
                        list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                        $joins[$lastTable . $cpoRel] = "->join('$table', '$table.$cpoID', '=', '$lastTable.$cpoRel')";
                        $checkJoin[] = "['$table.$cpoID', '$cpoRel']";

                        $lastTable = $table;
                    }

                    $CODEERROR = Helper::generateCode($item->table) . '-087' . $key;

                    $rules[$cpoRelWhere] = $this->getTest(
                        HelperCreator::getNameSpace($data, $tableWhere, '\\Models\\') . ucwords(Helper::name2CamelCase(Helper::singularize($tableWhere))),
                        $tableWhere,
                        $cpoRelWhere,
                        implode("\n", $joins),
                        $CODEERROR,
                        $tableA,
                        $checkJoin,
                    );
                }
            }


            // Campos marcados como nullable
            if (count($routers['nullable']) > 0) {
                foreach ($routers['nullable'] as $key =>  $router) {
                    $relations = explode('.', $router);

                    // Obter a tabela relacionada com company. Avaliar se o campo relacionado é nullable e ignorar caso seja
                    array_pop($relations);
                    list($tableA, $cpoIDA, $cpoRelA) = explode('|', end($relations));

                    // Obter a primeira tabela para buscar o ID
                    list($tableWhere, $cpoIDWhere, $cpoRelWhere) = explode('|', $relations[0]);
                    array_shift($relations);

                    $lastTable = $tableWhere;
                    $joins = [];
                    $checkJoin = [];
                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') === false) {
                            continue;
                        }
                        list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                        $joins[$lastTable . $cpoRel] = "->leftJoin('$table', '$table.$cpoID', '=', '$lastTable.$cpoRel')";
                        $checkJoin[] = "['$table.$cpoID', '$cpoRel']";

                        $lastTable = $table;
                    }

                    $CODEERROR = Helper::generateCode($item->table) . '-088' . $key;

                    $rules[$cpoRelWhere] = $this->getTestNullable(
                        HelperCreator::getNameSpace($data, $tableWhere, '\\Models\\') . ucwords(Helper::name2CamelCase(Helper::singularize($tableWhere))),
                        $tableWhere,
                        $cpoRelWhere,
                        implode("\n", $joins),
                        $CODEERROR,
                        $tableA,
                        $checkJoin,
                    );
                }
            }


            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    if (!isset($conditions['company_id']) && $field->field_origin === 'company_id') {
                        $rules[] = $this->getTestLastNode(Helper::generateCode($item->table) . '-999');
                    }
                }
            }

            $update = [
                'rules' => implode("\n", $rules),
                'construct' => '',
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'injection' => implode("\n", $conditions)
            ];

            SaveFile::save(self::class, $module, $item->model, new Template($this->template, $update), new Template($this->templateModule, $update));

            $middlewares[$module] = "'$module.check.belong.to' => \\App\\Modules\\$module\\Http\\Middlewares\\CheckBelongTo::class,";
        }


        // Midleware
        $tmpGenerated = "<?php

        namespace App\Generated\Modules\{module}\Http\Middlewares;
        
        use Closure;
        use Illuminate\Http\Request;
        use GrogooRestfier\Helpers\Helper;
        use Illuminate\Support\Facades\File;
        use Illuminate\Support\Facades\Cache;
        
        abstract class AbstractCheckBelongTo
        {
            /**
             * Handle an incoming request.
             *
             * @param  \Illuminate\Http\Request  \$request
             * @param  \Closure  \$next
             * @return mixed
             */
            public function handle(Request \$request, Closure \$next)
            {
                // Check if the HTTP method is POST or PUT
                //if (in_array(\$request->method(),  ['POST', 'PUT'])) {
    
                    \$routeName = \$request->route()->getName();
                    \$cacheKey = \"CheckBelongTo-\$routeName\";
                    \$rules = Cache::rememberForever(\$cacheKey, function () use (\$routeName) {
                        \$parts = explode('.', \$routeName);
                        array_pop(\$parts);
                        \$rules = [];
                        foreach (\$parts as \$route) {
                            \$modelName = ucwords(Helper::name2CamelCase(\$route));

                            list(\$table, \$module) = config('grogoo.models')[\$modelName];

                            // Build the name of the verification rule based on the route name
                            \$model = \"\\\\App\\\\Modules\\\\\$module\\\\Rules\\\\\$modelName\";

                                // Procurar por todos os arquivos de rules desta model
                                \$files = File::files(base_path() . \"/app/Modules/\$module/Rules/\$modelName\");
                                foreach (\$files as \$file) {
                                    \$filename = pathinfo(\$file, PATHINFO_FILENAME);
                                    \$rule = \$model . '\\\'. \$filename;
                                    // Call the verification rule to check if the user has access to the resource
                                    if (class_exists(\$rule)) {
                                        \$rules[] = \$rule;
                                    }                           
                                }
                        }
                        return \$rules;
                    });

                    foreach (\$rules as \$rule) {
                        \$request = (new \$rule())(\$request);
                    }
                //}
        
                // Continue with the request
                return \$next(\$request);
            }
        }
        ";

        $tmpModule = "<?php

        namespace App\Modules\{module}\Http\Middlewares;

        use App\Generated\Modules\{module}\Http\Middlewares\AbstractCheckBelongTo;
                
        final class CheckBelongTo extends AbstractCheckBelongTo
        {

        }
        ";


        // (new Template($tmp, ['module' => $module]))
        //     ->renderTo(Helper::getPathApp() . "/app/Modules/$module/Http/Middlewares/CheckBelongTo.php", Helper::IF_NOT_UPDATE);



        SaveFile::save(
            'Middlewares',
            $module,
            'CheckBelongTo',
            new Template($tmpGenerated, ['module' => $module]),
            new Template($tmpModule, ['module' => $module])
        );



        // return count(scandir($pathToSave)) - 2;
        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
