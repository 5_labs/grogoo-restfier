<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Resource implements CreateInterface
{

    private $template = '';

    private $templateModule = "<?php

    namespace App\Modules\{module}\Http\Resources;

    use App\Generated\Modules\{module}\Http\Resources\Abstract{model}Resource;
    use Illuminate\Http\Request;
    use Illuminate\Http\Resources\Json\JsonResource;


    /**
     * Class {model}Resource
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}Resource extends Abstract{model}Resource
    {
        /**
         * Transform the resource into an array.
         *
         * @return array<string, mixed>
         */
        public function toArray(Request \$request): array
        {
            \$data = [];
            if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
                \$data = array_merge(
                    parent::toArray(\$request),
                    []
                );
            }

            return \$data;
        }
    }
    ";



    public function handle(Data $data, $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Http/Resources';
        // echo "- Resource: ";

        $module = $data->getData()->module;
        $ignoreFillable = $data->getData()->ignoreFieldsToFillable ?? null;

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = ['relations' => [], 'items' => []];

            // Has many
            foreach ($item->hasMany as $hasMany) {
                // $tableItens['items_relation'][Helper::name2CamelCase($hasMany->table)] = HelperCreator::getNameSpace($data, $hasMany->table, '\\Http\Resources\\') . "{$hasMany->model}Resource::collection(\$this->whenLoaded('" . Helper::name2CamelCase($hasMany->table) . "'))";
            }

            foreach ($item->fields as $field) {
                // Belong to
                if (!isset($field->type)) {
                    if ($field->model !== "Company") {
                        $singularize = Helper::singularize($field->table);

                        $tableItens['items_relation'][$singularize] = HelperCreator::getNameSpace($data, $field->table, '\\Http\Resources\\') . "{$field->model}Resource::make(\$this->whenLoaded('" . lcfirst($field->model) . "'))";

                        // $tableItens['items_relation'][$field->field_origin] = '$this->' . $field->field_origin;
                    }
                    continue;
                }

                if (
                    // $field->isPrimaryKey
                    // stripos($field->column_name, 'createtime') !== false
                    // || stripos($field->column_name, 'created_at') !== false
                    // || stripos($field->column_name, 'updated_at') !== false
                    stripos($field->column_name, 'deleted_at') !== false
                    || stripos($field->column_name, 'password') !== false
                    || stripos($field->column_name, 'remember_token') !== false
                    || stripos($field->type, 'tsvector') !== false
                    || stripos($field->column_name, 'email_verified_at') !== false

                ) {
                    continue;
                }

                // items
                // Type
                switch (LoadDB::$config[$field->type][1]) {
                    // case 'jsonb':
                    // case 'json':
                    //     $tableItens['items'][$field->column_name] = 'json_decode($this->' . $field->column_name . ')';
                    //     break;
                    default:
                        $tableItens['items'][$field->column_name] = '$this->' . $field->column_name;
                }
            }

            $tableItens['relations'] = array_merge($tableItens['relations'], ($tableItens['relations_relation'] ?? []));
            $tableItens['items'] = array_merge($tableItens['items'], ($tableItens['items_relation'] ?? []));

            // Regras especificas
            switch ($item->model) {
                case 'Invite':
                    $tableItens['items']['company'] = '[\'name\' => $this->company_name, \'email\' => $this->company_email]';
                    $tableItens['items']['user_profile'] = '$this->getUserProfile($this->profile_id, $this->email)';
                    break;
                case 'User':
                    $tableItens['items']['is_root'] = 'isset($this->is_root) ? $this->is_root : $this->isRoot($request)';
                    break;
                case 'Profile':
                    $tableItens['items']['total_users'] = '$this->total_users';
                    break;
            }

            // campos condicionais
            $conditional_fields = [];
            if ($item->model === 'Company') {
                $conditional_fields[] = ' if ($request->route()->getName() === "auth.login" || $request->route()->getName() === "auth.switchSet" || $request->route()->getName() === "auth.switch") { $data["canTrial"] = $this->canTrial;$data["isRoot"] = $this->isRoot;}';
            }

            // check restriction to fillable
            if (
                [] !== ($ignoreFillable->{$item->table} ?? [])
            ) {
                foreach ($ignoreFillable->{$item->table} as $excluded) {
                    unset($tableItens['items'][$excluded]);
                }
            }

            $update = [
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'relations' => implode("\n", array_map(fn($key, $item) => $item, array_keys($tableItens['relations']), array_values($tableItens['relations']))),
                'items' => implode(",\n", array_map(fn($item, $key) => "'$key'=>$item", array_values($tableItens['items']), array_keys($tableItens['items']))),
                'conditional_fields' => implode(",\n", $conditional_fields)
            ];

            $include = __DIR__ . "/Templates/Resource/$item->model.php";
            $this->template = file_exists($include)
                ? include $include
                : include __DIR__ . "/Templates/Resource/Default.php";


            SaveFile::save(self::class, $module, $item->model, new Template($this->template, $update), new Template($this->templateModule, $update));
        }

        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
