<?php
namespace GrogooRestfier\Builder\Creators\Request;

use GrogooRestfier\Builder\Creators\SaveFile;
use GrogooRestfier\Helpers\Template;

class ProfilePermissionBatchRequest
{
    private $template = "<?php

    namespace  App\Generated\Modules\Company\Http\Requests;
    
    /**
     * This is an abstract class for ProfilePermission requests, which extends the NsUtilLaravel DefaultRequest.
     * This class sets up the validation rules and error messages for the ProfilePermission requests.
     */
    abstract class AbstractProfilePermissionBatchRequest extends \GrogooRestfier\Http\Request\DefaultRequest
    {
        /**
         * @var array The validation rules for the request.
         */
        protected \$rulesRules = [
            'permissions' => 'required|array', 
            'permissions.*' => 'required|integer',
            'profile_id' => 'required|integer',
            'has' => 'required|boolean'
        ];
    
        /**
         * @var array The custom error messages for the validation rules.
         */
        protected \$messagesRules = [];
    
        /**
         * @var bool Authorization status for the request.
         */
        protected \$authorizeRule = true;
    
    
        /**
         * Constructor for the AbstractProfilePermissionRequest class.
         * Initializes the custom error messages.
         */
        public function __construct()
        {
    
            \$this->messagesRules = [
                'permissions.required' => __tr('The field is required'), 
                'profile_id.required' => __tr('The field is required'),
                'has' => __tr('The field is required'),
            ];
        }
    
        /**
         * Returns the authorization status for the request.
         *
         * @return bool
         */
        public function authorize()
        {
            return \$this->authorizeRule;
        }
    
        /**
         * Returns the validation rules for the request.
         *
         * @return array
         */
        public function rules()
        {
            return parent::rulesDefault(\$this->rulesRules);
        }
    
        /**
         * Returns the custom error messages for the validation rules.
         *
         * @return array
         */
        public function messages()
        {
            return \$this->messagesRules;
        }
    } 
    ";

    private $templateModule = "<?php

    namespace App\Modules\Company\Http\Requests;

    use App\Generated\Modules\Company\Http\Requests\AbstractProfilePermissionBatchRequest;
    use GrogooRestfier\Rules\EnumValidation;

    /**
     * Class ProfilePermissionRequest
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class ProfilePermissionBatchRequest extends AbstractProfilePermissionBatchRequest
    {
        /**
         * Constructor for the ProfilePermissionRequest class.
         *
         * Initializes the validation rules and error messages specific to this request.
         */
        public function __construct() {

            parent::__construct();

            // Add additional validations rules
            \$this->rulesRules = array_merge(\$this->rulesRules, []);

            // Add any additional custom error messages
            \$this->messagesRules = array_merge(\$this->messagesRules, []);

            // Set the authorization status for the request
            \$this->authorizeRule = true;
        }
    }    
    ";

    public function handle(): void
    {
        $module = 'Company';
        $update = ['ProfilePermissionBatch'];

        SaveFile::save(
            'GrogooRestfier\Builder\Creators\Request',
            $module,
            'ProfilePermissionBatch',
            new Template($this->template, $update),
            new Template($this->templateModule, $update)
        );
        SaveFile::countGeneratedFiles('GrogooRestfier\Builder\Creators\Request', $module);
    }
}
