<?php

uses(Illuminate\Foundation\Testing\RefreshDatabase::class);

use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Company\Models\UserProfile as ModelTest;
use Illuminate\Support\Facades\Hash;


beforeEach(function () {
    list(
        $this->company,
        $this->profile,
        $this->user,
        $this->company_user,
        $modelLogin
    ) = \GrogooRestfier\Helpers\TestHelper::beforeEachDefault();

    $response = $this->postJson('/api/auth/login', $modelLogin);
    $this->token = $response->json('data.access_token');

    \App\Modules\Company\Models\Profile::factory()->create();
    \App\Modules\Company\Models\User::factory()->create();
});




test('can create a userProfile', function () {
    // $data = ['company_id' => $this->company->id];
    $data = ['profile_id' => $this->profile->id, 'user_id' => $this->user->id];

    // $data = [];
    // $modelData = ModelTest::make()->fill($data)->toArray();
    $modelData = ModelTest::factory()->make()->toArray();

    $response = $this->withHeaders(['Authorization' => "Bearer {$this->token}"])->postJson('/api/user-profile', $modelData);

    \GrogooRestfier\Helpers\TestHelper::checkJsonError($response, $this->getName(), $modelData);

    $response->assertStatus(201);
    $response->assertJsonStructure([
        'data' => ['id']
    ]);
});

test('can read a userProfile', function () {
    $data = ['profile_id' => $this->profile->id, 'user_id' => $this->user->id];
    $model = (new ModelTest())->firstOrCreate($data);

    $response = $this->withHeaders(['Authorization' => "Bearer {$this->token}"])->getJson("/api/user-profile/{$model->id}");

    \GrogooRestfier\Helpers\TestHelper::checkJsonError($response, $this->getName(), ['id' => $model->id]);

    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => ['id']
    ]);
});


test('can list userProfile', function () {
    $data = ['profile_id' => $this->profile->id, 'user_id' => $this->user->id];
    (new ModelTest())->firstOrCreate($data);


    $response = $this->withHeaders(['Authorization' => "Bearer {$this->token}"])->getJson('/api/user-profile');

    \GrogooRestfier\Helpers\TestHelper::checkJsonError($response, $this->getName(), []);

    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => ['*' => ['id']]
    ]);
});


test('can update a userProfile', function () {
    $data = ['profile_id' => $this->profile->id, 'user_id' => $this->user->id];
    $model = (new ModelTest())->firstOrCreate($data);


    if (null !== $model->is_root) {
        $model->forceFill(['is_root' => false])->save();
        $model->refresh();
    }

    if (null !== $model->company_id) {
        $model->forceFill(['company_id' => $this->company->id])->save();
        $model->refresh();
    }

    $updatedData = ModelTest::factory()->make(['company_id' => $this->company->id])->toArray();
    $updatedData['is_root'] = false;
    $updatedData['products_id'] = [1];

    $response = $this->withHeaders(['Authorization' => "Bearer {$this->token}"])->putJson('/api/user-profile/' . $model->id, $updatedData);

    \GrogooRestfier\Helpers\TestHelper::checkJsonError($response, $this->getName(), $updatedData);

    $response->assertStatus(200);
    $response->assertJsonStructure([
        'data' => ['id']
    ]);
});


test('can delete a userProfile', function () {

    $data = ['profile_id' => $this->profile->id, 'user_id' => $this->user->id];
    $model = (new ModelTest())->firstOrCreate($data);


    $response = $this->withHeaders(['Authorization' => "Bearer {$this->token}"])->deleteJson('/api/user-profile/' . $model->id);
    $response->assertStatus(204);
});
