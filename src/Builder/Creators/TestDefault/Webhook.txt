<?php

uses(Illuminate\Foundation\Testing\RefreshDatabase::class);

use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Application\Models\Webhook as ModelTest;
use Illuminate\Support\Facades\Hash;

beforeEach(function () {
    list(
        $this->company,
        $this->profile,
        $this->user,
        $this->company_user,
        $modelLogin
    ) = \GrogooRestfier\Helpers\TestHelper::beforeEachDefault();

    $response = $this->postJson('/api/auth/login', $modelLogin);
    $this->token = $response->json('data.access_token');
});



test('can create a webhook', function () {
    $data = ['company_id' => $this->company->id];
    $modelData = ModelTest::factory()->make($data)->toArray();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->postJson('/api/webhook', $modelData);

    \GrogooRestfier\Helpers\TestHelper::checkJsonError($response, $this->getName(), $modelData);

    $response->assertStatus(201);
    $response->assertJsonStructure([
        'data' => ['id', 'executed_at', 'type', 'status', 'data_proccess']
    ]);
});

test('can >>not<< read a webhook', function () {
    $model = ModelTest::factory()->create();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/webhook/' . $model->id);

    $response->assertStatus(404);
});


test('can >>not<< list webhook', function () {
    ModelTest::factory()->count(1)->create();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->getJson('/api/webhook');
    $response->assertStatus(405);
});


test('can >>not<< update a webhook', function () {
    $model = ModelTest::factory()->create();
    $updatedData = ModelTest::factory()->make(['company_id' => $this->company->id])->toArray();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->putJson('/api/webhook/' . $model->id, $updatedData);

    $response->assertStatus(404);
});


test('can >>not<< delete a webhook', function () {
    $model = ModelTest::factory()->create();
    $response = $this->withHeaders(['Authorization' => 'Bearer ' . $this->token])->deleteJson('/api/webhook/' . $model->id);
    $response->assertStatus(404);
});
