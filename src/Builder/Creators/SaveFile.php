<?php

namespace GrogooRestfier\Builder\Creators;

use Exception;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

/**
 * Class SaveFile
 *
 * Handles file saving operations for various components of a Laravel module.
 */
class SaveFile
{


    /**
     * Saves the generated content for a given module component.
     *
     * @param string $origin The class name of the component.
     * @param string $module The name of the module.
     * @param Template $generatedContent The generated content to be saved.
     * @param string $modelName The name of the model.
     * @throws Exception If the module name is empty.
     */
    public static function save(string $origin, string $module, string $modelName, Template $templateGenerated, Template $templateModel, bool $mode = Helper::IF_NOT_UPDATE): void
    {
        if (empty($module)) {
            throw new Exception("Module cannot be an empty string");
        }

        $paths = self::getPaths($origin, $module, $modelName);

        $filename = self::getDefaultNames($modelName)[$origin];
        $abstract = $origin !== Router::class && $origin !== Migration::class ? 'Abstract' : '';

        $pathGenerated = $paths['generated'] . DIRECTORY_SEPARATOR . $abstract . $filename;
        $userModule = $paths['module'] . DIRECTORY_SEPARATOR .  $filename;

        Helper::saveFile($pathGenerated, $templateGenerated->render(), Helper::OVERWRITE);

        if ($origin === Migration::class) {
            Helper::mkdir($paths['module']);
        } else {
            Helper::saveFile($userModule, $templateModel->render(), $mode);
        }
    }

    /**
     * Counts the total number of generated files for a given module component.
     *
     * @param string $origin The class name of the component.
     * @param string $module The name of the module.
     * @return int The total number of generated files.
     */
    public static function countGeneratedFiles(string $origin, string $module)
    {
        $paths = self::getPaths($origin, $module);
        $generated = is_dir($paths['generated']) ? count(scandir($paths['generated'])) : 0;
        $modules = is_dir($paths['module']) ? count(scandir($paths['module'])) : 0;
        return $generated + $modules - 4;
    }

    /**
     * Returns an array of base directories for each component.
     *
     * @return array An associative array of base directories.
     */
    private static function getBaseDirs(): array
    {
        return [
            Controller::class => 'Http/Controllers',
            Factory::class => 'Database/Factories',
            Migration::class => 'Database/Migrations',
            Model::class => 'Models',
            Provider::class => 'Providers',
            Request::class => 'Http/Requests',
            Resource::class => 'Http/Resources',
            Router::class => 'Routes',
            VerifyBelongToRule::class => 'Rules',
            RulesJsonPrepared::class => 'RulesJsonPrepared',
            Seeder::class => 'Database/Seeders',
            Tests::class => 'Tests',
            Translate::class => 'Translations',
            'Middlewares' => 'Http/Middlewares',
            CreateEvent::class => 'Events/Create',
        ];
    }

    /**
     * Returns an array of paths for generated and user module directories.
     *
     * @param string $origin The class name of the component.
     * @param string $module The name of the module.
     * @return array An associative array of paths.
     * @throws Exception If the origin is not expected.
     */
    private static function getPaths(string $origin, string $module, ?string $modelName = null): array
    {
        $appPath = Helper::getPathApp() . "/app";

        $baseDirs = self::getBaseDirs();

        if (!isset($baseDirs[$origin])) {
            throw new Exception("Origin is not expected: $origin");
        }
        $extraPath = $origin === VerifyBelongToRule::class && null !== $modelName ? "/$modelName" : '';

        return [
            'generated' => "$appPath/Generated/Modules/$module/$baseDirs[$origin]" . $extraPath,
            'module' => "$appPath/Modules/$module/$baseDirs[$origin]" . $extraPath
        ];
    }

    /**
     * Returns an array of default names for each component based on the model name.
     *
     * @param string $modelName The name of the model.
     * @return array An associative array of default names.
     */
    private static function getDefaultNames(string $modelName): array
    {
        return [
            Migration::class => $modelName . '.php',
            Controller::class => $modelName . 'Controller.php',
            Factory::class => $modelName . 'Factory.php',
            Model::class => $modelName . '.php',
            Provider::class => $modelName . 'ServiceProvider.php',
            Request::class => $modelName . 'Request.php',
            Resource::class => $modelName . 'Resource.php',
            Router::class => 'api.php',
            VerifyBelongToRule::class => $modelName . 'VerifyBelongToRule.php',
            RulesJsonPrepared::class => $modelName . 'RulesJsonPrepared.php',
            Seeder::class => $modelName . 'Seeder.php',
            Tests::class => $modelName . 'Test.php',
            Translate::class => strtolower($modelName) . '.php',
            'Middlewares' => $modelName . '.php',
            CreateEvent::class => $modelName . 'CreateEvent.php',
            'CheckIfCompanyHasValidSubscription' => 'CheckIfCompanyHasValidSubscription.php',
            'CheckSubscriptionLimit' => 'CheckSubscriptionLimit.php'
        ];
    }
}
