<?php

namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;

interface CreateInterface
{
    public function handle(Data $data): int;
}
