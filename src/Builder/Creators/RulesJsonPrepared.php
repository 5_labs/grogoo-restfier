<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class RulesJsonPrepared implements CreateInterface
{

    private $template = "<?php

    namespace App\Modules\{module}\Rules\{model};
    
    use Illuminate\Http\Request;
    
    class {model}JsonPreparedRule
    {
        public function __invoke(Request \$request): Request
        {            
            {rules}

            return \$request;
        }
    }
    ";

    public function handle(Data $data, array $ignore = []): int
    {
        $middlewares = [];
        $module = $data->getData()->module;
        $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Rules';
        // echo "\n- RulesJsonPrepared: ";

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            $rules = array_map(
                fn ($item) => "if (null !== \$request->input('$item->column_name') && is_array(\$request->input('$item->column_name'))) {
                    \$request->merge(['$item->column_name' => json_encode(\$request->input('$item->column_name'))]);
                }",
                array_filter(
                    (array)$item->fields,
                    fn ($item) => isset($item->type) && stripos(LoadDB::$config[$item->type][1], 'json') !== false
                )
            );

            if (count($rules) === 0) {
                continue;
            }

            $update = [
                'rules' => implode("\n", $rules),
                'construct' => '',
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
            ];

            (new Template($this->template, $update))
                ->renderTo($pathToSave . "/$item->model/{$item->model}JsonPreparedRule.php", Helper::IF_NOT_UPDATE);
        }
        return is_dir($pathToSave) ? count(scandir($pathToSave)) - 2 : 0;
    }
}
