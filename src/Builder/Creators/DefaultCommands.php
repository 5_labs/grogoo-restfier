<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class DefaultCommands implements CreateInterface
{
    public function handle(Data $data, array $ignore = []): int
    {
        $origin = __DIR__ . '/Templates/Commands/*';
        $destine = app()->basePath() . '/app/Console/Commands/GrogooCommands/';
        shell_exec("cp -n $origin $destine");
        return 1;
    }
}
