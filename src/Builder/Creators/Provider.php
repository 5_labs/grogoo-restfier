<?php

namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Creators\CreateInterface;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Provider implements CreateInterface
{
    private $template = "<?php

    namespace App\Generated\Modules\{module}\Providers;

    use Illuminate\Support\ServiceProvider;
    use Illuminate\Database\Eloquent\Factories\Factory as EloquentFactory;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Route;
    use Faker\Factory as FakerFactory;

    abstract class Abstract{module}ServiceProvider extends ServiceProvider
    {

        protected \$listen = [];

        public function register()
        {
            // Não é necessário registrar a fábrica aqui.
        }

        public function boot()
        {
            // API Routes. Using Middleware to response on JSON mode
            \$apiPath = base_path().'/app/Modules/{module}/Routes/api.php';
            if (file_exists(\$apiPath)){
                Route::prefix('api')
                    ->middleware([
                        \GrogooRestfier\Http\Middleware\ResponseMiddleware::class,
                        \Illuminate\Routing\Middleware\ThrottleRequests::class . ':api'
                    ])
                    ->group(fn () => \$this->loadRoutesFrom(\$apiPath));
            }

            // // WEB Routes.
            // \$webPath = base_path().'/app/Modules/{module}/Routes/web.php';
            // if (file_exists(\$webPath)){
            //     Route::prefix('')
            //         ->middleware([
            //             \Illuminate\Routing\Middleware\ThrottleRequests::class . ':api'
            //         ])
            //         ->group(fn () => \$this->loadRoutesFrom(\$webPath));
            // }

            \$this->app['router']->middleware('{module}.check.belong.to', \App\Modules\{module}\Http\Middlewares\CheckBelongTo::class);

            // OK!
            \$this->loadMigrationsFrom([
                base_path() . '/app/Modules/{module}/Database/Migrations',
            ]);

            // \$this->loadViewsFrom(base_path().'/app/Modules/{module}/Views', '{module_lower}');

            \$this->publishes([
                base_path().'/app/Modules/{module}/Config/{module_lower}.php' => config_path('{module_lower}.php'),
            ], '{module_lower}-config');

            \$this->mergeConfigFrom(
                base_path().'/app/Modules/{module}/Config/{module_lower}.php', 
                '{module_lower}'
            );         
            
            // Listners
            \$this->listeners();
        }

        /**
         * Determine if events and listeners should be automatically discovered.
         */
        public function shouldDiscoverEvents(): bool
        {
            return true;
        }

        public function listeners() {
            foreach (\$this->listen as \$event => \$listners) {
                foreach (\$listners as \$listner) {
                    \$this->app['events']->listen(
                        \$event,
                        \$listner
                    );
                }
            }
        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Providers;

    use App\Generated\Modules\{module}\Providers\Abstract{module}ServiceProvider;

    final class {module}ServiceProvider extends Abstract{module}ServiceProvider
    {
        /**
         * The events to listener mappings for the application.
         *
         * @var array<class-string, array<int, class-string>>
         */
        protected \$listen = [
            // UpdateHitsOnVersionEvent::class => [
            //     UpdateHitsOnVersionListener::class,
            // ],
        ];
    }
    ";

    public function handle(Data $data, $ignore = []): int
    {
        // echo "\n- Provider: ";
        $update = [
            'module' => $data->getData()->module,
            'module_lower' => mb_strtolower($data->getData()->module),
        ];
        $module = $data->getData()->module;

        SaveFile::save(self::class, $module, $module, new Template($this->template, $update), new Template($this->templateModule, $update));

        $this->createCheckCompaniesSubscriptionMiddleware();
        $this->createCheckSubscriptionLimitMiddleware();

        return SaveFile::countGeneratedFiles(self::class, $module);
    }

    public function createCheckCompaniesSubscriptionMiddleware()
    {
        $abstract = include __DIR__ . '/Templates/Middlewares/AbstractCheckIfCompanyHasValidSubscription.php';
        $final = include __DIR__ . '/Templates/Middlewares/FinalCheckIfCompanyHasValidSubscription.php';

        SaveFile::save('Middlewares', 'Subscription', 'CheckIfCompanyHasValidSubscription', new Template($abstract), new Template($final));
    }

    public function createCheckSubscriptionLimitMiddleware()
    {
        $abstract = include __DIR__ . '/Templates/Middlewares/AbstractCheckSubscriptionLimit.php';
        $final = include __DIR__ . '/Templates/Middlewares/FinalCheckSubscriptionLimit.php';
        SaveFile::save('Middlewares', 'Subscription', 'CheckSubscriptionLimit', new Template($abstract), new Template($final));
    }
}
