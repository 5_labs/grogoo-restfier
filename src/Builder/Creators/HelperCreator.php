<?php

namespace GrogooRestfier\Builder\Creators;

use Exception;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use GrogooRestfier\Helpers\TopologicalSort;

class HelperCreator
{

    /**
     * Prepara o namespace correto da importação
     *
     * @param Data $data
     * @param object $item
     * @param string $filetype ex '\\Http\Resources\\'
     * @return string
     */
    public static function getNameSpace(Data $data, string $table, string $filetype): string
    {
        $namespaces = (array) $data->getData()->namespaces;

        return isset($namespaces[$table]) && $namespaces[$table] !== $data->getData()->module
            ? '\\App\Modules\\' . $namespaces[$table] . $filetype
            : '\\App\Modules\\' . $data->getData()->module . $filetype;
    }


    /**
     * Recupera o caminho da relação com a tabela que possui o campo company_id.
     *
     * @param string $currentTable
     * @param array $relations
     * @param string $companyTable
     * @param array $paths
     * @param array $path
     * @return array
     */
    public static function getRelationsToCompany(
        string $currentTable,
        array $relations,
        $field_origin,
        $field_relation,
        string $companyTable = 'companies',
        $paths = [],
        $path = [],
        $includeNullable = false
    ) {
        if (count($path) === 0) {
            $path[] = "{$currentTable}|{$field_relation}|{$field_origin}";
        }

        // Procura o relacionamento da tabela atual com a tabela de destino que possui o campo company_id
        $relationToCompany = array_filter($relations[$currentTable], function ($relation) use ($companyTable) {
            return $relation->referenced_table_name == $companyTable && $relation->column_name == 'company_id';
        });

        // Se encontrou, adiciona o caminho ao array de caminhos
        if (count($relationToCompany) > 0) {
            $path[] = array_values($relationToCompany)[0]->referenced_table_name;
            $paths[] = implode('.', $path);
        }

        // Continua procurando pelas relações das tabelas relacionadas
        foreach ($relations[$currentTable] as $relation) {
            if ($relation->referenced_table_name !== $companyTable && ($relation->notnull || $includeNullable)) {

                $nextPath = array_merge($path, ["{$relation->referenced_table_name}|{$relation->referenced_column_name}|{$relation->column_name}"]);

                $paths = self::getRelationsToCompany($relation->referenced_table_name, $relations, '', '',  $companyTable, $paths, $nextPath);
            }
        }

        return $paths;
    }

    /**
     * // Obter a relação com company_id
     *
     * @param object $item
     * @return array
     */
    public static function getRoutes(Data $data, string $table, bool $includeNullable = false)
    {
        $data = $data->getData();
        $items = $data->items;
        $relations = $data->relations;
        $item = (object) array_values(array_filter((array)$items, fn ($item) => $item->table === $table))[0];
        $out = ['router' => [], 'nullable' => []];
        $tmp = [];

        if (!count(
            array_filter(
                (array) $item->fields,
                fn ($field) => ($field->field_origin ?? null) === 'company_id'
            )
        ) > 0) {
            foreach (array_filter((array) $item->fields, fn ($field) => !isset($field->type)) as $field) {
                if (!$field->nullable || $includeNullable === true) {
                    $tmp[$field->field_origin] = self::getRelationsToCompany(
                        $field->table,
                        (array) $relations,
                        $field->field_origin,
                        $field->field_relation,
                        'companies',
                        [],
                        [],
                        $includeNullable
                    );
                }
            }
            foreach ($tmp as $item) {
                $out['router'] = array_merge(
                    $out['router'],
                    $item
                );
            }
        }

        if (count($out['router']) > 0) {
            // $out['router'] = array_map(fn ($item) => implode('.', array_reverse(explode('.', $item))), $out['router']);
            // echo "\n## $table:";
            // var_export($out);
        }


        // Obter com base nas relacoes nullable, as rules caso seja enviado um id (ex.: files)
        $relationsFull = (array)$relations;
        $relations = array_filter(
            $relationsFull[$table],
            fn ($i) => !$i->notnull
        );
        $tmp = [];
        foreach ($relations as $relation) {
            $tmp[$relation->column_name] = HelperCreator::getRelationsToCompany(
                $relation->referenced_table_name,
                $relationsFull,
                $relation->column_name,
                $relation->referenced_column_name
            );
        }
        foreach ($tmp as $item) {
            $out['nullable'] = array_merge(
                $out['nullable'],
                $item
            );
        }

        return $out;
    }

    public static function getItemsSortedByTopology(Data $data): array
    {
        // Extrair tabelas e suas dependências do objeto $data
        $tables = self::extractTablesAndDependencies((array)$data->getData()->items);

        // Ordenar as tabelas com base em suas dependências usando a ordenação topológica
        $sortedTables = TopologicalSort::sort($tables);

        // Ordenar a lista de itens com base na ordem das tabelas ordenadas
        return self::sortItemsBySortedTables((array)$data->getData()->items, $sortedTables);
    }

    // Funções auxiliares
    private static function extractTablesAndDependencies(array $items): array
    {
        $tables = [];
        foreach ($items as $item) {
            $tables[$item->table] = array_map(
                fn ($field) => $field->table,
                array_filter(
                    (array)$item->fields,
                    fn ($field) => !isset($field->type)
                )
            );
        }
        return $tables;
    }

    private static function sortItemsBySortedTables(array $items, array $sortedTables): array
    {
        usort(
            $items,
            function ($a, $b) use ($sortedTables) {
                $indexA = array_search($a->table, $sortedTables);
                $indexB = array_search($b->table, $sortedTables);
                if ($indexA == $indexB) {
                    return 0;
                }
                return ($indexA < $indexB) ? -1 : 1;
            }
        );
        return $items;
    }

    public static function checkIfFieldIsNullable($data, $table, $cpoRel): object
    {
        $itemRelacionado = array_values(
            array_filter(
                (array) $data->getData()->items,
                fn ($itemFilter) => $itemFilter->table === $table
            )
        );

        if (isset($itemRelacionado[0])) {
            $field = array_values(
                array_filter(
                    (array) $itemRelacionado[0]->fields,
                    fn ($item) => isset($item->field_origin) && $item->field_origin === $cpoRel
                )
            );
        }
        return (object)[
            'exists' => isset($field[0]),
            'nullable' => $field[0]->nullable ?? true
        ];
    }

    public static function arrayToString(array $array)
    {

        $string = var_export($array, true);
        $string = preg_replace('/array \(/', '[', $string);
        $string = preg_replace('/\)$/', ']', $string);

        return $string;
    }
}
