<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Translate implements CreateInterface
{

    public function handle(Data $data, array $ignore = []): int
    {

        $lang = Helper::getPathApp() . '/lang/en.json';
        if (!file_exists($lang)) {
            Helper::saveFile($lang, json_encode([]));
        }

        $dataConfig = array_merge(
            json_decode(json_encode($data->getData()->aliasesFields), true) ?? [],
            json_decode(file_get_contents($lang), true)
        );
        ksort($dataConfig);

        Helper::saveFile($lang, json_encode($dataConfig, JSON_UNESCAPED_SLASHES), Helper::OVERWRITE);

        return 1;
    }
}
