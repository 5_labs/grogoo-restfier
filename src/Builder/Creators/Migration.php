<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use GrogooRestfier\Helpers\TopologicalSort;

class Migration implements CreateInterface
{

    private $template = "<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    
    return new class extends Migration
    {
        public function up()
        {
            Schema::create('{table}', function (Blueprint \$table) {
               {fields}
            });
        }
    
        public function down()
        {
            Schema::dropIfExists('{table}');
        }
    };";

    private $templateModule = "<?php
    \$migrationsGenerated = base_path() . '/app/Generated/Modules/{module}/Database/Migrations/{filename}.php';
    if (file_exists(\$migrationsGenerated)) {
        include \$migrationsGenerated;
    }
    ";


    public function handle(Data $data, $ignore = []): int
    {

        $module = $data->getData()->module;

        $items = HelperCreator::getItemsSortedByTopology($data);

        foreach ($items as $key => $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            $tableItens = [];
            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    $model = HelperCreator::getNameSpace($data, $field->table, '\\Models\\') . $field->model . '::class';
                    // $tableItens[] = "\$table->foreignIdFor($model, '$field->field_origin')->constrained()->onUpdate('cascade')->onDelete('cascade');";

                    $tableItens[] = "
                    \$table->foreignId('$field->field_origin')
                    ->references('$field->field_relation')
                    ->on('{$field->schema}.{$field->table}')
                    ->onUpdate('$field->onUpdate')
                    ->onDelete('$field->onDelete');
                    ";
                    continue;
                }
                list($type, $entryType, $returnType) = LoadDB::$config[$field->type] ?? ['none', 'no-tem', 'none'];
                switch ($field->type) {
                    case 'string':
                        $field->maxsize = $field->maxsize > 255 ? 255 : $field->maxsize;
                        $tableItem = "\$table->$entryType('$field->column_name', '$field->maxsize')";
                        break;
                    case 'tsvector':
                        $tableItem = "\$table->addColumn('tsvector', '$field->column_name')";
                    default:
                        $tableItem = "\$table->$entryType('$field->column_name')";
                        break;
                }

                // primary key
                if ($field->isPrimaryKey) {
                    $tableItem = "\$table->id('$field->column_name')";
                }

                if ($field->nullable) {
                    $tableItem .= '->nullable()';
                }

                $tableItem .= "->comments('$field->coments')";


                // encerrando 
                $tableItem .= ';';

                $tableItens[] = $tableItem;
            }

            // Uniques constraint
            $constraints = [];
            foreach ($item->uniquesConstraint as $migrate) {
                $constraints[$migrate->constraint_name][] = "'$migrate->column_name'";
            }
            foreach ($constraints as $migrate) {
                $tableItens[]  = "\$table->unique([" . implode(',', $migrate) . "]);";
            }

            $update = [
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'fields' => implode("\n", $tableItens),
            ];

            $serial = str_pad((string)$key, 6, '0', STR_PAD_LEFT);
            $update['filename'] = '1900_01_01_' . $serial . '_create_' . $item->table . '_table';

            Helper::saveFile(
                Helper::getPathApp() . "/database/grogoo_migrations/" . $update['filename'],
                (new Template($this->template, $update))->render(),
                'SOBREPOR'
            );

            // SaveFile::save(
            //     self::class,
            //     $module,
            //     $update['filename'],
            //     new Template($this->template, $update),
            //     new Template($this->templateModule, $update)
            // );
        }

        // return count(scandir($pathToSave)) - 2;
        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
