<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class Model implements CreateInterface
{
    private $template = "<?php

    namespace App\Generated\Modules\{module}\Models;

    {softDeletesA}

    use App\Modules\{module}\Database\Factories\{model}Factory;
    use Illuminate\Support\Facades\Auth;

    {extraUses}

    abstract class Abstract{model} extends Model
    {
        {softDeletesB}

        protected \$table = '{table}';
        protected \$fillable = [{fillable}];
        protected \$casts = [{casts}];
        {ignoreTimestamps}

        protected static function boot()
        {
            parent::boot();

            if (class_exists(\App\Modules\{module}\Events\Create\{model}CreateEvent::class)) {
                static::created(function (\$model) {
                    event(new \App\Modules\{module}\Events\Create\{model}CreateEvent({createEventParam}));
                });
            }
        }


        {relations}

        {prunable_function}

        protected static function newFactory(): {model}Factory
        {
            return new {model}Factory();
        }

    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Models;

    use App\Generated\Modules\{module}\Models\Abstract{model};

    final class {model} extends Abstract{model}
    {
        public function __construct(array \$attributes = [])
        {
            parent::__construct(\$attributes);
        }
    }
    ";

    private function getBelongTo($fnName, $model)
    {
        return "public function $fnName()
        {
            return \$this->belongsTo($model::class);
        }
        ";
    }

    private function getHasMany($fnName, $model)
    {
        return "public function $fnName()
        {
            return \$this->hasMany($model::class);
        }
        ";
    }

    private function updateGrogooConfig($tablename, $modelname, $modulename)
    {
        $config = include(Helper::getPathApp() . '/config/grogoo.php');
        $config['models'][$modelname] = [$tablename, $modulename];
        Helper::saveFile(
            Helper::getPathApp() . '/config/grogoo.php',
            "<?php\n\n // This content is generated automatic. Do not edit.\n\n return " . var_export($config, true) . ";",
            Helper::OVERWRITE
        );
    }

    public function handle(Data $data, array $ignore = []): int
    {
        $module = $data->getData()->module;
        $ignoreFillable = $data->getData()->ignoreFieldsToFillable ?? null;

        // Na model, vou criar o arquivo "config" da model
        Helper::saveFile(
            Helper::getPathApp() . '/app/Modules/' . $module . '/Config/' . mb_strtolower($module) . '.php',
            "<?php\n\n return [];"
        );

        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Models';

        $update = [
            'prunable_function' => '',
            'softDeletesA' => '
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
',
            'softDeletesB' => 'use HasFactory;'
        ];
        // echo "- Models: ";

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            // mapear os dados para a criação da config
            $this->updateGrogooConfig($item->table, $item->model,  $module);


            $update = [
                'prunable_function' => '',
                'softDeletesA' => '
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    ',
                'softDeletesB' => 'use HasFactory;',
                'extraUses' => ''
            ];

            $tableItens = ['relations' => [], 'fillable' => [], 'casts' => []];


            foreach ($item->hasMany as $hasMany) {
                $tableItens['relations'][$hasMany->table] = $this->getHasMany(
                    Helper::name2CamelCase($hasMany->table),
                    HelperCreator::getNameSpace($data, $hasMany->table, '\\Models\\') . $hasMany->model
                );
            }


            $primaryKey = 'id';
            foreach ($item->fields as $field) {
                if (!isset($field->type)) {
                    $tableItens['relations'][] = $this->getBelongTo(
                        lcfirst($field->model),
                        HelperCreator::getNameSpace($data, $field->table, '\\Models\\') . $field->model
                    );
                    $tableItens['fillable'][] = $field->field_origin;
                    continue;
                }
                if (
                    $field->isPrimaryKey
                    || stripos($field->column_name, 'createtime') !== false
                    || stripos($field->column_name, 'created_at') !== false
                    || stripos($field->column_name, 'updated_at') !== false
                    || stripos($field->type, 'tsvector') !== false
                ) {
                    continue;
                }
                $primaryKey =  $field->isPrimaryKey ? $field->column_name : $primaryKey;

                // softdelete
                if (stripos($field->column_name, 'deleted_at') !== false) {
                    $update['prunable_function'] = "
                    public function prunable()
                    {
                        return static::where('deleted_at', '<=', now()->subMonth());
                    }
                    ";
                    $update['softDeletesA'] = '
                        use Illuminate\Database\Eloquent\Factories\HasFactory;
                        use Illuminate\Database\Eloquent\Model;
                        use Illuminate\Database\Eloquent\SoftDeletes;
                        use Illuminate\Database\Eloquent\Prunable;
                        ';
                    $update['softDeletesB'] = 'use HasFactory, SoftDeletes, Prunable;';
                    continue;
                }

                // check restriction to fillable
                if (
                    null === $ignoreFillable
                    // || null === $ignoreFillable->{$item->table}
                    || array_search($field->column_name, ($ignoreFillable->{$item->table} ?? [])) === false
                ) {
                    $tableItens['fillable'][] = $field->column_name;
                }

                // casts
                if (LoadDB::$config[$field->type][0] === 'json') {
                    $tableItens['casts'][] = "'" . $field->column_name . "' => 'json'";
                }
            }

            // caso a model não tenha os timestamps, devo ignorar a formaçaõ automatica pelo eloquent
            $ignoreTimestamps =
                count(array_filter(
                    (array)$item->fields,
                    fn ($field) => isset($field->column_name) && (stripos($field->column_name, 'created_at') !== false || stripos($field->column_name, 'updated_at') !== false)
                )) === 2
                ? ''
                : 'public $timestamps = false;';

            // Regras especiifcas
            switch ($item->model) {
                case 'Profile':
                    $update['prunable_function'] .= "public function getTotalUsersAttribute() { return \$this->userProfile->count();} \n

                    public function getPermissionsAllowed() {
                        \$result = \App\Modules\Company\Models\ProfilePermission::where('profile_id', \$this->id)
                        ->join('permissions', 'permissions.id', '=', 'profile_permissions.permission_id')
                        ->select('permissions.*')
                        ->get();

                        \$permission = [];

                        foreach (\$result as \$item) {
                            \$permission[] = \$item->group . '.' . \$item->route . '.' . \$item->verb;
                        }

                        return \$permission;
                    }";

                    $update['extraUses'] .= 'use Illuminate\Support\Facades\DB;';
                    break;
            }

            $update = array_merge($update, [
                'casts' => implode(",", $tableItens['casts']),
                'primaryKey' => $primaryKey,
                'module' => $module,
                'table' => $item->table,
                'model' => $item->model,
                'fillable' => implode(",", array_map(fn ($item) => "'$item'", $tableItens['fillable'])),
                'relations' => implode("\n", array_map(fn ($item) => $item, $tableItens['relations'])),
                'ignoreTimestamps' => $ignoreTimestamps,
                'createEventParam' => $this->auditEnabled() ? "\$model, Auth::user()" : "\$model"
            ]);

            $me = explode('\\', __CLASS__);
            $include = __DIR__
                . '/Templates/'
                . end($me)
                . '/'
                . $item->model
                . '.php';

            $template = file_exists($include)
                ? include $include
                : $this->template;

            SaveFile::save(self::class, $module,  $item->model, new Template(
                $template,
                $update
            ), new Template($this->templateModule, $update));
        }

        return SaveFile::countGeneratedFiles(self::class, $module);
    }

    public function auditEnabled(): bool {
        $fileconfig = Helper::getPathApp() . '/.build.config.json';
        if (!file_exists($fileconfig)) {
            throw new FileNotFoundException("ERROR: File $fileconfig not found");
        }
        $config = json_decode(file_get_contents($fileconfig), true);

        if(!isset($config['audit'])) {
            return false;
        }

        return $config['audit'];
    }
}
