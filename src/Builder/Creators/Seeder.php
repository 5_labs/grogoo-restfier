<?php

namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Seeder
{

    private $template = "<?php

    namespace App\Generated\Modules\{module}\Database\Seeders;
    
    use Illuminate\Database\Console\Seeds\WithoutModelEvents;
    use Illuminate\Database\Seeder;
    
    abstract class Abstract{module}Seeder extends Seeder
    {
        protected \$qtdeToSeed = 10;

        /**
         * Run the database seeds.
         */
        public function run(): void
        {
            \$models = [
                {seeders}
            ];
        
            foreach (\$models as\$model) {
                for (\$i = 0; \$i < \$this->qtdeToSeed; \$i++) {
                    \$model::factory()->create();
                }
            }
        }
    }
       
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Database\Seeders;

    use App\Generated\Modules\{module}\Database\Seeders\Abstract{module}Seeder;
        
    final class {module}Seeder extends Abstract{module}Seeder
    {
        public function __construct()
        {
            \$this->qtdeToSeed = 10;
        }
    
    }
       
    ";


    public function handle(Data $data, $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Database/Seeders';
        $module = $data->getData()->module;
        // echo "\n- Seeders: ";
        $items = HelperCreator::getItemsSortedByTopology($data);
        $seeders = [];
        $models = [];
        $ignore = array_merge($ignore, $data->getData()->seedIgnore);


        // foreach ($data->getData()->items as $item) {
        foreach ($items as $key => $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            $seeders[] = HelperCreator::getNameSpace($data, $item->table, '\\Models\\') . $item->model . '::class';
        }


        $update = [
            'seeders' => implode(",\n", $seeders),
            'module' => $data->getData()->module,
            'table' => $item->table,
            'model' => $item->model,
            'modelModel' => HelperCreator::getNameSpace($data, $item->table, '\\Models\\')
                . $item->model
        ];

        // (new Template($this->template, $update))
        //     ->renderTo($pathToSave . '/' . $update['module'] . 'Seeder.php', Helper::IF_NOT_UPDATE);

        SaveFile::save(self::class, $module,  $module, new Template($this->template, $update), new Template($this->templateModule, $update));

        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
