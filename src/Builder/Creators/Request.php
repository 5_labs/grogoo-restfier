<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Creators\Request\ProfilePermissionBatchRequest;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\ConfigPersisted;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Request implements CreateInterface
{

    private $template = "<?php

    namespace  App\Generated\Modules\{module}\Http\Requests;
    
    /**
     * This is an abstract class for {model} requests, which extends the NsUtilLaravel DefaultRequest.
     * This class sets up the validation rules and error messages for the {model} requests.
     */
    abstract class Abstract{model}Request extends \GrogooRestfier\Http\Request\DefaultRequest
    {
        /**
     * @var array The validation rules for the request.
     */
        protected \$rulesRules = [{rules}]; 

        /**
         * @var array The custom error messages for the validation rules.
         */
        protected \$messagesRules = [];

        /**
         * @var bool Authorization status for the request.
         */
        protected \$authorizeRule = true;


        /**
         * Constructor for the Abstract{model}Request class.
         * Initializes the custom error messages.
         */
        public function __construct() {

            \$this->messagesRules = [{messages}];

        }

        /**
         * Returns the authorization status for the request.
         *
         * @return bool
         */
        public function authorize()
        {
            return \$this->authorizeRule;
        }
    
        /**
         * Returns the validation rules for the request.
         *
         * @return array
         */
        public function rules()
        {
            return parent::rulesDefault(\$this->rulesRules);
        }
    
        /**
         * Returns the custom error messages for the validation rules.
         *
         * @return array
         */
        public function messages()
        {
            return \$this->messagesRules;
        }

        protected function prepareForValidation()
        {
            parent::_prepareForValidation(\$this->rulesRules);
        }

        {defaults}
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Http\Requests;

    use App\Generated\Modules\{module}\Http\Requests\Abstract{model}Request;
    use GrogooRestfier\Rules\EnumValidation;

    /**
     * Class {model}Request
     *
     * This class extends the generated class. Update this to resolve the requirements of this application module.
     */
    final class {model}Request extends Abstract{model}Request
    {
        /**
         * Constructor for the {model}Request class.
         *
         * Initializes the validation rules and error messages specific to this request.
         */
        public function __construct() {

            parent::__construct();

            // Add additional validations rules
            \$this->rulesRules = array_merge(\$this->rulesRules, []);

            // Add any additional custom error messages
            \$this->messagesRules = array_merge(\$this->messagesRules, []);

            // Set the authorization status for the request
            \$this->authorizeRule = true;
        }
    }    
    ";

    public function handle(Data $data, array $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Http/Requests';
        $module = $data->getData()->module;
        $ignoreFillable = $data->getData()->ignoreFieldsToFillable ?? null;
        // echo "- Request: ";
        (new ProfilePermissionBatchRequest())->handle();
        // Chamar aqui

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = ['messages' => [], 'rules' => [], 'rules_relation' => []];
            $defaults = [];

            foreach ($item->fields as $field) {
                $model_camelcase = lcfirst($field->model);

                // Relacionamentos
                if (!isset($field->type)) {

                    // rules
                    $required = $field->nullable ? 'nullable' : 'required';
                    $field->model_camelcase = lcfirst($field->model);

                    $exists = 'exists:' . $field->table . ',' . $field->field_relation . '';
                    $tableItens['rules_relation'][$field->field_origin] = "$required|integer|$exists";
                    $tableItens['messages_relation']["$field->field_origin.exists"] = "__tr('The value is invalid')";
                    $tableItens['messages_relation']["$field->field_origin.required"] = "__tr('The field is required')";

                    continue;
                }

                if (
                    $field->isPrimaryKey
                    || stripos($field->column_name, 'createtime') !== false
                    || stripos($field->column_name, 'created_at') !== false
                    || stripos($field->column_name, 'updated_at') !== false
                    || stripos($field->column_name, 'deleted_at') !== false
                    || stripos($field->column_name, 'company_id') !== false
                    || stripos($field->type, 'tsvector') !== false
                    || array_search($field->column_name, ($ignoreFillable->{$item->table} ?? [])) !== false
                ) {
                    continue;
                }

                // rules
                $rules_item = [];
                $rules_item[] = $field->nullable || $field->default !== null ? 'nullable' : 'required';

                // Type
                $type = LoadDB::$config[$field->type][1];
                switch (true) {
                    case $type === 'float':
                    case $type === 'decimal':
                    case $type === 'float':
                        $rules_item[] = 'regex:/^\d+(\.\d{1,2})?$/';
                        break;
                    case stripos($field->column_name, 'cpfcnpj') !== false:
                        $rules_item[] = 'cpfcnpj';
                        $tableItens['messages']["$field->column_name"] = "__tr('Invalid CPF or CNPJ')";
                        break;
                    case $type === 'text':
                        $rules_item[] = 'string';
                        break;
                    case $type === 'ipAddress':
                        $rules_item[] = 'ip';
                        break;
                    case $type === 'jsonb':
                    case $type === 'json':
                        $rules_item[] = 'array';
                        break;
                    case $type === 'timestamp':
                    case $type === 'date':
                    case $type === 'time':
                        $rules_item[] = 'date';
                        break;
                    default:
                        $rules_item[] = LoadDB::$config[$field->type][1];
                }

                // defaults
                if ($field->default !== null) {
                    $defaults[] = "\$validated['$field->column_name'] ??= $field->default;";
                }

                // regra especial para email
                if (stripos($field->column_name, 'email') !== false) {
                    $rules_item[] = 'email';
                    $tableItens['messages']["$field->column_name.email"] = "__tr('Please enter a valid email address')";
                } else {
                    switch (LoadDB::$config[$field->type][1]) {
                        case 'float':
                        case 'decimal':
                        case 'float':
                            $rules_item[] = 'max:' . $field->maxsize;
                            $tableItens['messages']["$field->column_name.max"] = "__tr('Max characters exceeded') . ' (:max)'";
                            break;
                        case 'integer':
                        case 'jsonb':
                        case 'json':
                        case 'boolean':
                        case 'text':
                        case 'array':
                            break;
                        case 'date':
                            $rules_item[] = 'date_format:Y-m-d';
                            $tableItens['messages']["$field->column_name.date_format"] = "__tr('Date format is invalid. Expected: ') . ':date_format'";
                            break;
                        case 'datetime':
                        case 'timestamp':
                            $rules_item[] = 'date_format:Y-m-d H:i:s';
                            $tableItens['messages']["$field->column_name.date_format"] = "__tr('Date format is invalid. Expected: ') . ':date_format'";
                            break;
                        default:
                            $rules_item[] = 'max:' . $field->maxsize;
                            $tableItens['messages']["$field->column_name.max"] = "__tr('Max characters exceeded') . ' (:max)'";
                            break;
                    }
                }
                $tableItens['rules'][$field->column_name] = implode('|', $rules_item);

                // messages
                $tableItens['messages']["$field->column_name.required"] = "__tr('The field is required')";
                $tableItens['messages']["$field->column_name." . LoadDB::$config[$field->type][1]] = "__tr('Must be a " . LoadDB::$config[$field->type][1] . "')";
            }

            $tableItens['rules'] = array_merge($tableItens['rules'], ($tableItens['rules_relation'] ?? []));
            $tableItens['messages'] = array_merge($tableItens['messages'], ($tableItens['messages_relation'] ?? []));

            // var_export($tableItens['rules']);
            // die();
            $update = [
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'rules' => implode(",\n", array_map(fn($key, $item) => "'$key'=>'$item'", array_keys($tableItens['rules']), array_values($tableItens['rules']))),
                'messages' => implode(",\n", array_map(fn($item, $key) => "'$key'=>$item", array_values($tableItens['messages']), array_keys($tableItens['messages']))),
                'defaults' => implode("\n", $defaults)
            ];

            if ($item->table === 'files') {
                $update['rules'] = "'file' => 'required|file',";
                $update['messages'] = '';
                $update['defaults'] = '';
                $defaults = [];
            }

            // Salvar as configurações para utilizar na construção da documentação
            (new ConfigPersisted('postman'))->add(
                Helper::name2KebabCase(Helper::singularize($item->table)),
                $tableItens['rules']
            );

            $template = $this->template;
            if (count($defaults) > 0) {
                $template = str_replace('{defaults}', " public function validated(\$key = null, \$default = null)
                {
                    \$validated = parent::validated(\$key, \$default);
        
                    {defaults}
        
                    return \$validated;
                }", $this->template);
            }

            SaveFile::save(self::class, $module, $item->model, new Template($template, $update), new Template($this->templateModule, $update));
        }

        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
