<?php

$default = include __DIR__ . '/Default.php';

$apikeyController = str_replace(
    [
        '$item = {model}::create($request->all());'
    ],
    [
        '
        $item = new Apikey($request->all());
        $item->forceFill([
            \'hash\' => $request->input(\'hash\')
        ]);
        $item->save();
        $item->refresh();        
        '
    ],
    $default
);


return $apikeyController;
