<?php

$default = include __DIR__ . '/Default.php';
$extras = <<<EOF

public static function add_Sendmail(
    string \$name,
    string \$email,
    string \$subject,
    string \$templateId,
    array \$templateData
) {
    // notify to user
    \$queue = Queue::create([
        'type' => 'sendmail',
        'data' => [
            'name' => \$name,
            'email' => \$email,
            'subject' => __tr(\$subject),
            'config' => [
                'template_id' => \$templateId,
                'template_data' => \$templateData
            ]
        ]
    ]);

    return \$queue->id > 0;
}

public function execute(\$limit = 5): bool
    {
        \$list = \App\Modules\Application\Models\Queue::where('status', '=', 'PENDING')
            ->orderBy('id')
            ->lockForUpdate()
            ->limit(\$limit)
            ->get();

        foreach (\$list as \$queue) {
            \$queue->status = 'RUNNING';
            \$queue->save();
            \$fn = \$queue->type;
            if (method_exists(\$this, \$fn)) {
                \$this->\$fn(\$queue);
            }
        }

        return count(\$list) > 0;
    }

    protected function sendmail(Queue \$queue): void
    {
        \$mailer = app()->make(\GrogooRestfier\Contracts\MailServiceInterface::class);
        \$data = \$queue->data;
        \$ret = \$mailer->send(
            \$data['email'],
            \$data['name'],
            \$data['subject'],
            '',
            \$data['config']
        );

        \$queue->status = \$ret->statusCode() < 300 ? 'SUCCESS' : 'ERROR';
        
        if (\$queue->status === 'SUCCESS' && \$queue->remove_on_success) {
            \$queue->delete();
        } else {
            \$queue->executed_at = \Carbon\Carbon::now();
            \$queue->data = array_merge(\$queue->data, ['error' => \$ret->headers()]);    
            \$queue->save();
        }
    }
EOF;

$out = str_replace(
    ['//{extras}', '//{extrasUses}'],
    [$extras, ''],
    $default
);

return $out;
