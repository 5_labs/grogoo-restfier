<?php
$default = include __DIR__ . '/Default.php';
$extras = <<<EOF

    public function index(UserProfileRequest \$request)
    {
        \$list = \$this->getQuery(\$request);

        // order
        \$this->controller->setOrder(\$list);

        // search
        \$searchTerm = \$request->input('search');
        if (!is_null(\$searchTerm)) {
            \$list->where(function (\$query) use (\$searchTerm) {
                \$query->whereRaw("unaccent(users.name) ~* unaccent(?)", [\$searchTerm])
                        ->orWhereRaw("unaccent(users.email) ~* unaccent(?)", [\$searchTerm]);
            });
        }

        // limit per page. Default 30. Max 100
        \$limit = \$request->input('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

        return UserProfileResource::collection(
            \$list->paginate(\$limit > 100 ? 100 : \$limit)
        );
    }

EOF;

$uses = <<<EOF

EOF;

$out = str_replace(
    ['public function index({model}Request', '//{extras}', '//{extrasUses}'],
    ['public function default__index({model}Request', $extras, $uses],
    $default
);

return $out;
