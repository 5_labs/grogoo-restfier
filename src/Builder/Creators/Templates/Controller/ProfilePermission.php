<?php

$string = include __DIR__ . '/Default.php';

$extrasUses = "
use Illuminate\Http\Response;
use App\Modules\Company\Http\Requests\ProfilePermissionBatchRequest;
";

$extras = <<<EOF
/**
 * Store a profile_permission in batch.
 *
 * @param ProfilePermissionBatchRequest \$request
 * @return ProfilePermissionResource
 */
public function batchStore(ProfilePermissionBatchRequest \$request)
{
    \$permissions = \$request->input('permissions');
    \$profileId = \$request->input('profile_id');
    \$hasPermission = \$request->input('has');
    \$results = [];

    foreach (\$permissions as \$id) {
        \$result = [
            'profile_permission_id' => null,
            'has' => \$hasPermission,
            'permission_id' => \$id,
            'profile_id' => \$profileId,
            'error' => null,
        ];

        try {
            if (\$hasPermission) {
                \$profilePermission = ProfilePermission::create([
                    'profile_id' => \$profileId,
                    'permission_id' => \$id,
                ]);
                \$result['profile_permission_id'] = \$profilePermission->id;
            } else {
                \$deleted = ProfilePermission::where('profile_id', \$profileId)
                    ->where('permission_id', \$id)
                    ->delete();

                if (!\$deleted) {
                    throw new \Exception('ProfilePermission not found.');
                }
            }
        } catch (\Exception \$e) {
            \$result['error'] = \$e->getMessage();
        }

        \$results[] = \$result;
    }

    return response()->json(\$results, Response::HTTP_CREATED);
}
EOF;

$out = str_replace(
    ['//{extras}', '//{extrasUses}'],
    [$extras, $extrasUses],
    $default
);

return $out;
