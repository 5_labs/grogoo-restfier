<?php

$extraUses = 'use App\Modules\Company\Models\UserProfile;';

$string = include __DIR__ . '/Default.php';

$string = str_replace(
    ["public function destroy({model}Request \$request)
    {
        \$item = \$this
            ->read(\$request, (int) \$request->route('{routeID}'))
            ->delete();

        return response()->json([], 204);
    }", '//{extrasUses}'],
    ["public function destroy(ProfileRequest \$request)
    {
        \$userProfilesCount = UserProfile::where('profile_id', '=', (int) \$request->route('profile'))->count();

        if (\$userProfilesCount > 0) {
            return response()->json(['message' => 'It is not possible to delete a profile that has users'], 409);
        }

        \$item = \$this
            ->read(\$request, (int) \$request->route('profile'))
            ->delete();

        return response()->json([], 204);
    }", $extraUses],
    $string
);

$string  = str_replace(
    'protected array $conditions = [];',
    'protected array $conditions = [\'is_root\' => false];',
    $string
);

return $string;
