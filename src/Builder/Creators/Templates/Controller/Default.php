<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Controllers;

use App\Modules\{module}\Http\Requests\{model}Request;
use App\Modules\{module}\Http\Resources\{model}Resource;
use App\Modules\{module}\Models\{model};
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Validator;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\ControllerHelper;
//{extrasUses}

/**
 * {model}Controller
 * 
 * This class is responsible for handling requests related to the {model} model.
 * 
 * @package App\Modules\{module}\Http\Controllers
 */ 
abstract class Abstract{model}Controller extends \App\Http\Controllers\Controller
{

    protected string \$table = '{table}';
    /**
     * Conditions to filter by.
     *
     * @var array
     */
    protected array \$conditions = [];

    /**
     * Sortable fields.
     *
     * @var array
     */
    protected array \$sortable = [
        {sortable}
    ];

    /**
     * Searchable fields.
     *
     * @var array
     */
    protected array \$searchable = [{searchable}];

    /**
     * Filterable fields.
     *
     * @var array
     */
    protected array \$filterable = {filterable};

    /**
     * Searchable query.
     *
     * @var Closure
     */
    protected ?Closure \$search = null;

    /**
     * Controller Helper by Grogoo
     * 
     * @var ControllerHelper
     */
    protected ?ControllerHelper \$controller = null;

    /**
     * Closure run to clone model
     * 
     * @var cloneRules
     */
    protected ?Closure \$cloneRules = null;


    /**
     * Error code to identify this controller.
     * 
     * @var string \$errorCode
     */
    protected string \$errorCode = '{errorCode}';

    /**
     * Determines whether the controller is available for use by clients (true) or only by the application (false).
     * 
     * @var bool \$hasPermissionToResource
     */
    protected bool \$hasPermissionToResource = true;
    

    public function __construct()
    {
        {construct}    
    }


    /**
     * Checks if the controller has permission to access the resource.
     *
     * @throws \Illuminate\Validation\UnauthorizedException Thrown if permission is denied.
     * 
     * @return void
     */
    protected function checkPermissionToResource(): void
    {
        if (\$this->hasPermissionToResource === false) {
            throw new \\Illuminate\\Validation\\UnauthorizedException(\"Sorry, you don't have permission to access this resource. (ERR_{\$this->errorCode})\", 403);\n
        }
    }

    /**
     * Sets the permission status for the controller.
     *
     * @param bool \$var The permission status to set.
     * 
     * @return self
     */
    protected function setHasPermissionToResource(bool \$var) : self 
    {
        \$this->hasPermissionToResource = \$var;

        return \$this;
    }

    /**
     * Initialize conditions for filtering.
     *
     * @param {model}Request \$request
     * @return void
     */
    protected function initCondition({model}Request \$request)
    {
        \$this->checkPermissionToResource();

        if (\$request->method() === 'GET') {
            {getConditions}
        }

        {conditions}
        
        if (null === \$this->controller) {
            \$this->controller = new ControllerHelper(\$request, \$this->table, \$this->filterable, \$this->conditions, \$this->sortable);
        }

        // searchable
        \$this->search = \$this->controller->setSearchabled(\$this->searchable);

        // filtrable
        \$this->controller->setFiltrabled();

        // clear conditions
        \$this->controller->clearConditionsIfNull();

    }

    /**
     * Create query to find a item or list
     *
     * @param {model}Request \$request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery({model}Request \$request) {
        
        \$this->initCondition(\$request);
        
        \$query = {model}::select('{table}.*')
            {joins}
            ;

        return \$this->controller->where(\$query)->with({loaders});

    }

    /**
     * Read a specific document type.
     *
     * @param {model}Request \$request
     * @param int \$id
     * @return {model}
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    protected function read({model}Request \$request, int \$id) : {model}
    {
                    
        \$query = \$this->getQuery(\$request);
        
        return  \$query
            ->where('{table}.id', (int) \$id)
            ->firstOrFail();

    }

    /**
     * Get a list of document types.
     *
     * @param {model}Request \$request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index({model}Request \$request)
    {
        \$list = \$this->getQuery(\$request);

        // order
        \$this->controller->setOrder(\$list);

        // search
        if (null !== \$this->search) {
            \$list->where(\$this->search);
        }

        // limit per page. Default 30. Max 100
        \$limit = \$request->input('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

        return {model}Resource::collection(
            \$list->paginate(\$limit > 100 ? 100 : \$limit)
        );
    }

    /**
     * Store a new document type.
     *
     * @param {model}Request \$request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store({model}Request \$request)
    {
        \$this->initCondition(\$request);

        \$item = {model}::create(\$request->all());
        
        \$item->refresh();

        \$item->load({loaders});

        return (new {model}Resource(\$item))
        ->response()
        ->setStatusCode(201);
    }

    /**
     * Display the specified {model}.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request  \$request
     * @param int  \$id
     * @return {model}Resource
     */
    public function show({model}Request \$request)
    {
        \$item = \$this->read(\$request, (int) \$request->route('{routeID}'));

        \$item->load({loaders});

        return new {model}Resource(\$item);
    }
    
    /**
     * Update the specified {model} in storage.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request  \$request
     * @param int  \$id
     * @return {model}Resource
     */
    public function update({model}Request \$request)
    {
        \$item = \$this->read(\$request, (int) \$request->route('{routeID}'));

        \$item->update(\$request->all());
        
        \$item->load({loaders});

        return new {model}Resource(\$item);
    }

    /**
     * Remove the specified {model} from storage.
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request \$request
     * @param int \$id
     * @return \Illuminate\Http\Response
     */
    public function destroy({model}Request \$request)
    {
        \$item = \$this
            ->read(\$request, (int) \$request->route('{routeID}'))
            ->delete();

        return response()->json([], 204);
    }

    /**
     * Clone the specified {model}
     *
     * @param \App\Modules\{module}\Http\Requests\{model}Request \$request
     * @param int \$id
     * @return \Illuminate\Http\Response
     */
    public function clone({model}Request \$request, int \$int)
    {
        \$original = \$this->read(\$request, \$int);

        try {
            \$original->id = null;
            \$data = \$original->toArray();

            if (is_callable(\$this->cloneRules)) {
                \$response = call_user_func_array(\$this->cloneRules, [\$data, \$request]);
                if (\$response instanceof \\Illuminate\\Http\\JsonResponse) {
                    return \$response;
                }
                if (is_array(\$response))   {
                    \$data = \$response;
                }

            }

            \$item = {model}::create(\$data);

            \$item->load({loaders});

            return (new {model}Resource(\$item))
                ->response()
                ->setStatusCode(201);
            
        } catch (\Exception \$exc) {
            switch (true) {

                case stripos(\$exc->getMessage(), 'SQLSTATE[23505]: Unique violation') !== false:
                    \$message = 'The unique characteristics of this registry do not allow its cloning';
                    break;

                default:
                    \$message = 'Unable to clone this record';
                    break;
            }
            throw new \Exception(
                __tr(\$message)
                    . \$exc->getMessage(),
                400
            );
        }
    }

    //{extras}
}    
";
