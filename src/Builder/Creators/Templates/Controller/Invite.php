<?php
$default = include __DIR__ . '/Default.php';
$extras = <<<EOF

public function search(Request \$request)
{
    return new UserResource(
        User::select('*')
            ->where('email', '=', \$request->input('email'))->firstOrFail()
    );
}

    /**
     * Get a list of document types.
     *
     * @param InviteRequest \$request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(InviteRequest \$request)
    {
        \$list = \$this->getQuery(\$request);

        // order
        \$this->controller->setOrder(\$list);

        \$list->select('invites.*', 'companies.name as company_name', 'companies.email as company_email')
            ->join('companies', 'companies.id', '=', 'profiles.company_id')
            ->where('invites.status', '=', 'PENDING');

        // search
        if (null !== \$this->search) {
            \$list->where(\$this->search);
        }

        // limit per page. Default 30. Max 100
        \$limit = \$request->input('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

        return InviteResource::collection(
            \$list->paginate(\$limit > 100 ? 100 : \$limit)
        );
    }

public function store(InviteRequest \$request)
    {
        \$profile = Profile::find(\$request->input('profile_id'));

        \$emailInvited = \$request->input('email');

        \$emailInvited = mb_strtolower(\$emailInvited);

        if (\$profile->is_root === true) {
            throw new BadRequestException(__tr('Profile root is only to owner of account'), 400);
        }

        // temos que validar se o usuário está se auto convidando
        \$userRequest = Auth::user();

        if (\$emailInvited === \$userRequest->email) {
            return response()->json([
                'message' => __tr('User cannot invite himself'),
            ], 400);
        }

        \$userInvited = User::select('id')
            ->where('email', '=', \$emailInvited)->first();

        // temos que validar se o usuário convidado
        // já não tem um profile na empresa
        if (\$userInvited) {
            \$userProfilesCount = UserProfile::query()
                ->join('profiles', 'user_profile.profile_id', '=', 'profiles.id')
                ->where('user_profile.user_id', \$userInvited->id)
                ->where('profiles.company_id', \$request->input('jwt_company_id'))
                ->select('user_profiles.id')
                ->count();

            if (\$userProfilesCount > 0) {
                return response()->json([
                    'message' => __tr('User invited is already on company'),
                ], 400);
            }
        }


        \$this->initCondition(\$request);

        // valido se já há um convite pro o e-mail na empresa convidada com status pending
        \$countPendingInviteOnCompany = Invite::whereEmail(\$emailInvited)
            ->whereStatus('PENDING')
            ->whereHas('profile', function (\$query) use (\$profile) {
                \$query->where('company_id', \$profile->company_id);
            })->count();

        if (\$countPendingInviteOnCompany > 0) {
            return response()->json([
                'message' => __tr('User has a pending invitation for the company')
            ], 400);
        }

        \$item = DB::transaction(function () use (\$request, \$userInvited, \$emailInvited) {

            \$url = env('FRONTEND_URL') . '/#/login';

            \$item = null;
            // Check if the user with the email exists
            if (!\$userInvited) {
                // Create an Invite with status set to PENDING
                \$item = Invite::create(
                    array_merge(
                        \$request->all(),
                        ['status' => 'PENDING',]
                    )
                );

                \$item->load('profile');

                \$inviteHash = Crypt::encrypt(\$item->profile_id);

                \$url = env('FRONTEND_URL') . '/#/register?invitation=' . \$inviteHash;
            } else {
                \$payloadCreateInvite = array_merge(\$request->all(), ['email' => \$emailInvited]);

                \$item = Invite::create(\$payloadCreateInvite);

                \$userProfile = new UserProfile();
                \$userProfile->fill([
                    'user_id' => \$userInvited->id,
                    'profile_id' => (int)\$request->input('profile_id'),
                ]);
                if (property_exists(\$userProfile, 'invite_id')) {
                    \$userProfile->invite_id = \$item->id;
                }
                \$userProfile->save();
            }

            // notify to user
            Queue::create([
                'type' => 'sendmail',
                'data' => [
                    'name' => \$item->name,
                    'email' => \$item->email,
                    'subject' => __tr('Invite'),
                    'config' => [
                        'template_id' => env('SENDGRID_INVITE'),
                        'template_data' => [
                            'name' => \$item->name,
                            'company' => Company::find(\$request->input('jwt_company_id'))->name,
                            'url_redirect' => \$url
                        ]
                    ]
                ]
            ]);
            return \$item;
        });

        return (new InviteResource(\$item))
            ->response()
            ->setStatusCode(201);
    }

public function decide(Request \$request, \$id)
{
    \$invite = Invite::where('email', '=', Auth::user()->email)
        ->where('invites.id', (int) \$id)
        ->firstOrFail();

    // avaliar status
    if (\$invite->status !== 'PENDING') {
        throw new Exception(__tr('This invitation has already been decided before'), 400);
    }

    // Demais validações
    \$request->validate([
        'decision' => 'required|in:ACCEPTED,REJECTED',
        'comments' => 'nullable|string'
    ], [
        'decision.required' => 'decision: ' . __tr('The field is required'),
        'decision.in' => 'decision: ' . __tr('The values acccept are ') . 'ACCEPTED or REJECTED',
        'comments.invalid' =>  'comments: ' . __tr('Must be a text'),
    ]);

    DB::transaction(function () use (\$request, \$invite) {
        // atualizar o invite
        \$invite->update([
            'decided_at' => Carbon::now(),
            'decision_comments' => \$request->input('comments'),
            'status' => mb_strtoupper(\$request->input('decision'))
        ]);

        // gerar o user-profile
        if (\$request->input('decision') === 'ACCEPTED') {
            \$user = User::where('email', '=', \$invite->email)->firstOrFail();
            UserProfile::create([
                'profile_id' => \$invite->profile_id,
                'user_id' => \$user->id
            ]);
        }
        // notificar os interessados
        \$company = Company::select('companies.*')
            ->join('profiles', 'profiles.company_id', '=', 'companies.id')
            ->where('profiles.id', '=', \$invite->profile_id)
            ->firstOrFail();


        // notification to user
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => \$user->name,
                'email' => \$user->email,
                'subject' => __tr('Decided invitation'),
                'config' => [
                    'template_id' => env('SENDGRID_INVITE_DECIDED_USER'),
                    'template_data' => [
                        'name' => \$user->name,
                        'company' => \$company->name,
                        'decision' => __tr(\$request->input('decision'))
                    ]
                ]
            ]
        ]);

        // notification to company
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => \$company->name,
                'email' => \$company->email,
                'subject' => __tr('Decided invitation'),
                'config' => [
                    'template_id' => env('SENDGRID_INVITE_DECIDED_USER'),
                    'template_data' => [
                        'name' => \$company->name,
                        'invited' => \$user->name,
                        'decision' => __tr(\$request->input('decision'))
                    ]
                ]
            ]
        ]);
    });

    return new InviteResource(Invite::where('email', '=', Auth::user()->email)
    ->where('invites.id', (int) \$id)
    ->firstOrFail());
}

       public function inviteMe(Request\$request)
    {
       \$list = UserProfile::select('user_profile.id','user_profile.created_at', 'user_profile.profile_id', 'companies.name as company_name', 'companies.email as company_email')
            ->join('profiles', 'profiles.id', '=', 'user_profile.profile_id')
            ->join('companies', 'companies.id', '=', 'profiles.company_id')
            ->where('user_profile.user_id', Auth::id())
            ->where('companies.id', '<>',\$request->company_id)->with('profile');

        // Limit per page. Default 30. Max 100
       \$limit =\$request->input('per_page') ?? env('LIMIT_PER_PAGE') ?? 30;

        // Order
        if (\$request->input('order')) {
            if (!in_array(\$request->input('order'),\$this->sortable)) {
                return response()->json(['error' => __tr('The requested field is not enabled for sorting')], 400);
            }
           \$request->input('sort') === 'desc'
                ?\$list->orderByDesc(\$request->input('order'))
                :\$list->orderBy(\$request->input('order'));
        }

        if (\$request->input('search') !== null) {
           \$list->where(function (\$query) use (\$request) {
               \$query->orWhere('companies.email', 'ILIKE', "%{\$request->input('search')}%");
               \$query->orWhere('companies.name', 'ILIKE', "%{\$request->input('search')}%");
            });
        }

       \$paginatedList =\$list->paginate(\$limit > 100 ? 100 :\$limit);
       \$paginatedList->getCollection()->transform(function (\$item) {
           \$item['company'] = [
                'name' =>\$item->company_name,
                'email' =>\$item->company_email
            ];
            unset(\$item->company_name,\$item->company_email);
            return \$item;
        });

       \$response = [
            'data' =>\$paginatedList->items(),
            'meta' => [
                'current_page' =>\$paginatedList->currentPage(),
                'from' =>\$paginatedList->firstItem(),
                'last_page' =>\$paginatedList->lastPage(),
                'path' =>\$paginatedList->path(),
                'per_page' =>\$paginatedList->perPage(),
                'to' =>\$paginatedList->lastItem(),
                'total' =>\$paginatedList->total(),
            ],
            'links' => [
                'first' =>\$paginatedList->url(1),
                'last' =>\$paginatedList->url(\$paginatedList->lastPage()),
                'prev' =>\$paginatedList->previousPageUrl(),
                'next' =>\$paginatedList->nextPageUrl(),
            ],
        ];

        return \$response;
    }
EOF;

$uses = <<<EOF
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\Profile;
use App\Modules\Application\Models\Queue;
use App\Modules\Company\Models\UserProfile;
use App\Modules\Company\Http\Resources\UserResource;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Illuminate\Support\Facades\Crypt;
EOF;

$out = str_replace(
    ['public function store({model}Request', 'public function index({model}Request', '//{extras}', '//{extrasUses}'],
    ['public function default__store({model}Request', 'public function default__index({model}Request', $extras, $uses],
    $default
);

return $out;
