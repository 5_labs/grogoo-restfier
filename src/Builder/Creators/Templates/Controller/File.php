<?php

$default = include __DIR__ . '/Default.php';
$extras = <<<EOF
/**
 * Store a new document type.
 *
 * @param {model}Request \$request
 * @return \Illuminate\Http\JsonResponse
 */
public function store({model}Request \$request)
{
    
    // Storage
    \$path = \$this->storeFileOnStorage(\$request);

    // DB
    \$file = \$request->file('file');
    \$filename = \$file->getFilename() . '.' . \$file->getClientOriginalExtension();
    \$request->merge([
        'path' => \$path,
        'filename' => \$filename,
        'name' => \$file->getClientOriginalName(),
        'extension' => \$file->getClientOriginalExtension(),
        'mime' => \$file->getClientMimeType(),
        'size' => \$file->getSize(),
        'storage' => env('FILESYSTEM_DISK', 'local')
    ]);

    \$this->initCondition(\$request);

    \$item = {model}::create(\$request->all());

    \$item->load({loaders});

    return (new {model}Resource(\$item))
    ->response()
    ->setStatusCode(201);
}

protected function storeFileOnStorage(FileRequest \$request): string
    {
        \$file = \$request->file('file');

        // Store
        \$path = \$file->store(
            \$request->company_id
                . 'fL' . substr(sha1(\$request->company_id), 0, 6)
                . '/' . date('Y')
                . '/' . date('m')
        );

        if (!\$path) {
            throw new \Exception(__tr('File upload failed'));
        }

        return \$path;
    }

    public function storeUserFile(FileRequest \$request)
    {
        // Storage
        \$path = \$this->storeFileOnStorage(\$request);

        // DB
        \$file = \$request->file('file');
        \$filename = \$file->getFilename() . '.' . \$file->getClientOriginalExtension();
        \$request->merge([
            'path' => \$path,
            'filename' => \$filename,
            'name' => \$file->getClientOriginalName(),
            'extension' => \$file->getClientOriginalExtension(),
            'mime' => \$file->getClientMimeType(),
            'size' => \$file->getSize(),
            'storage' => env('FILESYSTEM_DISK', 'local')
        ]);

        \$this->initCondition(\$request);

        \$item = File::create(\$request->all());

        return (new FileResource(\$item))
        ->response()
        ->setStatusCode(201);
    }

    public function destroyUserFile(string \$id, FileRequest \$request)
    {
        \$this
        ->read(\$request, (int) \$id)
        ->delete();
        
        return response()->json([], 204);
    }
EOF;

$uses = <<<EOF
EOF;

$out = str_replace(
    ['public function store({model}Request', '//{extras}', '//{extrasUses}'],
    ['public function default__store({model}Request', $extras, $uses],
    $default
);

return $out;
