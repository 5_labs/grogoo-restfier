<?php

$default = include __DIR__ . '/Default.php';
$extras = <<<EOF

public function me(Request \$request)
{
    \$profile_id = \$request->get('jwt_profile_id');

    \$profile = \App\Modules\Company\Models\Profile::where('id', \$profile_id)->firstOrFail();
    \$user = \Illuminate\Support\Facades\Auth::user();
    \$user->is_root = \$profile->is_root;

    \$user->load('file');

    \$subscription = \App\Modules\Subscription\Models\Subscription::with('plan')->where([
        ['company_id', '=', \$profile->company->id],
        ['status', '=', 'SUCCESS']
    ])->first();


    return response()->json([
        'user' => (new \App\Modules\Company\Http\Resources\UserResource(
            \$user
        )),
        'company' => (new \App\Modules\Company\Http\Resources\CompanyResource(
            \$profile->company
        )),
        'subscription' => \$subscription,
        'permissions' => \$profile->getPermissionsAllowed()
    ], 200);
}

public function updateMe(UserRequest \$request)
{
    \$item = \$this->read(\$request, (int) \$request->get('jwt_user_id'));

    \$item->update(\$request->all());

    return new UserResource(\$item);
}

EOF;

$out = str_replace(
    ['//{extras}', '//{extrasUses}'],
    [$extras, ''],
    $default
);

return $out;
