<?php

$string = include __DIR__ . '/Default.php';


return str_replace(
    ['$data = [{items}];'],
    ["\$data = [
                'id' => \$this->id,
                'title' => \$this->title, 
                'created_at' => \$this->created_at,
                'valid_at' => \$this->valid_at,
                'hash' => \$this->hash,
                'last_usage_date' => \$this->last_usage_date, 
                'last_usage_ip' => \$this->last_usage_ip, 
                'usage_hits' => \$this->usage_hits
            ];"],
    $string
);
