<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

abstract class Abstract{model}Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request \$request): array
    {
        \$data = [];
        if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            {relations}
            \$data = [{items}];

        }

        {conditional_fields}

        return \$data;

    }
}
";
