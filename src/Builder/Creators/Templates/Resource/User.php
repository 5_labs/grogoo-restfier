<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Company\Models\UserProfile;

abstract class Abstract{model}Resource extends JsonResource
{
        public function isRoot(\$request)
        {
            \$isRoot = UserProfile::select('profiles.is_root')
            ->join('profiles', 'profiles.id', 'user_profile.profile_id')
            ->where('user_profile.user_id', \$this->id)
            ->where('profiles.company_id', \$request->company_id)->first();

            if (empty(\$isRoot)) {
                return null;
            }

            return \$isRoot->is_root;
        }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request \$request): array
    {
        \$data = [];
        if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            {relations}
            \$data = [{items}];

        }

        {conditional_fields}

        return \$data;

    }
}
";
