<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use Exception;

abstract class Abstract{model}Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request \$request): array
    {
        \$data = [];
        if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            {relations}
            \$data = [{items}];

            // URL
            try {
                \$url = Storage::temporaryUrl(\$this->path, now()->addMinutes(30));
                \$data['url'] = \$url;
                \$data['valid_at'] = now()->addMinutes(30)->format('c');    
            } catch (Exception \$exc) {
                \$data['url'] = 'url-not-supported';
            }

            unset(\$data['recognition']);


        }

        return \$data;

    }
}
";
