<?php

$string = include __DIR__ . '/Default.php';


return str_replace(
    ['$data = [{items}];'],
    ["\$data = array_merge(
        [{items}], 
        [
            'group_label' => __tr(\$this->group), 
            'route_label' => __tr(\$this->route), 
            'verb_label' => __tr(\$this->verb), 
        ]);
        if (isset(\$this->has)) {
            \$data['has'] = \$this->has;
        }
        "],
    $string
);
