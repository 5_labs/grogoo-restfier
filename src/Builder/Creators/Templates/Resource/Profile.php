<?php

$string = include __DIR__ . '/Default.php';


return str_replace(
    ['$data = [{items}];'],
    ["\$data = array_merge(
        [{items}], 
        ['permissions' => (new \GrogooRestfier\Modules\Auth\Http\Controllers\PermissionsController())->getPermissions((int) \$this->id)]
    );"],
    $string
);
