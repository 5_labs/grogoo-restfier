<?php

return "<?php

namespace App\Generated\Modules\{module}\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

abstract class Abstract{model}Resource extends JsonResource
{
    public function getUserProfile(int \$profile_id, string \$email)
    {
        \$userProfile = DB::table('user_profile')
            ->select('user_profile.*')
            ->join('users', 'user_profile.user_id', '=', 'users.id')
            ->where('profile_id', '=', \$profile_id)
            ->where('users.email', '=', \$email)
            ->first();

        if (\$userProfile) {
            return \$userProfile;
        }

        return null;
    }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request \$request): array
    {
        \$data = [];
        if (\$this->resource instanceof \Illuminate\Database\Eloquent\Model) {
            {relations}
            \$data = [{items}];

        }

        {conditional_fields}

        return \$data;

    }
}
";
