<?php

return "<?php

namespace App\Generated\Modules\Subscription\Http\Middlewares;

use Closure;
use Exception;
use Illuminate\Http\Request;

abstract class AbstractCheckSubscriptionLimit
{
    public array \$LIMITERS = [];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @param  \Closure  \$next
     * @return mixed
     */
    public function handle(Request \$request, Closure \$next)
    {
        if (config('app.env') === 'testing')
            return \$next(\$request);

        \$subscription = \$request->get('company_subscription');
        if (!\$subscription) {
            return \$next(\$request);
        }

        \$routeName = \$request->route()->getName();

        if (
            (!array_key_exists(\$routeName, \$this->LIMITERS)) ||
            !(new(\$this->LIMITERS[\$routeName]))->isExceeded(\$subscription)
        ) {
            return \$next(\$request);
        }
        return response()->json([
            'msg' => __tr('Resource exceeded limit'),
        ], 426);
    }
}
";