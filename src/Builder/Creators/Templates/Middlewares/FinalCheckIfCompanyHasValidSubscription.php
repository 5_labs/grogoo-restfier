<?php

return "<?php

namespace App\Modules\Subscription\Http\Middlewares;

use App\Generated\Modules\Subscription\Http\Middlewares\AbstractCheckIfCompanyHasValidSubscription;

final class CheckIfCompanyHasValidSubscription extends AbstractCheckIfCompanyHasValidSubscription
{
    // Sobrescreva essa função para caso queira adicionar validações extras para busca do company_id por questão de negócio
    // public function extraValidation(\Illuminate\Http\Request \$request) ?int
    // {
    // }

    // Sobrescreva essa função para caso queira alterar a validação da assinatura
    // public function validSubscription(\Illuminate\Http\Request \$request) ?int
    // {
    // }
}
";
