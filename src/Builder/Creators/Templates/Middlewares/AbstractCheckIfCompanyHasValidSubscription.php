<?php

return "<?php

namespace App\Generated\Modules\Subscription\Http\Middlewares;

use App\Modules\Subscription\Models\Subscription;
use Closure;
use Exception;
use Illuminate\Http\Request;

abstract class AbstractCheckIfCompanyHasValidSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @param  \Closure  \$next
     * @return mixed
     */
    public function handle(Request \$request, Closure \$next)
    {
        if(config('app.env') === 'testing')
            return \$next(\$request);

        \$company_id = null;

        // identificação da empresa logada ou relacionada ou scritp carregado
        if (\$request->has('jwt_company_id')) {
            \$company_id = \$request->get('jwt_company_id');
        } else {
            \$company_id = \$this->extraValidation(\$request);
            if (!\$company_id)
                return response()->json(['msg' => __tr('Company not identified')], 401);
        }

        try {
            \$subscription = \$this->validSubscription(\$company_id);

            \$request->merge([
                'company_subscription' => \$subscription,
            ]);
        } catch (Exception \$e) {
            return response()->json([
                'msg' => __tr('Company not has active subscription')
            ], 402);
        }

        return \$next(\$request);
    }

    public function extraValidation(Request \$request): ?int
    {
        return null;
    }

    public function validSubscription(int \$company_id): Subscription
    {
        \$subscription = Subscription::where([
            ['company_id', \$company_id],
            ['status', 'SUCCESS']
        ])->firstOrFail();

        if (!\$subscription->isValid()) {
            throw new Exception('Company not has valid subscription');
            // response()->json([
            //     'msg' => __tr('Company not has valid subscription')
            // ], 402);
        }

        return \$subscription;
    }
}
";
