<?php

return "<?php

namespace App\Modules\Subscription\Http\Middlewares;

use App\Generated\Modules\Subscription\Http\Middlewares\AbstractCheckSubscriptionLimit;

class CheckSubscriptionLimit extends AbstractCheckSubscriptionLimit
{
    // Para implementar os limitadores, faça as implementações dos limitadores, e registre aqui por rota
    // public array \$LIMITERS = [
    //     'document.store' => DocumentLimiter::class,
    //     'document.scriptGenerator' => ViewsLimiter::class,
    // ];
}
";