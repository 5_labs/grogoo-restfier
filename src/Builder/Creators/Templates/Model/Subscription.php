<?php

return "<?php

namespace App\Generated\Modules\{module}\Models;
    
{softDeletesA}

use App\Modules\{module}\Database\Factories\{model}Factory;
use Carbon\Carbon;

abstract class Abstract{model} extends Model
{
    {softDeletesB}

    protected \$table = '{table}';
    protected \$fillable = [{fillable}];
    protected \$casts = [{casts}];
    {ignoreTimestamps}

    protected static function boot()
    {
        parent::boot();
        
        if (class_exists(\App\Modules\{module}\Events\Create\{model}CreateEvent::class)) {
            static::created(function (\$model) {
                event(new \App\Modules\{module}\Events\Create\{model}CreateEvent(\$model));
            });
        }
    }


    {relations}

    {prunable_function}

    protected static function newFactory(): {model}Factory
    {
        return new {model}Factory();
    }

    public function isPending(): bool
    {
        return \$this->status === 'PENDING';
    }

    public function isRunning(): bool
    {
        return \$this->status === 'RUNNING';
    }

    public function isError(): bool
    {
        return \$this->status === 'ERROR';
    }

    public function isSuccess(): bool
    {
        return \$this->status === 'SUCCESS';
    }

    public function isExpired(): bool
    {
        return !Carbon::parse(\$this->end_date)->greaterThanOrEqualTo(Carbon::now()->startOfDay());
    }

    public function isValid(): bool
    {
        \$isExpired = \$this->isExpired();
        \$isActived = \$this->isSuccess();

        return (!\$isExpired && \$isActived);
    }

}
";
