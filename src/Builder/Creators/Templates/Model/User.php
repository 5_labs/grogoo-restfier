<?php

return "<?php

namespace App\Generated\Modules\{module}\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Prunable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Modules\Company\Database\Factories\UserFactory;

class AbstractUser extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable,  SoftDeletes, Prunable;

    protected \$primaryKey = '{primaryKey}';

    /**
        * The attributes that are mass assignable.
        *
        * @var array<int, string>
        */
    protected \$fillable = [
        'name',
        'email',
        'password',
        'file_id',
        'additional_info',
        'additional_info_answered',
    ];

    /**
        * The attributes that should be hidden for serialization.
        *   
        * @var array<int, string>
        */
    protected \$hidden = [
        'password',
        'remember_token',
    ];

    /**
        * The attributes that should be cast.
        *
        * @var array<string, string>
        */
    protected \$casts = [
        'email_verified_at' => 'datetime',
        'additional_info' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();
        
        if (class_exists(\App\Modules\{module}\Events\Create\{model}CreateEvent::class)) {
            static::created(function (\$model) {
                event(new \App\Modules\{module}\Events\Create\{model}CreateEvent(\$model));
            });
        }
    }


    public function getJWTIdentifier()
    {
        return \$this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function file()
    {
        return \$this->belongsTo(\App\Modules\Application\Models\File::class);
    }

    public function prunable()
    {
        return static::where('deleted_at', '<=', now()->subMonth());
    }

    protected static function newFactory(): UserFactory
    {
        return new UserFactory();
    }

    public function setEmailAttribute(\$value)
    {
        \$this->attributes['email'] = mb_strtolower(\$value);
    }
}";
