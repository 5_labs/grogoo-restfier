<?php

namespace App\Console\Commands\GrogooCommands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class RunTestsCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:test');
    }

    public function handle(): void
    {
        Helper::commandPrintHeader('Run Tests Pest');

        if (env('APP_ENV') !== 'testing') {
            throw new Exception('ENV must be testing');
        }
        ;

        Artisan::call('cache:clear');

        Artisan::call('route:clear');

        Helper::shellExec('composer install --quiet');

        $command = 'cd ' . Helper::getPathApp()
            . ' && '
            . "./vendor/bin/pest --stop-on-failure --testdox --coverage-html ./coverage tests/Feature";

        Helper::shellExec($command);

        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R');
    }
}
