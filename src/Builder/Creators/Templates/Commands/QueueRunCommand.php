<?php

namespace App\Console\Commands\GrogooCommands;

use App\Modules\Application\Http\Controllers\QueueController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class QueueRunCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:queue');
    }

    public function handle(): void
    {
        $queue = new QueueController();
        do {
            $continue = $queue->execute();
        } while ($continue);
    }
}
