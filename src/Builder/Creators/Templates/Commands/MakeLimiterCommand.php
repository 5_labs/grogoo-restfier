<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use Nette\Utils\FileSystem;

class MakeLimiterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:limiter {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'creates a new limiter';


    public function handle()
    {
        $name = $this->argument('name');
        $path = app_path("/Modules/Subscription/Limiters/{$name}Limiter.php");

        if(file_exists($path)){
            $this->error("The file {$name}Limiter.php already exists");
            return;
        }

        $stub = $this->getStub($name);
        $content = str_replace('{{name}}', $name, $stub);

        file_put_contents($path, $content);
        $this->info("Limiter {$name} criado com sucesso em app/Modules/Subscription/Limiters.");
    }

    protected function getStub($name)
    {
        $stub = <<<STUB
            <?php

            namespace App\Modules\Subscription\Limiters;

            use App\Modules\Subscription\Models\Subscription;

            class {$name}Limiter implements LimiterInterface
            {
                public function isExceeded(Subscription \$subscription): bool
                {
                    // Implement your logic here
                    return false;
                }
            }
        STUB;

        return $stub;
    }
}

