<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Builder\Codebox;
use GrogooRestfier\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class InstallCodeboxCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:codebox:config');
    }

    public function handle(): void
    {
        Helper::commandPrintHeader('Install Initial Files');

        Helper::shellExec('composer install --quiet');

        Codebox::config();
    }
}
