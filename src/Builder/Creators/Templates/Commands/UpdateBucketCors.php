<?php

namespace App\Console\Commands\GrogooCommands;

use GrogooRestfier\Helpers\GoogleCloud;
use GrogooRestfier\Helpers\Helper;
use Illuminate\Console\Command;
use GrogooRestfier\Helpers\UpdateEnvFile;
use GrogooRestfier\Helpers\Str;

class UpdateBucketCors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-bucket-cors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        GoogleCloud::setCors();
    }
}
