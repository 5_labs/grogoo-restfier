<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use GrogooRestfier\Helpers\Helper;

class ClearFilesFromBuilderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        echo shell_exec(
            'cd ' . Helper::getPathApp()
            . ' && '
            . "find . -type f -name '__NEW*' -exec rm {} \;"
        );
    }
}
