<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use GrogooRestfier\Modules\Auth\Http\Middlewares\CheckPermission;
use Illuminate\Support\Facades\Route;

class PermissionsCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create the permission by the route aliases';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Constant that indicates which routes require a permission to be created.
        $PERMISSION_MIDDLEWARE = "Auth.check.permission";
        $routes = Route::getRoutes();

        foreach ($routes as $route) {
            $middlewares = $route->gatherMiddleware();

            if (in_array($PERMISSION_MIDDLEWARE, $middlewares)) {
                $name = $route->getName();
                $group = (explode("\\", $route->getActionName())[2] ?? 'Company');

                $parts = explode('.', $name);
                $verb = array_pop($parts);
                $route = array_pop($parts);

                if ($group !== 'Subscription' && $group !== 'Application') {
                    CheckPermission::createIfNotExists($group, $route, $verb);
                }
            }
        }
    }
}
