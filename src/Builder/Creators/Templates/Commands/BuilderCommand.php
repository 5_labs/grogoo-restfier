<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Helpers\Helper;

class BuilderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application Builder';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R');

        // echo "Update composer\n";
        // shell_exec('cd ' . Helper::getPathApp() . ' && composer update --quiet');

        \GrogooRestfier\Builder\Builder::run();

        Artisan::call('app:clear');

        shell_exec('cd ' . Helper::getPathApp() . ' && composer dump-autoload --quiet');

        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R');

        Artisan::call('cache:clear');

        Artisan::call('route:clear');

        echo "\n";
    }
}
