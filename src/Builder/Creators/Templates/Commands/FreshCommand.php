<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Helpers\Helper;
use Symfony\Component\Console\Input\InputOption;

class FreshCommand extends Command
{

    protected function configure()
    {
        $this->setName('app:fresh');
    }

    public function handle(): void
    {

        Artisan::call('cache:clear');

        Artisan::call('route:clear');

        Helper::shellExec('composer install --quiet');

        Artisan::call('migrate:fresh');

        Artisan::call('app:build');

        Artisan::call('migrate --seed');
    }
}
