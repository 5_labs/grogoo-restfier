<?php

namespace App\Console\Commands\GrogooCommands;

use GrogooRestfier\Builder\CreateApikeysModule;
use Illuminate\Console\Command;

class InstallApikeysModuleCommand extends Command
{
    protected $signature = "grogoo:install-apikeys-module";
    protected $description = "Grogoo Install Apikeys Module";
    public function handle(): void
    {
        CreateApikeysModule::handle();
    }
}
