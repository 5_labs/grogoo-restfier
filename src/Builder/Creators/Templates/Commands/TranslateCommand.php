<?php

namespace App\Console\Commands\GrogooCommands;

use GrogooRestfier\Builder\TranslateDicionaryAI;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Helpers\Helper;

class TranslateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:translate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application Builder';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R');

        TranslateDicionaryAI::handle(env('OPENAI_APIKEY'), Helper::getPathApp() . '/lang/en.json');

        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R');

        Artisan::call('cache:clear');

        Artisan::call('route:clear');

        echo "\n";
    }
}
