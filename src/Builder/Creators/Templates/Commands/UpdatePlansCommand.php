<?php

namespace App\Console\Commands\GrogooCommands;

use Illuminate\Console\Command;

class UpdatePlansCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'signatures:update-plans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create the permission by the route aliases';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // logger('configs', [config('services.superlogica.app_token'), config('services.superlogica.access_token')]);
        if (class_exists('App\Modules\Subscription\Jobs\UpdatePlans')) {
            dispatch(new App\Modules\Subscription\Jobs\UpdatePlans\UpdatePlans)->onConnection('sync');
        }
    }
}
