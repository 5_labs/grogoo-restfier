<?php

return "<?php

use Illuminate\Support\Facades\Route;


//{extras_up}


    Route::group(['middleware' => [
        'Auth.check.token',
        \App\Modules\Subscription\Http\Middlewares\CheckIfCompanyHasValidSubscription::class,
        \App\Modules\Subscription\Http\Middlewares\CheckSubscriptionLimit::class,
        'Auth.check.permission',
        GrogooRestfier\Modules\Auth\Http\Middlewares\SetHashToApikeyMiddleware::class, 
        '{module}.check.belong.to', 
        ]
    ], function () {

        // clone routers
        {cloneRouters}

        // RestFUL routers
        {routers}
    });


//{extras_down}
";
