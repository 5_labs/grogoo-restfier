<?php

$default = include __DIR__ . '/Default.php';

$extras = <<<EOF
    Route::group([
    'middleware' => [
        'Auth.check.token',
        \App\Modules\Subscription\Http\Middlewares\CheckIfCompanyHasValidSubscription::class,
        \App\Modules\Subscription\Http\Middlewares\CheckSubscriptionLimit::class,
        'Company.check.belong.to',
    ]
], function () {
    Route::post('user-file', [\App\Modules\Application\Http\Controllers\FileController::class, 'storeUserFile']);
    Route::delete('user-file/{id}', [\App\Modules\Application\Http\Controllers\FileController::class, 'destroyUserFile']);
});

EOF;

return str_replace('//{extras_up}', $extras, $default);
