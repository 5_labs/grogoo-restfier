<?php

$default = include __DIR__ . '/Default.php';

$extras = <<<EOF
    Route::group([
        'middleware' => [
            'Auth.check.token',
            'Auth.check.permission',
            'Company.check.belong.to'
        ]
    ], function () {
        Route::post('profile-permission/{id}/batch', [\App\Modules\Company\Http\Controllers\ProfilePermissionController::class, 'batchStore'])->name('profile-permission.batch');
    });
EOF;

return str_replace('//{extras_up}', $extras, $default);
