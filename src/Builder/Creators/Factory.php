<?php

namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Factory
{

    private $template = "<?php

    namespace App\Generated\Modules\{module}\Database\Factories;
    
    use Illuminate\Database\Eloquent\Factories\Factory;
    use App\Modules\{module}\Models\{model};
    
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\{module}\Models\{model}>
     */
    abstract class Abstract{model}Factory extends Factory
    {

        /**
         * The name of the factory's corresponding model.
         *
         * @var string
         */
        protected \$model = {model}::class;


        /**
         * Define the model's default state.
         *
         * @return array<string, mixed>
         */
        public function definition(): array
        {
            {dowhile}

            return [
                {items}
            ];
        }
    }
    ";

    private $templateModule = "<?php

    namespace App\Modules\{module}\Database\Factories;
    
    use App\Generated\Modules\{module}\Database\Factories\Abstract{model}Factory;
    
    class {model}Factory extends Abstract{model}Factory
    {
    }
    ";




    public function handle(Data $data, $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Database/Factories';
        // echo "\n- Factories: ";

        $module = $data->getData()->module;

        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            $tableItens = [];

            foreach ($item->fields as $field) {

                // Belogn to
                if (!isset($field->type)) {
                    $tableItens['items'][$field->field_origin] = "'$field->field_origin'"
                        . '=>'
                        . HelperCreator::getNameSpace($data, $field->table, '\\Models\\')
                        . $field->model . '::inRandomOrder()->first()->id ?? null';

                    continue;
                }

                // ignore
                if (
                    $field->isPrimaryKey
                    || stripos($field->column_name, 'createtime') !== false
                    || stripos($field->column_name, 'created_at') !== false
                    || stripos($field->column_name, 'updated_at') !== false
                    || stripos($field->column_name, 'deleted_at') !== false
                    || stripos($field->type, 'tsvector') !== false
                ) {
                    continue;
                }


                // Types
                $value = '';
                switch (true) {

                    case isset($field->enum) && null !== $field->enum:
                        $value = 'fake()->randomElement([' . implode(',', array_map(fn ($item) => '"' . $item . '"', $field->enum)) . '])';
                        break;

                    case stripos($field->column_name, 'name') !== false && $item->table === 'companies':
                    case stripos($field->column_name, 'name') !== false && $item->table === 'filials':
                        $value = 'fake()->unique()->company()';
                        break;
                    case stripos($field->column_name, 'name') !== false && stripos($item->table, 'product') !== false:
                    case stripos($field->column_name, 'name') !== false && stripos($item->table, 'document') !== false:
                    case stripos($field->column_name, 'name') !== false && stripos($item->table, 'filials') !== false:
                        $value = 'fake()->unique()->sentence(2)';
                        break;
                    case stripos($field->column_name, 'phone') !== false:
                        $value = 'fake()->unique()->phoneNumber()';
                        break;
                    case stripos($field->column_name, 'title') !== false:
                        $value = 'fake()->unique()->sentence(10)';
                        break;
                    case stripos($field->column_name, 'verified_at') !== false:
                        $value = "fake()->datetime()->format(\"Y-m-d H:i:s\")";
                        break;
                    case stripos($field->column_name, 'name') !== false:
                        $value = 'fake()->name()';
                        break;
                    case stripos($field->column_name, 'email') !== false:
                        $value = 'fake()->unique()->safeEmail()';
                        break;
                    case $field->column_name === 'ip':
                    case $field->column_name === 'ip_address':
                        $value = 'fake()->ipv4()';
                        break;
                    case stripos($field->column_name, 'cpf') !== false || stripos($field->column_name, 'cnpj') !== false:
                        $value = 'rand(11111111111111, 99999999999999)';
                        break;
                    case $field->column_name === 'username':
                        $value = 'fake()->unique()->userName()';
                        break;
                    case $field->column_name === 'external_id':
                        $value = 'fake()->uuid()';
                        break;
                    case $field->column_name === 'filename':
                        $value = 'fake()->filename()';
                        break;
                    case $field->column_name === 'password':
                        $value = '\Illuminate\Support\Facades\Hash::make(time())';
                        break;
                    case $field->column_name === 'remember_token':
                        $value = '\Illuminate\Support\Str::random(10)';
                        break;

                    default:
                        switch (LoadDB::$config[$field->type][1]) {
                            case 'float':
                            case 'decimal':
                            case 'double':
                                $value = 'fake()->randomFloat(2,0,9999999999.99)';
                                break;
                            case 'text':
                                $value = 'fake()->sentence(300)';
                                break;
                            case 'json':
                            case 'jsonb':
                            case 'array':
                                $value = "[
                                            'browser' => fake()->userAgent(),
                                            'language' => fake()->languageCode(),
                                            'screen_resolution' => fake()->randomElement(['1920x1080', '1366x768', '1280x720']),
                                        ]";
                                break;
                            case 'boolean':
                                $value = 'fake()->boolean()';
                                break;
                            case 'integer':
                                $value = 'fake()->numberBetween(1, 100000000)';
                                break;
                            case 'date':
                                $value = "fake()->datetime()->format(\"Y-m-d\")";
                                break;
                            case 'datetime':
                            case 'timestamp':
                                $value = "fake()->datetime()->format(\"Y-m-d H:i:s\")";
                                break;
                            default:
                                $value = 'fake()->text(' . $field->maxsize . ')';
                                break;
                        }
                }

                $tableItens['items'][$field->column_name] = "'{$field->column_name}' => $value";
            }

            // chaves unicas
            $dowhile = '';
            if (count($item->uniquesConstraint) > 0) {
                $dowhile = "\$i=0;do {\n \$i++;\n";
                $wheres = [];
                foreach ($item->uniquesConstraint as $cons) {
                    if (!isset($tableItens['items'][$cons->column_name])) {
                        // echo "\n- Not found: $item->table, $cons->column_name";
                        continue;
                    }
                    $dowhile .= '$' . str_replace(["'", '=>'], ['', '='], $tableItens['items'][$cons->column_name]) . ';';

                    $tableItens['items'][$cons->column_name] = "'{$cons->column_name}' => $" . $cons->column_name;

                    $wheres[] = ((count($wheres) === 0)  ? '::' : '->')
                        . "where('" . $cons->column_name . "', \$" . $cons->column_name . ")";
                }
                $Model = HelperCreator::getNameSpace($data, $item->table, '\\Models\\') . $item->model;
                $dowhile .= "\n} while (\$i<100 && $Model" . implode('', $wheres) . "->exists());";
            }

            // while (\App\Modules\Company\Models\UserProfile::where('user_id', $userId)->where('profile_id', $profileId)->exists());

            $update = [
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'model_camelcase' => trim(lcfirst($item->model)),
                'items' => implode(", \n", $tableItens['items'] ?? []),
                'dowhile' => $dowhile
            ];

            SaveFile::save(self::class, $module,  $item->model, new Template($this->template, $update), new Template($this->templateModule, $update));
        }
        return SaveFile::countGeneratedFiles(self::class, $module);
    }
}
