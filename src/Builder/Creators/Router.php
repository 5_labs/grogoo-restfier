<?php


namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;

class Router implements CreateInterface
{

    private $ignoreRouters;
    private $template = '';

    private $templateModule = "<?php
    \$apiGeneratedPath = base_path() . '/app/Generated/Modules/{module}/Routes/api.php';
    if (file_exists(\$apiGeneratedPath)) {
        include \$apiGeneratedPath;
    }
    ";

    private function getExcept($table)
    {
        $out = '';
        if (isset($this->ignoreRouters[$table])) {
            if ($this->ignoreRouters[$table] === true) {
                $out = "->except(['index', 'show', 'store', 'update', 'destroy'])";
            } else {
                $items = implode(',', array_map(fn ($i) => "'$i'", $this->ignoreRouters[$table]));
                $out = "->except([$items])";
            }
        }
        return $out;
    }

    public function handle(Data $data, array $ignore = [], $ignoreRouters = []): int
    {
        $this->ignoreRouters = $ignoreRouters;
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Routes/api.php';
        $module = $data->getData()->module;
        // echo "- Router: ";

        $tableItens = ['routers' => [], 'cloneRouters' => []];
        $module = $data->getData()->module;
        foreach ($data->getData()->items as $item) {
            if (array_search($item->table, $ignore) !== false) {
                continue;
            }
            
            if($item->table === 'users')
            {
                continue;
            }
            
            // ROUTER: caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table);
            // echo "\n## Table: $item->table \n";
            if (count($routers['router']) > 0) {
                foreach ($routers['router'] as $router) {
                    $parts = array_reverse(explode('.', $router));
                    // echo "\n$router";
                    array_shift($parts);
                    $routerItem = [];
                    foreach ($parts as $part) {
                        $route = explode('|', $part)[0];
                        $routerItem[] = Helper::name2KebabCase(Helper::singularize($route));
                    }
                    $routerItem[] = Helper::name2KebabCase(Helper::singularize($item->table));
                    $tableItens['routers'][] = "Route::apiResource('" . implode('.', $routerItem) . "', \\App\\Modules\\" . $module . "\\Http\\Controllers\\{$item->model}Controller::class)" . $this->getExcept($item->table) . ";";
                }
            } else {
                $tableItens['routers'][$item->table] = "Route::apiResource('" . Helper::name2KebabCase(Helper::singularize($item->table)) . "', \\App\\Modules\\" . $module . "\\Http\\Controllers\\{$item->model}Controller::class)" . $this->getExcept($item->table) . ";";
            }

            $r = str_replace('_', '-', Helper::reverteName2CamelCase($item->model));
            $tableItens['routers'][$item->table] = "Route::apiResource('$r', \\App\\Modules\\" . $module . "\\Http\\Controllers\\{$item->model}Controller::class)" . $this->getExcept($item->table) . ";";

            $tableItens['cloneRouters'][] = "Route::get('$r/{id}/clone', [\\App\\Modules\\{$module}\\Http\\Controllers\\{$item->model}Controller::class, 'clone'])->name('$r.clone');";
        }

        $update = [
            'module' => $module,
            'table' => $item->table,
            'model' => $item->model,
            'routers' => implode("\n", array_map(fn ($item) => $item, array_values($tableItens['routers']))),
            'cloneRouters' => implode("\n", array_map(fn ($item) => $item, array_values($tableItens['cloneRouters']))),
        ];

        // Definição do template
        $include = __DIR__ . "/Templates/Router/$module.php";
        $this->template = file_exists($include)
            ? include $include
            : include __DIR__ . "/Templates/Router/Default.php";
        $this->template = str_replace(['//{extras_up}', '//{extras_down}'], ['', ''], $this->template);


        SaveFile::save(
            self::class,
            $module,
            $item->model,
            new Template($this->template, $update),
            new Template($this->templateModule, $update)
        );

        // (new Template($this->template, $update))
        //     ->renderTo($pathToSave, Helper::IF_NOT_UPDATE);

        return 2;
    }
}
