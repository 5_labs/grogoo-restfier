<?php

namespace GrogooRestfier\Builder\Creators;

use GrogooRestfier\Builder\Data;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use GrogooRestfier\Helpers\TopologicalSort;

class Tests
{

    private $template = '';

    private function getCanCreate()
    {
        return "
        test('can create a {model_camelcase}', function ()  {
            \$data = ['company_id' => \$this->company->id];
            \$modelData = ModelTest::factory()->make(\$data)->toArray();        
            \$response = \$this->withHeaders(['Authorization' => \"Bearer {\$this->token}\"])->postJson('/api/{route}', \$modelData);

            \GrogooRestfier\Helpers\TestHelper::checkJsonError(\$response, \$this->getName(), \$modelData);

            \$response->assertStatus(201);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });";
    }

    private function getCanUpdate()
    {
        return "
        test('can update a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();

            if (null !== \$model->is_root) {
                \$model->forceFill(['is_root' => false])->save();
                \$model->refresh();
            }

            if (null !== \$model->company_id) {
                \$model->forceFill(['company_id' => \$this->company->id])->save();
                \$model->refresh();
            }

            \$updatedData = ModelTest::factory()->make(['company_id' => \$this->company->id])->toArray();
            \$updatedData['is_root'] = false;

            \$response = \$this->withHeaders(['Authorization' => \"Bearer {\$this->token}\"])->putJson('/api/{route}/' . \$model->id, \$updatedData);

            \GrogooRestfier\Helpers\TestHelper::checkJsonError(\$response, \$this->getName(), \$updatedData);

            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });
        ";
    }

    private function getCanDelete()
    {
        return "
        test('can delete a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();
            
            if (null !== \$model->is_root) {
                \$model->forceFill(['is_root' => false])->save();
                \$model->refresh();
            }

            \$response = \$this->withHeaders(['Authorization' => \"Bearer {\$this->token}\"])->deleteJson('/api/{route}/' . \$model->id);

            // \GrogooRestfier\Helpers\TestHelper::checkJsonError(\$response, \$this->getName(), ['id' => \$model->id]);
            
            \$response->assertStatus(204);
        });
        ";
    }

    private function getCanList()
    {
        return "
        test('can list {model_camelcase}', function ()  {
            ModelTest::factory()->count(1)->create();
            \$response = \$this->withHeaders(['Authorization' => \"Bearer {\$this->token}\"])->getJson('/api/{route}');

            \GrogooRestfier\Helpers\TestHelper::checkJsonError(\$response, \$this->getName(), []);
            
            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => ['*' => [{fields}]]
            ]);
        });
        ";
    }

    private function getCanRead()
    {
        return "
        test('can read a {model_camelcase}', function ()  {
            \$model = ModelTest::factory()->create();

            if (null !== \$model->is_root) {
                \$model->forceFill(['is_root' => false])->save();
                \$model->refresh();
            }

            \$response = \$this->withHeaders(['Authorization' => \"Bearer {\$this->token}\"])->getJson('/api/{route}/' . \$model->id);

            \GrogooRestfier\Helpers\TestHelper::checkJsonError(\$response, \$this->getName(), ['id' => \$model->id]);
            
            \$response->assertStatus(200);
            \$response->assertJsonStructure([
                'data' => [{fields}]
            ]);
        });
        ";
    }


    public function handle(Data $data, $ignore = []): int
    {
        // $pathToSave = Helper::getPathApp() . '/app/Modules/' . $data->getData()->module . '/Tests/Feature/Generated';
        $pathToSave = Helper::getPathApp() . '/tests/Feature/Modules/' . $data->getData()->module;
        $this->template = file_get_contents(__DIR__ . "/TestDefault/Default.txt");
        $fieldsToIgnore = ['created_at', 'updated_at', 'deleted_at', 'company_id', 'is_root', 'views'];
        $ignoreFillable = $data->getData()->ignoreFieldsToFillable ?? null;


        foreach ($data->getData()->items as $item) {

            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            // Tests default
            $default = __DIR__ . "/TestDefault/$item->model.txt";
            if (file_exists($default)) {
                (new Template($default, []))
                    ->renderTo($pathToSave . '/' . $item->model . 'Test.php', Helper::IF_NOT_UPDATE);
                continue;
            }

            $top = [];
            $tables = [];
            foreach (
                array_filter(
                    (array) $item->fields,
                    fn($item) => isset($item->field_origin) && $item->model !== 'Company'
                ) as $filtered
            ) {
                $top[$item->table][] = $filtered->table;
                $tables[] = $filtered->table;
            }

            // Caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table, /*includeNullable=*/ true);
            if (count($routers['router']) > 0) {
                foreach ($routers['router'] as $router) {
                    $relations = explode('.', $router);
                    array_pop($relations);
                    list($table, $cpoID, $cpoRel) = explode('|', end($relations));
                    $lastTable = $item->table;
                    foreach ($relations as $relationItem) {
                        if (stripos($relationItem, '|') !== false) {
                            list($table, $cpoID, $cpoRel) = explode('|', $relationItem);
                            $top[$lastTable][] = $table;
                            $lastTable = $table;
                            $tables[] = $lastTable;
                            $tables[] = $table;
                        }
                    }
                }
            }

            // montar a tree das tabelas relacionadas
            $ignoreRelations = ['companies', 'profiles', 'users', 'user_profile', 'migrations', 'files'];
            $tables = array_diff(array_unique($tables), $ignoreRelations);
            $relations = $data->getData()->relations;
            $tree = [];
            foreach ($relations as $ref => $rels) {
                if (array_search($ref, $tables) !== false) {
                    $tree[$ref] = array_map(
                        fn($item) => $item->referenced_table_name,
                        array_filter(
                            (array)$rels,
                            // ignorar tabelas já criadas no test
                            fn($item) => array_search($item->referenced_table_name, $ignoreRelations) === false
                        )
                    );
                }
            }

            $relationsCreator = [
                'router' => array_map(
                    fn($table) =>
                    HelperCreator::getNameSpace($data, $table, '\\Models\\')
                        . ucwords(Helper::name2CamelCase(Helper::singularize($table)))
                        . '::factory()->create();',
                    array_filter(TopologicalSort::sort($tree), fn($t) => $t !== $item->table)
                )
            ];

            // fields to ignore
            $fieldsToIgnoreThis = array_merge($fieldsToIgnore, $ignoreFillable->{$item->table} ?? []);
            $fields = array_map(
                fn($item) => "'" . ($item->column_name ?? $item->field_origin) . "'",
                array_filter(
                    (array) $item->fields,
                    fn($item) =>
                    isset($item->column_name)
                        && array_search(($item->column_name ?? $item->field_origin), $fieldsToIgnoreThis) === false
                )
            );
            $router = str_replace('_', '-', Helper::reverteName2CamelCase($item->model));
            $update = [
                'relationsCreator' => implode('', $relationsCreator['router']),
                'module' => $data->getData()->module,
                'table' => $item->table,
                'model' => $item->model,
                'model_camelcase' => trim(lcfirst($item->model)),
                'fields' => implode(',', $fields),
                'route' => $router
            ];

            $this->template = str_replace('{tests}', implode("\n", [
                $this->getCanCreate(),
                $this->getCanRead(),
                $this->getCanList(),
                $this->getCanUpdate(),
                $this->getCanDelete()
            ]), $this->template);

            // check if exists manual test for this
            // tests/Feature/Manual/VersionTest.php
            if (file_exists(Helper::getPathApp() . '/tests/Feature/Manual/' . $item->model . 'Test.php')) {
                continue;
            }

            (new Template($this->template, $update))
                ->renderTo($pathToSave . '/' . $item->model . 'Test.php', Helper::IF_NOT_UPDATE);
        }

        return is_dir($pathToSave) ? count(scandir($pathToSave)) - 2 : 0;
    }
}
