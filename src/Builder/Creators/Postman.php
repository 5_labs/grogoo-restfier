<?php


namespace GrogooRestfier\Builder\Creators;

use Illuminate\Support\Facades\Artisan;
use GrogooRestfier\Builder\Data;
use GrogooRestfier\Builder\LoadDB;
use GrogooRestfier\Helpers\ConfigPersisted;
use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Template;
use PhpParser\Node\Stmt\Break_;

class Postman implements CreateInterface
{

    private $ignoreRouters;
    private $template = "";

    private $templateModule = "";

    private function getExcept($table)
    {
        $out = '';
        if (isset($this->ignoreRouters[$table])) {
            if ($this->ignoreRouters[$table] === true) {
                $out = "->except(['index', 'show', 'store', 'update', 'destroy'])";
            } else {
                $items = implode(',', array_map(fn ($i) => "'$i'", $this->ignoreRouters[$table]));
                $out = "->except([$items])";
            }
        }
        return $out;
    }

    public function handle(Data $data, array $ignore = [], $ignoreRouters = []): int
    {
        $this->ignoreRouters = $ignoreRouters;

        $module = $data->getData()->module;

        $tableItens = ['routers' => []];

        $postman = [];
        foreach ($data->getData()->items as $item) {
            $doc = [
                'name' => $item->model,
                'item' => []
            ];

            if (array_search($item->table, $ignore) !== false) {
                continue;
            }

            // ROUTER: caminho unico para chegar até company
            $routers = HelperCreator::getRoutes($data, $item->table);

            if (count($routers['router']) > 0) {
                foreach ($routers['router'] as $router) {
                    $parts = array_reverse(explode('.', $router));
                    array_shift($parts);
                    $routerItem = [];
                    foreach ($parts as $part) {
                        $route = explode('|', $part)[0];
                        $routerItem[] = Helper::name2KebabCase(Helper::singularize($route))
                            . '/:' . Helper::singularize($route) . '_id';
                    }
                    $routerItem[] = Helper::name2KebabCase(Helper::singularize($item->table));


                    $from  = Helper::name2KebabCase(Helper::singularize($item->table))
                        . ' from '
                        . str_replace('_', '-', Helper::singularize(explode('|', $parts[0])[0]));

                    $tableItens['routers'][$from] = '/' . implode('/', $routerItem);
                }
            } else {
                $tableItens['routers'][Helper::name2KebabCase(Helper::singularize($item->table))] = '/' . Helper::name2KebabCase(Helper::singularize($item->table));
            }
        }

        // Criacao da documentaçaõ do postman
        $config = (new ConfigPersisted('postman'))->get();
        $documentation = [
            'name' => $module,
            'item' => []
        ];
        $restful = ['list' => ['GET', ''], 'read' => ['GET', '/:id'], 'create' => ['POST', ''], 'update' => ['PUT', '/:id'], 'delete' => ['DELETE', '/:id']];
        foreach ($tableItens['routers'] as $model => $rota) {
            $item = ['name' => ucwords(str_replace('-', ' ', $model)), 'item' => []];
            foreach ($restful as $rest => $param) {
                $path = explode('/', $rota);
                if ($param[1] !== '') {
                    $path[] = ":{$router}_id";
                }
                $router = explode(' ', $model)[0];
                array_shift($path);
                $name = ucwords(str_replace('-', ' ', $model) . ' - ' . $rest);
                if (isset($config[$router]['company_id'])) {
                    unset($config[$router]['company_id']);
                }
                $tgb = [
                    "name" => "$name",
                    "protocolProfileBehavior" => [
                        "disableBodyPruning" => true
                    ],
                    "request" => [
                        "method" => $param[0],
                        "header" => [
                            [
                                "key" => "Content-Type",
                                "value" => "application/json"
                            ]
                        ],
                        "body" => [
                            "mode" => "raw",
                            "raw" => str_replace([',"', '{', '}'], [",\n\"", "{\n", "\n}"], json_encode(isset($config[$router]) ? $config[$router] : []))
                        ],
                        "url" => [
                            "raw" => "{{base_url}}" . $rota . ($param[1] !== '' ? "/:{$router}_id" : ''),
                            "host" => [
                                "{{base_url}}"
                            ],
                            "path" => $path
                        ]
                    ],
                    "response" => []
                ];
                if (array_search($rest, ['list', 'read', 'delete']) !== false) {
                    unset($tgb['request']['body']);
                }
                $item['item'][] = $tgb;
            }
            $documentation['item'][] = $item;
        }

        $postman = new ConfigPersisted('postman');
        $data = $postman->get();
        $data['collection']['item'] = array_merge($data['collection']['item'], [$documentation]);
        $postman->init($data);

        return 2;
    }

    public static function generateHTML(string $collectionJsonFile): string
    {

        if (!file_exists($collectionJsonFile)) {
            return 'COLLECTION_NOT_FOUND';
        }
        $json = file_get_contents($collectionJsonFile);
        $collection = json_decode($json, true);
        $tree = self::buildTree($collection['item']);
        $sidebar = self::generateSidebar($tree);
        $mainContent = '<div class="accordion" id="accordionFunctions">' . self::generateMainContent($tree) . '</div>';
        $html = self::generateHtmlContent($sidebar, $mainContent);

        // copiar os arquivos completos da documentacao para pasta correta
        $pathTemplate =  __DIR__ . '/Postman/doc-template';
        $savepath = Helper::getPathApp() . '/storage/api-documentation';
        $saveto = $savepath . '/index.html';
        Helper::deleteDirectory($savepath);
        shell_exec("cp -r $pathTemplate $savepath");
        Helper::saveFile($saveto, $html, 'SOBREPOR');

        return file_exists($saveto) ? 'SUCCESS' : 'FAIL_TO_SAVE';
    }

    private static function buildTree(array $items)
    {
        $tree = [];
        foreach ($items as $item) {
            if (isset($item['item'])) {
                $item['children'] = self::buildTree($item['item']);
                unset($item['item']);
            }
            $tree[] = $item;
        }
        return $tree;
    }

    private static function generateSidebar(array $tree, $level = 0)
    {
        $output = '';
        foreach ($tree as $node) {
            $idUnique = '_' . md5(urlencode($node['name']));
            if (isset($node['children'])) {
                $output .= '<li class="nav-item dropdown">';
                $output .= '<a class="nav-link dropdown-toggle" 
                href="#" 
                role="button" 
                data-bs-toggle="dropdown" 
                aria-haspopup="true" 
                aria-expanded="false">' . $node['name'] . '</a>';
                $output .= '<div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">';
            } else {
                $output .= '<li class="nav-item">';
                $output .= '<a class="nav-link" href="#div_' . $idUnique . '">' . $node['name'] . '</a>';
            }

            if (isset($node['children'])) {
                foreach ($node['children'] as $child) {
                    $idUnique = '_' . md5(urlencode($child['name']));
                    $output .= '<a class="dropdown-item" href="#div_' . $idUnique . '" href="#div_' . $idUnique . '">' . $child['name'] . '</a>';
                }
                $output .= '</div>';
            }

            $output .= '</li>';
        }

        return $output;
    }


    private static function generateMainContent(array $tree, int $level = 2)
    {
        $output = '';

        foreach ($tree as $node) {
            $idUnique = '_' . md5(urlencode($node['name']));
            if ($level === 2) {
                $output .= '<div id="collapse_' . $idUnique . '" class="border-item">';
            }
            $output .= '<div id="div_' . $idUnique . '" class="section p-' . $level . ' pb-0 pr-1">';
            $output .= "<h$level><span>$node[name]</span>" . self::getBadgeVerb($node) . "</h$level>";

            if (isset($node['request'])) {
                $curlSnippet = self::createCurlCommand($node);
                $nodeSnippet = self::createAxiosCode($node);
                $phpSnippet = self::createPHPCode($node);

                $output .= '<ul class="nav nav-tabs" id="snippetTabs_' . $idUnique . '" role="tablist">';

                $output .= '<li class="nav-item" role="presentation">';
                $output .= '<button class="nav-link active" id="php-tab_' . $idUnique . '" data-bs-toggle="tab" data-bs-target="#php_' . $idUnique . '" type="button" role="tab" aria-controls="php" aria-selected="false">PHP</button>';
                $output .= '</li>';

                $output .= '<li class="nav-item" role="presentation">';
                $output .= '<button class="nav-link" id="curl-tab_' . $idUnique . '" data-bs-toggle="tab" data-bs-target="#curl_' . $idUnique . '" type="button" role="tab" aria-controls="curl" aria-selected="true">cURL</button>';
                $output .= '</li>';

                $output .= '<li class="nav-item" role="presentation">';
                $output .= '<button class="nav-link" id="node-tab_' . $idUnique . '" data-bs-toggle="tab" data-bs-target="#node_' . $idUnique . '" type="button" role="tab" aria-controls="node" aria-selected="false">Node.js</button>';
                $output .= '</li>';


                $output .= '</ul>';

                $output .= '<div class="tab-content" id="snippetTabsContent_' . $idUnique . '">';

                $output .= '<div class="tab-pane fade p-2 show active " id="php_' . $idUnique . '" role="tabpanel" aria-labelledby="php-tab">';
                $output .= "<pre><code class=\"php\">$phpSnippet</code></pre>";
                $output .= '</div>';

                $output .= '<div class="tab-pane fade p-2" id="curl_' . $idUnique . '" role="tabpanel" aria-labelledby="curl-tab">';
                $output .= "<pre><code class=\"curl\">$curlSnippet</code></pre>";
                $output .= '</div>';

                $output .= '<div class="tab-pane fade p-2" id="node_' . $idUnique . '" role="tabpanel" aria-labelledby="node-tab">';
                $output .= "<pre><code class=\"node\">$nodeSnippet</code></pre>";
                $output .= '</div>';

                $output .= '</div>';
            }

            if (isset($node['children'])) {
                $output .= self::generateMainContent($node['children'], ($level + 1));
            }

            $output .= '</div>';


            if ($level === 2) {

                // //fecha accordion-item
                // $output .= '</div>';

                // //fecha accordion-body
                // $output .= '</div>';

                //fecha section
                $output .= '</div>';
            }
        }

        return $output;
    }


    private static function generateHtmlContent($sidebar, $mainContent): string
    {
        $filecontent = __DIR__ . '/Postman/doc-template/index.html';
        return (new Template($filecontent, [
            'sidebar' => $sidebar,
            'mainContent' => $mainContent,
            'title' => mb_strtoupper(getenv('APP_NAME'))
        ]))->render();
    }



    private static function createCurlCommand($request)
    {
        $requestMethod = $request["request"]["method"];
        $requestHeaders = $request["request"]["header"];
        $requestBody = $request["request"]["body"]["raw"] ?? '';
        $requestParams = $request["request"]["url"]["query"] ?? [];
        $requestUrl = $request["request"]["url"]["raw"] ?? '';

        $curlCommand = "<span class='function'>curl</span> -X <span class='string'>{$requestMethod}</span> '<span class='string'>{$requestUrl}</span>' \\\n";
        foreach ($requestHeaders as $header) {
            $curlCommand .= "     -H '<span class='string'>{$header["key"]}: {$header["value"]}</span>' \\\n";
        }
        if (!empty($requestBody)) {
            $curlCommand .= "     -d '<span class='string'>{$requestBody}</span>' \\\n";
        }
        foreach ($requestParams as $param) {
            $curlCommand .= "     --data-urlencode '<span class='string'>{$param["key"]}'='{$param["value"]}</span>' \\\n";
        }

        return $curlCommand;
    }


    private static function createAxiosCode($request)
    {
        $requestMethod = $request["request"]["method"];
        $requestHeaders = $request["request"]["header"];
        $requestBody = $request["request"]["body"]["raw"] ?? '';
        $requestParams = $request["request"]["url"]["query"] ?? [];
        $requestUrl = $request["request"]["url"]["raw"] ?? '';

        $axiosCode = "<span class='keyword'>const</span> <span class='variable'>axios</span> = <span class='function'>require</span>('<span class='string'>axios</span>');\n";
        $axiosCode .= "<span class='keyword'>const</span> <span class='variable'>url</span> = '<span class='string'>{$requestUrl}</span>';\n";
        $axiosCode .= "<span class='keyword'>const</span> <span class='variable'>options</span> = {\n";
        $axiosCode .= "  <span class='property'>method</span>: '<span class='string'>{$requestMethod}</span>',\n";
        $axiosCode .= "  <span class='property'>headers</span>: {\n";
        foreach ($requestHeaders as $header) {
            $axiosCode .= "    '<span class='string'>{$header["key"]}</span>': '<span class='string'>{$header["value"]}</span>',\n";
        }
        $axiosCode .= "  },\n";
        if (!empty($requestBody)) {
            $axiosCode .= "  <span class='property'>data</span>: <span class='string'>{$requestBody}</span>,\n";
        }
        if (!empty($requestParams)) {
            $axiosCode .= "  <span class='property'>params</span>: {\n";
            foreach ($requestParams as $param) {
                $axiosCode .= "    '<span class='string'>{$param["key"]}</span>': '<span class='string'>{$param["value"]}</span>',\n";
            }
            $axiosCode .= "  },\n";
        }
        $axiosCode .= "}\n";
        $axiosCode .= "<span class='variable'>axios</span>(<span class='variable'>url</span>, <span class='variable'>options</span>)\n";
        $axiosCode .= "  .<span class='function'>then</span>(<span class='variable'>response</span> => {\n";
        $axiosCode .= "    <span class='function'>console.log</span>(<span class='variable'>response</span>);\n";
        $axiosCode .= "  })\n";
        $axiosCode .= "  .<span class='function'>catch</span>(<span class='variable'>error</span> => {\n";
        $axiosCode .= "    <span class='function'>console.error</span>(<span class='variable'>error</span>);\n";
        $axiosCode .= "  });\n";

        return $axiosCode;
    }


    private static function createPHPCode($request)
    {
        $requestMethod = $request["request"]["method"];
        $requestHeaders = $request["request"]["header"];
        $requestBody = $request["request"]["body"]["raw"] ?? '';
        $requestParams = $request["request"]["url"]["query"] ?? [];
        $requestUrl = $request["request"]["url"]["raw"] ?? '';

        $phpCode = "<span class='variable'>\$url</span> = '<span class='string'>{$requestUrl}</span>';\n";
        $phpCode .= "<span class='variable'>\$ch</span> = <span class='function'>curl_init</span>(<span class='variable'>\$url</span>);\n";
        $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_RETURNTRANSFER</span>, <span class='keyword'>true</span>);\n";
        $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_HEADER</span>, <span class='keyword'>false</span>);\n";
        foreach ($requestHeaders as $header) {
            $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_HTTPHEADER</span>, <span class='keyword'>array</span>('<span class='string'>{\$header[\"key\"]}: {\$header[\"value\"]}</span>'));\n";
        }
        if (!empty($requestBody)) {
            $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_POST</span>, <span class='keyword'>true</span>);\n";
            $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_POSTFIELDS</span>, <span class='string'>{$requestBody}</span>);\n";
        }
        if (!empty($requestParams)) {
            $phpCode .= "<span class='function'>curl_setopt</span>(<span class='variable'>\$ch</span>, <span class='constant'>CURLOPT_POSTFIELDS</span>, <span class='function'>http_build_query</span>(";
            $phpCode .= "<span class='keyword'>[</span>" . implode(", ", array_map(function ($param) {
                return "'<span class='string'>{\$param[\"key\"]}</span>' => '<span class='string'>{\$param[\"value\"]}</span>'";
            }, $requestParams)) . "<span class='keyword'>]</span>";
            $phpCode .= "));\n";
        }
        $phpCode .= "<span class='variable'>\$response</span> = <span class='function'>curl_exec</span>(<span class='variable'>\$ch</span>);\n";
        $phpCode .= "<span class='function'>curl_close</span>(<span class='variable'>\$ch</span>);\n";
        $phpCode .= "<span class='function'>print_r</span>(<span class='variable'>\$response</span>);\n";

        return $phpCode;
    }

    private static function getBadgeVerb($node)
    {
        $verb = str_replace(['"', "'"], '', mb_strtolower($node['request']['method'] ?? ''));
        if (strlen($verb) === 0) {
            return '';
        }
        $color = 'success';
        switch ($verb) {
            case 'post':
                $color = 'primary';
                break;
            case 'put':
                $color = 'warning';
                break;
            case 'delete':
                $color = 'danger';
                break;
        }

        return '  <span class="badge bg-' . $color . ' p-1" style="font-weight:100;font-size:14px;">' . $verb . '</span>';
    }
}
