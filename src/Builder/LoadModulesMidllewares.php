<?php

namespace GrogooRestfier\Builder;

use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Modules\Auth\Http\Middlewares\CheckPermission;
use GrogooRestfier\Modules\Auth\Http\Middlewares\CheckToken;

/**
 * LoadModulesMiddlewares is a class responsible for loading module-specific middleware
 * from the 'app/Modules' directory.
 */
class LoadModulesMidllewares
{

    /**
     * Load modules middlewares.
     *
     * This method scans the 'app/Modules' directory and loads middleware
     * classes from the 'Http/Middlewares' subdirectory in each module.
     *
     * @return array<string, class-string|string> An associative array where keys are the converted middleware names and values are their corresponding class names
     */
    public static function load(): array
    {
        $out = [
            'Auth.check.token' => CheckToken::class,
            'Auth.check.permission' => CheckPermission::class
        ];

        $modulesPath = Helper::getPathApp() . '/app/Modules';
        $modules = glob($modulesPath . '/*', GLOB_ONLYDIR);

        foreach ($modules as $module) {
            $middlewarePath = $module . '/Http/Middlewares';
            if (is_dir($middlewarePath)) {
                $files = glob($middlewarePath . '/*.php');
                foreach ($files as $file) {
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $name = basename($module)
                        . '.'
                        . self::convertNameToPoint($filename);
                    $class = 'App\\Modules\\' . basename($module) . '\\Http\\Middlewares\\' . $filename;
                    $out[$name] = $class;
                }
            }
        }

        return $out;
    }

    /**
     * Convert the camel-cased middleware name to a dot-separated name.
     *
     * This method takes a camel-cased string, such as 'CheckBelongTo', and
     * converts it to a dot-separated string, like 'check.belong.to'.
     *
     * @param string $name The camel-cased middleware name
     * @return string The dot-separated middleware name
     */
    private static function convertNameToPoint(string $name): string
    {

        // Encontre letras maiúsculas e coloque um ponto antes delas
        $output = preg_replace_callback('/[A-Z]/', function ($matches) {
            return '.' . strtolower($matches[0]);
        }, $name);

        // Remova o primeiro ponto (caso haja) e retorne a string convertida
        return ltrim($output, '.');
    }
}
