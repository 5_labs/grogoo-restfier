<?php

namespace GrogooRestfier\Builder;

use Exception;
use GrogooRestfier\Helpers\Helper;
use Illuminate\Support\Facades\Artisan;


class CreateApikeysModule
{
    public static function handle()
    {
        $directory = app()->basePath() . '/database/migrations/';
        $pattern = '_create_apikeys_table.php';
        $files = glob($directory . '*' . $pattern);
        if (!empty($files)) {
            echo "\n";
            foreach ($files as $file) {
                echo "$file\n";
            }
            throw new Exception("ERROR: Foi encontrado um arquivo similar ao padrão. Abortado");
        }

        $date = date('Y_m_d_His');
        $filename = "{$directory}/{$date}{$pattern}";

        $content = file_get_contents(__DIR__ . '/templates/CreateApikeysTableTemplate.php');

        Helper::saveFile($filename, $content);

        Artisan::call('migrate');

        Artisan::call('cache:clear');

        Artisan::call('app:build');
    }
}
