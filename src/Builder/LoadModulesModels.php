<?php

namespace GrogooRestfier\Builder;

use GrogooRestfier\Helpers\Helper;

/**
 * LoadModulesProviders is a class responsible for loading module-models
 * from the 'app/Modules' directory.
 */
class LoadModulesModels
{
    /**
     * Load modules providers.
     *
     * This method scans the 'app/Modules' directory and loads provider
     * classes from the 'Providers' subdirectory in each module.
     *
     * @return array<string, class-string> An indexed array of fully-qualified provider class names
     */
    public static function load(): array
    {
        $out = [];


        // Modules path
        $modulesPath = Helper::getPathApp() . '/app/Modules';
        $modules = glob($modulesPath . '/*', GLOB_ONLYDIR);
        foreach ($modules as $module) {
            $modelsPath = $module . '/Models';
            if (is_dir($modelsPath)) {
                $files = glob($modelsPath . '/*.php');
                foreach ($files as $file) {
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $moduleName = basename($module);
                    if (file_exists(Helper::getPathApp() . "/app/Generated/Modules/$moduleName/Models/Abstract$filename.php")) {
                        $out[] = "App\\Modules\\$moduleName\\Models\\$filename";
                    }
                }
            }
        }

        return $out;
    }
}
