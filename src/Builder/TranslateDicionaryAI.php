<?php

namespace GrogooRestfier\Builder;

use Exception;
use GrogooRestfier\Helpers\Helper;
use OpenAI;

class TranslateDicionaryAI
{

    public static function handle($openAIApiKey, $pathOrigin, $langToTranslate = 'pt_BR')
    {
        if (!file_exists($pathOrigin)) {
            throw new Exception('Origin file not found: ' . $pathOrigin);
        }
        echo "Translate dicionary to $langToTranslate with AI ... wait please";

        $content = file_get_contents($pathOrigin);
        $tamanhoDoPedaço = 3000; // Especifica o tamanho de cada pedaço
        $saveTo = Helper::getPathApp() . "/lang/$langToTranslate.json";
        if (!file_exists($saveTo)) {
            Helper::saveFile($saveTo, '{}');
        }

        $prompt = [];
        $prompt[] = ['role' => 'user', 'content' => 'i will send a big json file on parts. wait for message TRANSLATE PLEASE'];
        foreach (str_split($content, $tamanhoDoPedaço) as $item) {
            $prompt[] = ['role' => 'user', 'content' => $item];
        }
        $prompt[] = ['role' => 'user', 'content' => "TRANSLATE PLEASE to  $langToTranslate. Send a valid json, without coments"];

        // GPT 
        $client = OpenAI::client($openAIApiKey);
        $response =  $client->chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' => $prompt
        ]);

        $ptBR = '';
        foreach ($response->choices as $result) {
            if ($result->message->role === 'assistant') {
                $ptBR .=  $result->message->content;
            }
        }

        $dataConfig = array_merge(
            json_decode($ptBR, true) ?? [],
            json_decode(file_get_contents($saveTo), true)
        );

        ksort($dataConfig);

        Helper::saveFile($saveTo, $ptBR, Helper::OVERWRITE);

        echo ". Done!\n";
    }
}
