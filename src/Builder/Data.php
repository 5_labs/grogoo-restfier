<?php

namespace GrogooRestfier\Builder;

use GrogooRestfier\Builder\LoadDB;

class Data
{

    private $data = [];
    private $config;

    /**
     *
     * @param string $tokenCrypto
     * @param string $appName
     * @param string $htmlTitle
     * @param string $adminName
     * @param string $adminEmail
     * @param array $ignoreEntities
     */
    public function __construct(
        $tokenCrypto = '',
        $appName = '',
        $htmlTitle = '',
        $adminName = '',
        $adminEmail = '',
        $ignoreEntities = []
    ) {
        $this->data = LoadDB::handle(
            $tokenCrypto,
            $appName,
            $htmlTitle,
            $adminName,
            $adminEmail,
            $ignoreEntities
        );
    }

    public function getData()
    {
        return json_decode(json_encode($this->data));
    }

    public function set($key, $value)
    {
        $this->data = array_merge($this->data, [$key => $value]);
        return $this;
    }
}
