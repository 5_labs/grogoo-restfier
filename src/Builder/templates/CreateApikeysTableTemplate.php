<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apikeys', function (Blueprint $table) {
            $table->id(); // id BIGINT
            $table->timestamps();
            $table->softDeletes();
            $table->date('valid_at');
            $table->string('title', 200)->nullable();
            $table->string('hash', 64);
            $table->timestamp('last_usage_date')->nullable();
            $table->string('last_usage_ip', 64)->nullable();
            $table->bigInteger('usage_hits')->default(0);
            $table->foreignId('company_id')->constrained('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');

            // indices 
            $table->index(['hash', 'valid_at'], 'apikeys_hash_valid_at_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apikeys');
    }
};
