<?php

namespace GrogooRestfier\Builder;

use GrogooRestfier\Helpers\Helper;
use GrogooRestfier\Helpers\Str;
use GrogooRestfier\Helpers\UpdateEnvFile;

class Codebox
{


    private static $dockerComposeTemplate = 'version: "3.7"

services:

    app_api:
        container_name: ${COMPOSE_PROJECT_NAME}_api
        image: nextstage/php:8.2-fpm-apache-dev
        restart: unless-stopped
        volumes:
            - ./:/var/www/html
        depends_on:
            - app_redis
            - app_db
        env_file:
            - .env
        environment:
            - APACHE_DOCUMENT_ROOT=/var/www/html/public
            - AUTORUN_LARAVEL_STORAGE_LINK="false"
            - PERSISTPATH=${DEVELOP_PERSISTPATH}
        ports:
            - "${DEVELOP_APP_PORT_EXPOSED}:80"
        healthcheck:
            test: ["CMD", "curl", "-f", "http://localhost/api/healthcheck"]
            interval: 15s
            timeout: 5s
            retries: 10

    
    app_testing:
        container_name: ${COMPOSE_PROJECT_NAME}_testing
        image: nextstage/php:8.2-fpm-apache-dev
        restart: unless-stopped
        volumes:
            - ./:/var/www/html
        depends_on:
            - app_redis
            - app_db
        env_file:
            - .env.testing
        environment:
            - APACHE_DOCUMENT_ROOT=/var/www/html/public
            - AUTORUN_LARAVEL_STORAGE_LINK="false"
            - PERSISTPATH=${DEVELOP_PERSISTPATH}


    app_db:
        container_name: ${COMPOSE_PROJECT_NAME}_db
        build:
            context: ./docker/postgres
            args:
                - PGVERSION=${DEVELOP_PGVERSION}
        volumes:
            - ${DEVELOP_PERSISTPATH}/${COMPOSE_PROJECT_NAME}/pg/${DEVELOP_PGVERSION}/data:/var/lib/postgresql/data
        ports:
            - ${DEVELOP_PG_EXPOSED_PORT}:5432
        environment:
            - PGVERSION=${DEVELOP_PGVERSION}
            - POSTGRES_USER=${DB_USERNAME}
            - POSTGRES_PASSWORD=${DB_PASSWORD}
            - POSTGRES_HOST_AUTH_METHOD=md5
            - PG_MULTIPLE_DATABASES=${DEVELOP_PG_MULTIPLE_DATABASES}
        healthcheck:
            test: ["CMD-SHELL", "pg_isready -U postgres"]
            interval: 15s
            timeout: 5s
            retries: 10

    app_redis:
        container_name: ${COMPOSE_PROJECT_NAME}_redis
        image: redis:latest
    
    ';


    private static function checkAppEnv()
    {
        $env = Helper::getEnv('APP_ENV');
        if (null !== $env && 'local' !== $env && 'testing' !== $env) {
            echo "\n\033[31m>> Your env 'APP_ENV' is not 'local or testing'. \"[$env]\" Aborted.\033[0m\n";
            die("Aborted");
        }
    }


    public static function update()
    {

        self::checkAppEnv();

        $pathAPP =  Helper::getPathApp();

        $alwaysDefault = [
            'docker/postgres/conf.d',
            'docker/postgres/extras',
            'docker/postgres/init-scripts',
            'docker/postgres/sql',
            'docker/postgres/Dockerfile',
            'docker/scripts',
            'tests/Feature/Auth'
        ];
        array_map(
            fn ($item) => is_file("$pathAPP/$item")
                ? unlink("$pathAPP/$item")
                : Helper::deleteDirectory("$pathAPP/$item"),
            $alwaysDefault
        );


        echo "\033[34m   >> Existing files will NOT be overwritten, except for default files\033[0m\n";

        Helper::copyDir(__DIR__ . '/../../codebox/', $pathAPP . '/', ['.backup', 'tag.sh']);

        shell_exec("chmod 0777 $pathAPP -R");
    }

    public static function config()
    {

        self::checkAppEnv();

        $pathAPP =  Helper::getPathApp();

        // check if .env is correct configured
        if (Helper::getEnv('DEVELOP_GROGOO_RESTFIER_INSTALLED_REVER') === null) {
            echo "\033[32m   >> Updated initial configurations!\033[0m\n";

            // Update composer.json
            $actualComposer = json_decode(file_get_contents("$pathAPP/composer.json"), true);
            $appName = explode('/', $actualComposer['name'])[1];

            # update envs
            $appNameCleared = str_replace(['-', '_', ' '], '_', $appName);
            $port = rand(8005, 9005);
            $phpVersion = explode('.', PHP_VERSION);
            $phpShortVersion = $phpVersion[0] . '.' . $phpVersion[1];

            $default = [
                'APP_NAME' =>  $appName,
                'APP_ENV' => 'local',
                'APP_KEY' => Str::random(64),
                'APP_DEBUG' => 'true',
                'APP_URL' => 'http://localhost:' . $port,
                'FRONTEND_URL' => 'http://localhost:3000',
                'LOG_CHANNEL' => 'stack',
                'LOG_DEPRECATIONS_CHANNEL' => 'null',
                'LOG_LEVEL' => 'debug',
                'DB_CONNECTION' => 'pgsql',
                'DB_HOST' => $appNameCleared . '_db',
                'DB_PORT' => 5432,
                'DB_DATABASE' => $appNameCleared . '_db',
                'DB_USERNAME' => $appNameCleared,
                'DB_PASSWORD' => rand(100000, 999999),
                'BROADCAST_DRIVER' => 'log',
                'CACHE_DRIVER' => 'file',
                'FILESYSTEM_DISK' => 'local',
                'QUEUE_CONNECTION' => 'sync',
                'SESSION_DRIVER' => 'file',
                'SESSION_LIFETIME' => 120,
                'MEMCACHED_HOST' => '127.0.0.1',
                'APP_LIMIT_PER_PAGE' => 10,
                'REDIS_HOST' => '127.0.0.1',
                'REDIS_PASSWORD' => 'null',
                'REDIS_PORT' => 6379,
                'MAIL_MAILER' => 'smtp',
                'MAIL_HOST' => 'mailpit',
                'MAIL_PORT' => 1025,
                'MAIL_USERNAME' => 'null',
                'MAIL_PASSWORD' => 'null',
                'MAIL_ENCRYPTION' => 'null',
                'MAIL_FROM_ADDRESS' => '',
                'MAIL_FROM_NAME' => '',
                'SENDGRID_API_KEY' => '',
                'AWS_ACCESS_KEY_ID' => '',
                'AWS_SECRET_ACCESS_KEY' => '',
                'AWS_DEFAULT_REGION' => 'us-east-1',
                'AWS_BUCKET' => '',
                'AWS_USE_PATH_STYLE_ENDPOINT' => 'false',
                'PUSHER_APP_ID' => '',
                'PUSHER_APP_KEY' => '',
                'PUSHER_APP_SECRET' => '',
                'PUSHER_HOST' => '',
                'PUSHER_PORT' => 443,
                'PUSHER_SCHEME' => 'https',
                'PUSHER_APP_CLUSTER' => 'mt1',
                'VITE_PUSHER_APP_KEY' => '${PUSHER_APP_KEY}',
                'VITE_PUSHER_HOST' => '${PUSHER_HOST}',
                'VITE_PUSHER_PORT' => '${PUSHER_PORT}',
                'VITE_PUSHER_SCHEME' => '${PUSHER_SCHEME}',
                'VITE_PUSHER_APP_CLUSTER' => '${PUSHER_APP_CLUSTER}',
                'JWT_TTL' => 60,
                'JWT_SECRET' => Str::random(64),
                'JWT_BLACKLIST_GRACE_PERIOD' => 1440,
                'GOOGLE_CLIENT_ID' => 'YOUR_GOOGLE_CLIENT_ID',
                'GOOGLE_CLIENT_SECRET' => 'YOUR_GOOGLE_CLIENT_SECRET',
                'GOOGLE_REDIRECT_URL' => 'http://your-app-url.com/auth/google/callback',
                'DEVELOP_PGVERSION' => 15,
                'DEVELOP_PERSISTPATH' => '/docker-persist',
                'DEVELOP_PHP_VERSION' => $phpShortVersion,
                'DEBUGBAR_ENABLED' => 'true',
                'DEVELOP_APP_PORT_EXPOSED' => $port,
                'COMPOSE_PROJECT_NAME' => $appName,
                'DEVELOP_PG_EXPOSED_PORT' => rand(16000, 19000),
                'DEVELOP_PG_MULTIPLE_DATABASES' => $appNameCleared . '_db',
                'DEVELOP_GROGOO_RESTFIER_INSTALLED' => 'true',
            ];

            // env default da aplicação
            (new UpdateEnvFile('.env'))->update($default);

            // testing 
            (new UpdateEnvFile('.env.testing'))->update(array_merge($default, [
                'DB_CONNECTION' => 'sqlite',
                'DB_DATABASE' => ':memory:',
                'APP_ENV' => 'testing',
                'DB_HOST' => null,
                'DB_PORT' => null,
                'DB_USERNAME' => null,
                'DB_PASSWORD' => null

            ]));

            // docker-compose
            self::updateDockerCompose($appNameCleared);
        } else {
            echo "\033[32m   >> Initial config has be done (env DEVELOP_GROGOO_RESTFIER_INSTALLED exists).\033[0m\n";
        }
    }

    private static function updateDockerCompose($appName)
    {
        $content = self::$dockerComposeTemplate;
        $content = str_replace([
            'app_api',
            'app_testing',
            'app_db',
            'app_queue',
            'app_redis',
        ], [
            $appName . '_api',
            $appName . '_test',
            $appName . '_db',
            $appName . '_queue',
            $appName . '_redis',
        ], $content);
        file_put_contents(Helper::getPathApp() . '/docker-compose.yml', $content);
    }
}
