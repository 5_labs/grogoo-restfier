<?php

namespace GrogooRestfier\Services;

use Illuminate\Support\Facades\Http;
use App\Modules\Company\Models\User;

class SalesflowService
{
    private static function generateIdentifier(string $conversion_type)
    {
        $composer = json_decode(file_get_contents(base_path('composer.json')));
        return $composer->name . " - " . $conversion_type;
    }

    private static function getClientHttp()
    {
        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer " . config('services.salesflow.private_token'),
        ])->baseUrl("https://api.salesflowcrm.com.br")->timeout(5);
    }

    /**
     *@param User $user
     *@param string $origin
     *@param string $conversion_type = register | update | active
     *@return void
     */
    public static function conversion(User $user, string $origin, string $conversion_type, array $custom_columns = [])
    {

        if (config('services.salesflow.public_token') && config('services.salesflow.private_token')) {
            try {
                $client = self::getClientHttp();

                $userSalesflowRequest = self::transformDataToSalesflowApiFormat(
                    $user,
                    $origin,
                    config('services.salesflow.public_token'),
                    self::generateIdentifier($conversion_type),
                    $custom_columns
                );

                if (env('APP_ENV') === "main") {
                    $client->post("/conversion", $userSalesflowRequest);
                }

                return;
            } catch (\Exception $exc) {
                return;
            }
        }
    }

    private static function transformDataToSalesflowApiFormat(
        User $user,
        string $origin,
        string $public_token,
        string $identifier,
        array $custom_columns = []
    ) {
        $SalesflowApiDefaultData = [
            'identifier' => $identifier,
            'token' => $public_token,
            'domain' => $origin,
            'email' => $user->email,
            'name' => $user->name,
            'client' => $user->id
        ];

        return array_merge($SalesflowApiDefaultData, $custom_columns);
    }

}