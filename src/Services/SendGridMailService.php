<?php

namespace GrogooRestfier\Services;

use SendGrid;
use Exception;
use SendGrid\Mail\Mail;
use SendGrid\Mail\Attachment;
use GrogooRestfier\Contracts\MailServiceInterface;


class SendGridMailService implements MailServiceInterface
{

    private Sendgrid $service;
    private Sendgrid\Mail\Mail $mailer;

    public function send(string $toAddress, string $toName, string $subject, string $body, array $config = []): object

    {

        $this->service = new SendGrid(env('SENDGRID_API_KEY'));
        $this->mailer = new Mail();

        if (env('APP_ENV') === 'local') {
            $toAddress = env('MAIL_FROM_ADDRESS');
            $toName = __tr('Mail on Local Development');
        }

        $headers = [
            'from' => ['address' => env('MAIL_FROM_ADDRESS'), 'name' => env('MAIL_FROM_NAME')],
            'to' => ['address' => $toAddress, 'name' => $toName],
            'subject' => env('APP_NAME') . ' - ' . __tr($subject)
        ];
        $this->setEmailHeaders($headers);

        if (!is_null($config['template_id']) && !is_null($config['template_data'])) {
            $this->setTemplate($config['template_id'], $config['template_data']);
        } else {
            $this->setEmailContent(["text/html" => $body]);
        }

        // Somente enviar email caso seja operando
        if (env('DB_DATABASE') !== ':memory:') {
            return $this->sendEmail();
        } else {
            return new SendGrid\Response();
        }
    }


    /**
     * @param array $headers ['to' => ['address'=>'example@mail.com', 'name' => 'user 1 ' ],'from'=> ['address'=>'example@mail.com', 'name' => 'user 1 ' ],'cc'=> ['address'=>'example@mail.com', 'name' => 'user 1 ' ],'subject'=> 'email title','headers?']
     * 
     * @return self
     */
    public function setEmailHeaders(array $headers): self
    {
        if (!array_key_exists('from', $headers) || !array_key_exists('to', $headers)) {
            throw new Exception('Email sem destinátario ou remetente');
        }
        $this->mailer->setFrom($headers['from']['address'], $headers['from']['name']);
        $this->mailer->addTo($headers['to']['address'], $headers['to']['name']);
        if (array_key_exists('subject', $headers)) {
            $this->mailer->setSubject($headers['subject']);
        }

        return $this;
    }

    /**
     * @param array $content ["text/html" => "<h1>Email</h1>"]
     * 
     * @return self
     */
    public function setEmailContent(array $contents): self
    {
        $this->mailer->addContents($contents);
        return $this;
    }

    /**
     * @param string $template_id 
     * @param array $template_data ["name" => "User 1"]
     * 
     * @return self
     */
    public function setTemplate(string $template_id, array $template_data): self
    {
        $this->mailer->setTemplateId($template_id);
        $this->mailer->addDynamicTemplateDatas($template_data);
        return $this;
    }

    /**

     * @param array $file_data ["content" => FILE, "content_type" => 'image/png', 'file_name' => 'relatorio.png', 'disposition' => 'attachment']
     * 
     * @return self
     */
    public function setAttachment(array $file_data): self
    {
        $attachment = new Attachment();
        $attachment->setContent($file_data['content']);
        $attachment->setType($file_data['content_type']);
        $attachment->setFilename($file_data['file_name']);
        $attachment->setDisposition($file_data['disposition']);
        $this->mailer->addAttachment($attachment);
        return $this;
    }

    public function sendEmail(): object
    {

        return $this->service->send($this->mailer);
    }
}
