<?php

namespace GrogooRestfier\Services\Dify;


class TextGeneratorDifyService extends DifyService
{
    protected function execute(array $inputs)
    {
        $response = $this->api->post($this->base_url . '/completion-messages', [
            'inputs' => $inputs,
            'response_mode' => $this->response_mode,
            'user' => 'DifyService'
        ]);

        return $response->json();
    }
}
