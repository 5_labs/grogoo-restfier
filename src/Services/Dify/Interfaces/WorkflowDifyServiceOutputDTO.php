<?php

namespace GrogooRestfier\Services\Dify\Interfaces;

class WorkflowDifyServiceOutputDTO
{
    public string $task_id;
    public string $workflow_run_id;
    public WorkflowDataServiceOutputDTO $data;

    public function __construct(string $task_id, string $workflow_run_id, WorkflowDataServiceOutputDTO $data)
    {
        $this->task_id = $task_id;
        $this->workflow_run_id = $workflow_run_id;
        $this->data = $data;
    }
}
