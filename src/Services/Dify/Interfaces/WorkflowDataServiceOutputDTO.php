<?php

namespace GrogooRestfier\Services\Dify\Interfaces;

class WorkflowDataServiceOutputDTO
{
    public string $id;
    public string $workflow_id;
    public string $status;
    public array $outputs;
    public string|null $error;
    public float $elapsed_time;
    public int $total_tokens;
    public int $total_steps;
    public int $created_at;
    public int $finished_at;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->workflow_id = $data['workflow_id'];
        $this->status = $data['status'];
        $this->outputs = $data['outputs'];
        $this->error = $data['error'];
        $this->elapsed_time = $data['elapsed_time'];
        $this->total_tokens = $data['total_tokens'];
        $this->total_steps = $data['total_steps'];
        $this->created_at = $data['created_at'];
        $this->finished_at = $data['finished_at'];
    }
}
