<?php

namespace GrogooRestfier\Services\Dify;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class DifyService
{

    protected PendingRequest $api;
    protected string $api_key;
    protected string $base_url;
    protected string $response_mode;

    public function __construct(string $api_key, string $response_mode = 'blocking')
    {
        $this->api_key = $api_key;
        $this->base_url = config('services.dify.base_url');
        $this->api = Http::withHeaders([
            'Authorization' => 'Bearer ' . $this->api_key,
        ]);
        $this->response_mode = $response_mode;
    }

    /**
     * Set response_mode of process. Must be 'blocking' or 'streaming'.
     * @param string $response_mode The response mode, either 'blocking' or 'streaming'.
     * @return void
     */
    public function setResponseMode(string $response_mode): void
    {
        if ($response_mode !== 'blocking' && $response_mode !== 'streaming') {
            throw new \Exception('Invalid response mode, must be "blocking" or "streaming"');
        }

        $this->response_mode = $response_mode;
    }

    public function getResponseMode(): string
    {
        return $this->response_mode;
    }
}
