<?php

namespace GrogooRestfier\Services\Dify;

use GrogooRestfier\Services\Dify\Interfaces\WorkflowDataServiceOutputDTO;
use GrogooRestfier\Services\Dify\Interfaces\WorkflowDifyServiceOutputDTO;

class WorkflowDifyService extends DifyService
{
    public function execute(array $inputs): WorkflowDifyServiceOutputDTO
    {
        $response = $this->api->post($this->base_url . '/workflows/run', [
            'inputs' => $inputs,
            'response_mode' => $this->response_mode,
            'user' => 'DifyService'
        ]);

        $response->throw();

        $data = $response->json();

        $workflowData = new WorkflowDifyServiceOutputDTO(
            $data['task_id'],
            $data['workflow_run_id'],
            new WorkflowDataServiceOutputDTO($data['data'])
        );

        return $workflowData;
    }
}
