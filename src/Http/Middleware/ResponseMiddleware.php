<?php

namespace GrogooRestfier\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use GrogooRestfier\Helpers\Helper;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class ResponseMiddleware
{

    public static $customStatusMessages = [
        'en' => [
            200 => 'Operation completed successfully',
            201 => 'New record created successfully',
            204 => 'No content to display',
            400 => 'Invalid request data',
            401 => 'You must sign in to access this resource',
            403 => 'You do not have permission to perform this action',
            404 => 'Requested resource not found',
            405 => 'This action is not supported',
            422 => 'The request contains invalid data',
            500 => 'An unexpected error occurred',
            502 => 'Unable to connect to external service',
            503 => 'Service temporarily unavailable',
        ],
        'pt_BR' => [
            200 => 'Operação concluída com sucesso',
            201 => 'Novo registro criado com sucesso',
            204 => 'Nenhum conteúdo para exibir',
            400 => 'Dados da solicitação inválidos',
            401 => 'Você precisa entrar para acessar este recurso',
            403 => 'Você não tem permissão para realizar esta ação',
            404 => 'Recurso solicitado não encontrado',
            405 => 'Esta ação não é suportada',
            422 => 'Não foi possível processar os dados enviados',
            500 => 'Ocorreu um erro inesperado',
            502 => 'Não foi possível conectar-se ao serviço externo',
            503 => 'Serviço temporariamente indisponível',
        ],
    ];
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if ($request->method() === 'OPTIONS') {
            return $next($request);
        }

        // return $next($request);
        $response = (object) $next($request);
        $response->original = json_decode(json_encode($response->original));
        $code = $response->status();
        $message = null;

        // $response->header('X-My-Custom-Header', 'my-custom-value');
        $error = null;
        if ($response->status() > 300) {
            // Avaliar se é erro de banco e reescrever a mensagem
            if (!$error = $this->getErrorMessage($response->exception, $code, $message)) {
                $error = $response->exception->validator->customMessages
                    ?? $response->original->error
                    ?? $response->error
                    ?? $response->original->message
                    ?? (isset($response->exception) ? $response->exception->getMessage() : null)
                    ??  Response::$statusTexts[$code]
                    ?? null;
            }
            if ($code === $response->status()) {
                $code = null
                    ?? (isset($response->exception->status)  ? $response->exception->status : null)
                    ?? (isset($response->exception)
                        ? (isset($response->exception->status)
                            ? $response->exception->status
                            : ($response->exception->getCode() < 500 && $response->exception->getCode() > 0 ? $response->exception->getCode() : null)
                        )
                        : null)
                    ?? $response->status();
            }
        }
        $code = ($code === 0 ? 500 : $code);

        $content = json_decode($response->getContent(), true);

        if (null !== $content && !isset($content['data'])) {
            $content = ['data' => $content];
        }

        if ($error) {
            $out = ['data' => null, 'meta' => []];
        } else {
            $out =
                $content
                ??  ['meta' => [],  'data' => ($response->original ?? [])];
        }

        $translate = __tr($content['message'] ?? $error ?? $this->getMessageByCode($code));

        $out['meta'] = array_merge(($out['meta'] ?? []), [
            'error' => null !== $error, // $code !== 500 ? (($error) ?? null) : 'Server error',
            'message' => $translate,
            'status' => $code,
        ]);

        if (getenv('APP_ENV') === 'local' || getenv('APP_ENV') === 'testing') {
            $out['develop'] = [
                'message' => $message,
                'response' => $response,
                'json_content' => $content
            ];
        }
        return response()->json(
            $out,
            $code,
            [
                'Content-Type' => 'application/json;charset=UTF-8',
                'Charset' => 'utf-8',
                'Access-Control-Allow-Origin' => '*'
            ],
        );
    }

    private function getErrorMessage(?object $exception, &$code, &$message): ?string
    {

        if (isset($exception->connectionName)) {
            list($codeerror, $ref, $message) = json_decode(json_encode($exception->errorInfo), true);
            $code = 409;
            $ret =  (Helper::$DBERRORS[$codeerror] ?? __tr('Oops! Something went wrong'));
            $messages = [
                'Undefined column' => __tr('An error occurred in the system (ABS1001)'),
                '42703' => __tr('An error occurred in the system (DB42703)'),
                'violates unique constraint' => __tr('A record with these characteristics already exists'),
                'SQLSTATE[23502]' => __tr('Required field not provided'),
                'SQLSTATE[23503]' => __tr('This record cannot be removed because others records depend on it'),
                'foreign' => __tr('This record cannot be removed because others records depend on it'),
                'SQLSTATE[25P02]' => __tr('Error occurred'),
                'Invalid regular expression' => __tr('Incorrectly filled field'),
                '--violates check constraint' => __tr('Invalid value or not accept'),
            ];
            $message = $this->searchMessage($messages, $message);
            return $message;
        } else {
            $messages = [
                'No query results for model' => $this->getMessageByCode(404),
                'method is not supported for route' => $this->getMessageByCode(405),
            ];
            $ret = $this->searchMessage($messages, null !== $exception ? $exception->getMessage() : null);
            if ($ret !== $message) {
                $message = $ret;
                return $ret;
            }
        }
        return null;
    }

    private function searchMessage(array $messages, ?string $message): ?string
    {
        if (null === $message) {
            return $message;
        }
        foreach ($messages as $key => $value) {
            if (stripos($message, $key) !== false) {
                return $value;
            }
        }
        return $message;
    }

    public function getMessageByCode(int $code)
    {
        return self::$customStatusMessages[app()->getLocale()][$code] ?? $code;
    }
}
