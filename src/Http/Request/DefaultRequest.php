<?php

namespace GrogooRestfier\Http\Request;


class DefaultRequest extends \Illuminate\Foundation\Http\FormRequest
{

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $failed = array_map(fn($item) => $item[0], $validator->getMessageBag()->toArray());
        throw new \Illuminate\Http\Exceptions\HttpResponseException(
            response()->json(['error' => $failed], 422)
        );
    }

    public function rulesDefault(array $rules = [])
    {
        // Adicionar sometimes aos itens
        if ($this->method() === 'PUT') {
            $rules = array_map(fn($item) => is_array($item) ? $item : "sometimes|$item", $rules);
        }

        return ($this->method === 'POST' || $this->method === 'PUT')
            ? $rules
            : [];
    }

    public function authorize()
    {
        return false;
    }

    /**
     * Decode JSON string to array before validation
     */
    protected function _prepareForValidation(array $rulesArray): void
    {
        foreach ($rulesArray as $key => $rules) {
            $rule = explode('|', $rules);
            $rule[1] ??= 'string';
            // 1: type
            if ($rule[1] === 'array' && $this->has($key) && is_string($this->input($key))) {
                $this->merge([$key => json_decode($this->input($key), true)]);
            }
        }
    }
}
