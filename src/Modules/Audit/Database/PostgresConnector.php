<?php

namespace GrogooRestfier\Modules\Audit\Database;

use Illuminate\Database\Connectors\PostgresConnector as BaseConnector;
use GrogooRestfier\Modules\Audit\Helpers\Audit;

class PostgresConnector extends BaseConnector
{
    public function connect(array $config)
    {
        $username = 'System';
        $userid = '-1';

        // logger('User autenticado', [\Illuminate\Support\Facades\Auth::user()]);

        $pdo = parent::connect($config);
        $pdo = Audit::setAuditParamsPdo($pdo, $username, $userid);

        return $pdo;
    }
}
