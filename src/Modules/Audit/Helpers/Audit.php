<?php

namespace GrogooRestfier\Modules\Audit\Helpers;

use Illuminate\Support\Facades\DB;

class Audit
{
    public static function setAuditParams(string $username, string $userId)
    {
        if (config('app.audit')) {
            DB::statement("SET audit.username = '$username';");
            DB::statement("SET audit.userid = '$userId'");
        }
    }

    public static function setAuditParamsPdo($pdo, string $username, string $userid)
    {
        if (config('app.audit')) {
            $pdo->query("SET audit.username = '$username'");
            $pdo->query("SET audit.userid = '$userid'");
        }

        return $pdo;
    }
}
