<?php

namespace GrogooRestfier\Modules\Audit\Events;

use App\Modules\Company\Models\User;
use GrogooRestfier\Modules\Audit\Helpers\Audit;

trait AbstractEvent
{
    public ?User $user_dispatcher;
    public function setAuditParam()
    {
        Audit::setAuditParams($this->user_dispatcher?->name ?? 'System', $this->user_dispatcher?->id ?? '-1');
    }
}

