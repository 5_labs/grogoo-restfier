<?php

namespace GrogooRestfier\Modules\Audit\Providers;

use GrogooRestfier\Modules\Audit\Database\PostgresConnector;
use Illuminate\Support\ServiceProvider;

class AuditModuleProvider extends ServiceProvider
{

    public function boot()
    {
        if(config('app.audit')) $this->app->bind('db.connector.pgsql', PostgresConnector::class);
    }
}
