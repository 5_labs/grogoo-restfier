<?php


namespace GrogooRestfier\Modules\Auth\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


// use App\Modules\Auth\Database\Factories\JwtTokenFactory;

class JwtToken extends Model
{
    use HasFactory;

    protected $table = 'jwt_tokens';
    protected $fillable = ['token'];


    // protected static function newFactory(): JwtTokenFactory
    // {
    //     return new JwtTokenFactory();
    // }
}
