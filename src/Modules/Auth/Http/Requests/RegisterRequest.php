<?php

namespace GrogooRestfier\Modules\Auth\Http\Requests;

use GrogooRestfier\Http\Request\DefaultRequest;

class RegisterRequest extends DefaultRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __tr('user.name') . '.' . __tr('The field is required'),
            'name.string' => __tr('user.name') . '.' . __tr('The field e-mail must be a string'),
            'name.max' => __tr('user.name') . '.' . __tr('Length must be less than 255 characters'),

            'email.required' => __tr('user.email') . '.' . __tr('The field is required'),
            'email.string' => __tr('The field e-mail must be a string'),
            'email.email' => __tr('The field e-mail must be a valid email address'),
            'email.unique' => __tr('This email is already registered'),

        ];
    }
}
