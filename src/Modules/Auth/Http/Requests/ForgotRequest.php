<?php

namespace GrogooRestfier\Modules\Auth\Http\Requests;

use GrogooRestfier\Http\Request\DefaultRequest;

class ForgotRequest extends DefaultRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|exists:users,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __tr('user.email') . '.' . __tr('The field is required'),
            'email.string' => __tr('The field e-mail must be a string'),
            'email.email' => __tr('The field e-mail must be a valid email address'),

        ];
    }
}
