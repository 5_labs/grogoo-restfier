<?php

namespace GrogooRestfier\Modules\Auth\Http\Requests;

use Illuminate\Support\Facades\DB;
use GrogooRestfier\Http\Request\DefaultRequest;

class ResetRequest extends DefaultRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'token' => 'required',
            'email' => 'required|string|exists:users,email',
            'password' => 'required|min:8|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'token.required' => __tr('user.token') . '.' . __tr('The field is required'),

            'email.required' => __tr('user.email') . '.' . __tr('The field is required'),
            'email.string' => __tr('The field e-mail must be a string'),
            'email.email' => __tr('The field e-mail must be a valid email address'),

            'password.required' => __tr('user.password') . '.' . __tr('The field is required'),
            'password.string' => __tr('The field e-mail must be a string'),
            'password.email' => __tr('The field e-mail must be a valid email address'),

        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $token = $this->input('token');
            $email = $this->input('email');

            $resetToken = DB::table('password_reset_tokens')
                ->where('email', $email)
                ->where('token', $token)
                ->where('created_at', '>=', now()->subMinutes(60))
                ->first();

            if (!$resetToken) {
                $validator->errors()->add('token', __('Invalid token or expired'));
            }
        });
    }
}
