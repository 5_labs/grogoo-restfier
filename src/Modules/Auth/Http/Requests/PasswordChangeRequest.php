<?php

namespace GrogooRestfier\Modules\Auth\Http\Requests;

use Illuminate\Support\Facades\DB;
use GrogooRestfier\Http\Request\DefaultRequest;

class PasswordChangeRequest extends DefaultRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'password' => 'required|min:8|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => __tr('The field is required'),
            'password.string' => __tr('The field e-mail must be a string'),
        ];
    }
}
