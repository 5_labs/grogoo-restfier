<?php

namespace GrogooRestfier\Modules\Auth\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use GrogooRestfier\Modules\Audit\Helpers\Audit;
use Illuminate\Support\Facades\Auth;
use App\Modules\Company\Models\Profile;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (env('APP_ENV') === 'local') {
            \Illuminate\Support\Facades\Log::info('---> ' . $request->route()->getName());
        }

        $token = $request->header('Authorization');

        if (!$token) {
            return response()->json(['error' => __tr('Token not provided')], 400);
        }

        try {
            $user = JWTAuth::parseToken()->authenticate();

            // Acrescentar os extras vindos do JWT
            $payload = JWTAuth::getPayload($token);

            $currentProfileId = Profile::select('profiles.id')
                ->join('user_profile', 'user_profile.profile_id', 'profiles.id')
                ->where('company_id', $payload->get('company_id'))
                ->where('user_profile.user_id', $user->id)
                ->first(); // retorna o dado - 404

            if (!$currentProfileId)
                return response()->json(
                    [
                        'error' => [
                            'message' => __tr('User not allowed on company'),
                        ]
                    ],
                    401
                );

            $request->merge([
                'jwt_company_id' => $payload->get('company_id'),
                'jwt_profile_id' => $currentProfileId->id,
                'jwt_user_id' => $user->id,
                'company_id' => $payload->get('company_id')
            ]);


            Audit::setAuditParams(Auth::user()->name, Auth::user()->id);

            return $next($request);
        } catch (JWTException $e) {
            return response()->json(
                [
                    'error' => (getenv('APP_ENV') === 'local' ? [
                        'message' => $e->getMessage(),
                        'token' => $token
                    ] : __tr('Invalid token'))
                ],
                401
            );
        }
    }
}
