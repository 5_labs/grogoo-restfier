<?php

namespace GrogooRestfier\Modules\Auth\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

final class SetHashToApikeyMiddleware
{
    // setar o user_profile em operação
    public function handle(Request $request, Closure $next)
    {
        $currentRoute = Route::currentRouteName();

        if ($currentRoute === 'apikey.store') {
            $request->merge([
                'user_id' => Auth::id(),
                'hash' => hash('sha256', time() . json_encode($request->all())),
            ]);
        }

        return $next($request);
    }
}
