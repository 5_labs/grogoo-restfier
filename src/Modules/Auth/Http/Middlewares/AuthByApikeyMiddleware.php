<?php

namespace GrogooRestfier\Modules\Auth\Http\Middlewares;

use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GrogooRestfier\Helpers\Helper;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Company\Models\User;
use App\Modules\Company\Models\Apikey;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GrogooRestfier\Modules\Auth\Http\Controllers\AuthController;

final class AuthByApikeyMiddleware
{
    // setar o user_profile em operação
    public function handle(Request $request, Closure $next)
    {
        if (
            class_exists('App\Modules\Company\Models\Apikey')
            && strlen((string) $request->header('apikey') ?? '') > 0
        ) {

            try {
                $apikey = Apikey::with('user')
                    ->where('hash', (string) $request->header('apikey'))
                    ->where('valid_at', '>=', Carbon::now()->format('Y-m-d'))
                    ->firstOrFail();

                $token = JWTAuth::fromUser($apikey->user);
                JWTAuth::setToken($token);

                $request->merge(['company_id' => $apikey->company_id]);
                $ret = json_decode((new AuthController())->loginCompanyVerification($apikey->user, $request, $token)->getContent());

                // Atualiza o uso da chave de API
                $apikey->last_usage_date = Carbon::now()->format('Y-m-d H:i:s');
                $apikey->last_usage_ip = Helper::getRequestIp();
                $apikey->usage_hits++;
                $apikey->save();

                // Sobrescrever os cabeçalhos no objeto Request
                $request->headers->replace(
                    array_merge(
                        $request->headers->all(),
                        ['Authorization' => 'Bearer ' . $ret->access_token]
                    )
                );
            } catch (ModelNotFoundException $exc) {
                return response()->json(['error' => __tr('Invalid apikey')], 401);
            }
        }

        return $next($request);
    }
}
