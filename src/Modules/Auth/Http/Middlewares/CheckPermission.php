<?php

namespace GrogooRestfier\Modules\Auth\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Modules\Company\Models\Profile;
use App\Modules\Company\Models\Permission;
use App\Modules\Company\Models\ProfilePermission;
use Symfony\Component\HttpFoundation\Response;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        // Verifica se o usuário está autenticado
        if (!$user = Auth::user()) {
            return response()->json([
                'error' => __tr('Unauthenticated user.')
            ], 401);
        }

        // Obtem a literal da permissão
        $currentRoute = Route::current();
        $parts = explode('.', $currentRoute->getName());
        $verb = array_pop($parts);
        $route = array_pop($parts);
        $group = (explode("\\", $currentRoute->getActionName())[2] ?? 'Company');


        // Verifica se o usuário é um superadmin
        $profile = Profile::find($request->input('jwt_profile_id'));
        if ($profile && $profile->is_root) {
            $this->createIfNotExists($group, $route, $verb);
            return $next($request);
        }

        // Verifica as permissões do usuário
        try {
            // Rotas liberadas, pois será filtrado pelas regras do belongTo
            $free = ['company.index', 'company.show'];
            if (array_search("$route.$verb", $free) !== false) {
                return $next($request);
            }

            // Buscar o permission do perfil na empresa
            ProfilePermission::where('profile_id', '=', $request->input('jwt_profile_id'))
                ->join('permissions', 'permissions.id', '=', 'profile_permissions.permission_id')
                ->select('profile_permissions.id')
                ->where([
                    'permissions.route' => $route,
                    'permissions.verb' => $verb
                ])
                ->firstOrFail();

            return $next($request);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            $this->createIfNotExists($group, $route, $verb);

            // Abortar navegação
            return response()->json(['message' => __tr('You do not have permission to perform this action')], 403);
        }
    }

    public static function createIfNotExists(string $group, string $route, string $verb)
    {
        // Se não existir, criar
        try {
            Permission::where([
                'route' => $route,
                'verb' => $verb
            ])->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            Permission::create([
                'route' => $route,
                'verb' => $verb,
                'group' => $group
            ]);
        }
    }
}
