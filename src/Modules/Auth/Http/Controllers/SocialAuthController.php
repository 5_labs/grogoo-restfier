<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Modules\Company\Http\Resources\CompanyResource;
use App\Modules\Company\Http\Resources\UserResource;
use App\Modules\Company\Models\User;
use App\Modules\Company\Models\UserProfile;
use Exception;
use GrogooRestfier\Modules\Auth\Http\Requests\LoginRequest;
use GrogooRestfier\Modules\Auth\Http\Requests\ResetRequest;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\Response;
use Google\Client as GoogleClient;
use GrogooRestfier\Modules\Auth\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SocialAuthController extends Controller
{

    protected $authController;

    public function __construct(AuthController $authController)
    {
        $this->authController = $authController;
    }

    public function loginGoogle(Request $request)
    {
        // Validar token enviado pelo front-end
        $request->validate([
            'tokenGoogle' => 'required|string'
        ]);

        // Validar token no serviço do Google
        try {
            $googleClient = new GoogleClient(['client_id' => env('services.google_client_id')]);
            $payload = $googleClient->verifyIdToken($request->tokenGoogle);

            $googleUserEmail = $payload['email'];
            $googleUserName = $payload['name'];
            // $googleUserAvatar = $payload['picture'];
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response(__tr('User not authorized.'), Response::HTTP_UNAUTHORIZED);
        }

        /**
         *
         */ /**
         * {
         *     "iss": "https://accounts.google.com",
         *     "azp": "49479144123-ggb15pjjtb7knq8mrs9rdb36k6p6e9g0.apps.googleusercontent.com",
         *     "aud": "49479144123-ggb15pjjtb7knq8mrs9rdb36k6p6e9g0.apps.googleusercontent.com",
         *     "sub": "101119517877819063156",
         *     "hd": "fivelabs.com.br",
         *     "email": "igor.delgado@fivelabs.com.br",
         *     "email_verified": true,
         *     "nbf": 1696443558,
         *     "name": "Igor Delgado",
         *     "picture": "https://lh3.googleusercontent.com/a/ACg8ocJtYWnTTr5vfrkCWT-8yaNsWjFsF8P0gFOO8dv-3VND=s96-c",
         *     "given_name": "Igor",
         *     "family_name": "Delgado",
         *     "locale": "pt-BR",
         *     "iat": 1696443858,
         *     "exp": 1696447458,
         *     "jti": "cabb74cfb9a95b060a5128707acced4042a03eac"
         * }
         */

        /**
         * Check if email used exists
         */
        $request->merge([
            'email' => $googleUserEmail,
            'name' => $googleUserName,
        ]);

        $user = User::where('email', '=', $googleUserEmail)->first();



        if ($user) {
            $request->merge([
                "password" => $user->getAttribute('password')
            ]);

            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);

            if($token === null)
            {
                return response(__tr('User not authorized.'), Response::HTTP_UNAUTHORIZED);
            }
            Log::info('$token in social', [JWTAuth::getToken()]);
            return (new AuthController())->loginCompanyVerification($user, $request, $token);
        } else {
            $registerRequest = new RegisterRequest();
            $registerRequest->merge($request->all());

            (new RegisterUserController())->register($registerRequest);

            $user = User::where('email', $googleUserEmail)->first();

            // Criar token para usuário sem senha
            $userProfile = UserProfile::where(
                'user_id',
                $user->id
            )->with('profile')->first();

            Log::info('$userProfile', [$userProfile]);

            $user->company_id = $userProfile->profile->company_id;
            $user->profile_id = $userProfile->profile_id;
            $user->is_root = $userProfile->profile->is_root;

            $token = JWTAuth::fromUser($user);

            $userResource = UserResource::make($user);

            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'user' => $userResource,
                'company' => CompanyResource::make($userProfile->profile->company)
            ]);
        }
    }


    public function redirectToGoogle()
    {
        $redirectResponse = Socialite::driver('google')
            // ->stateless()
            // ->setScopes(['email', 'profile'])
            // ->redirectUrl(route('auth.google.callback'))
            ->redirect();

        $authorizationUrl = $redirectResponse->getTargetUrl();

        return response()->json(['authorization_url' => $authorizationUrl]);
    }

    public function handleGoogleCallback(Request $request)
    {
        // $mailService = app()->make(MailServiceInterface::class);
        try {
            $googleUser = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            Logger('GOOGLE LOGIN', [$e]);
            return response()->json(['error' => __tr('An error occurred while authenticating with Google. Please try again.')], 400);
        }



        $user = User::first(['email' => $googleUser->getEmail()]);
        if (!$user) {
            // Gerar token fake para validar
            DB::table('password_reset_tokens')->where('email', $googleUser->getEmail())->delete();
            $token = 'Login by Google';
            DB::table('password_reset_tokens')->insert([
                'email' => $googleUser->getEmail(),
                'token' => (string) $token,
                'created_at' => Carbon::now()
            ]);

            // Criar e validar usuario
            $itens['password'] = 'PENDING';
            $user = User::create([
                'password' => 'GOOGLE',
                'name' => $googleUser->getName(),
                'email' => $googleUser->getEmail(),
                'email_verified_at' => Carbon::now()
            ]);

            // Confirmar a conta
            $request = new ResetRequest();
            $request->merge([
                [
                    "email" => $googleUser->getEmail(),
                    "token" => $token,
                    "password" => "PENDING-BY-GOOGLE",
                    "password_confirmation" => "PENDING-BY-GOOGLE"
                ]
            ]);
            $ret = (new RegisterUserController())->confirm($request);
        }

        // Obter a empresa ROOT do usuario
        $userProfiles = UserProfile::with('user', 'profile')
            ->whereHas('user', fn ($query) => $query->where('user.id', $user->id)->where('profile.is_root', 'true'))
            ->get();

        // Cria um novo token com os claims atualizados
        $token = JWTAuth::fromUser($user);

        // Loga o usuario na sua empresa root, sempre
        $requestLoginSocial = (new LoginRequest())
            ->merge(['company_id' => (int) $userProfiles[0]->profile->company_id]);

        return $this->authController->login($requestLoginSocial, $token);
    }
}

