<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Modules\Company\Models\Profile;
use Exception;
use Illuminate\Support\Facades\Cache;

class PermissionsController
{

    private $payload;
    private $companyId;
    private $profileId;
    private $cacheKey;


    public function __construct(?int $profileId = null)
    {
        $token = Request::header('Authorization');
        if (!$token) {
            throw new Exception(__tr('Token not provided'));
        }

        // Acrescentar os extras vindos do JWT
        $this->payload = JWTAuth::getPayload($token);
        $this->companyId = (int) $this->payload->get('company_id');
        $this->profileId = $profileId ?? (int) $this->payload->get('profile_id');
        $this->cacheKey = 'permissions_' . $this->companyId . '_' . $this->profileId;
    }

    public function clearCache()
    {
        Cache::forever($this->cacheKey);
    }

    public function getCacheKey(?int $profileId = null)
    {
        return $this->cacheKey;
    }


    public function getPermissions(?int $profileId = null)
    {
        $companyId = $this->companyId;
        $profileId ??= $this->profileId;

        $list = Cache::remember($this->cacheKey, 0, function () use ($companyId, $profileId) {


            $query = "
                WITH tbl AS (
                    SELECT a.* FROM profiles a
                    WHERE a.company_id = :companyId
                    AND a.id = :profileId
                )
                SELECT 
                    p.id, 
                    p.\"group\", 
                    p.route, 
                    p.verb, 
                    CASE 
                        WHEN (SELECT 1 FROM tbl WHERE is_root = 'true' and id=pp.profile_id) IS NOT NULL THEN true
                        WHEN pp.id IS NOT NULL THEN true 
                        ELSE false 
                    END AS has
                FROM 
                    permissions p
                LEFT JOIN 
                    profile_permissions pp ON p.id = pp.permission_id
                        AND pp.profile_id = (SELECT id FROM tbl)
            ";

            $list = DB::select($query, ['companyId' => (int) $companyId, 'profileId' => (int) $profileId]);
            $list = json_decode(json_encode($list), true);

            // Translate
            foreach ($list as $k => $item) {
                foreach ($item as $key => $val) {
                    if (is_string($val)) {
                        $list[$k][$key . '_label'] = __tr($val);
                    }
                }
            }

            return $list;
        });

        return $list;
    }
}
