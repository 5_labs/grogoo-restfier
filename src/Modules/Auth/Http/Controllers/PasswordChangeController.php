<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Modules\Application\Models\Queue;
use GrogooRestfier\Modules\Auth\Http\Requests\PasswordChangeRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class PasswordChangeController extends Controller
{

    public function handle(PasswordChangeRequest $request)
    {
        // Encontrar o usuário
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json([
                'error' => __tr('Unauthenticated user.')
            ], 401);
        }

        // Atualizar a senha do usuário
        $user->password = Hash::make($request->password);
        $user->save();

        // Enviar email informando alteração de senha
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $user->name,
                'email' => $user->email,
                'subject' => __tr('Password changed'),
                'config' => [
                    'template_id' => env('SENDGRID_CHANGE_PASSWORD', 'DEFAULT'),
                    'template_data' => ['name' => $user->name]
                ]
            ]
        ]);
    }
}
