<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use App\Modules\Company\Models\Profile;
use App\Modules\Subscription\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Modules\Company\Models\UserProfile;
use App\Modules\Company\Http\Resources\UserResource;
use App\Modules\Company\Http\Resources\CompanyResource;
use GrogooRestfier\Modules\Auth\Http\Requests\LoginRequest;
use App\Modules\Company\Models\User;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{



    public function __construct()
    {
    }

    public function loginCompanyVerification(User $user, Request $request = null, string $token = null): JsonResponse
    {


        // Check quantity of companies to user
        if (null !== $request->input('company_id')) {

            $userProfiles = UserProfile::with('user', 'profile')
                ->whereHas('user', fn($query) => $query->where('id', $user->id))
                ->whereHas('profile', fn($query) => $query->where('company_id', $request->input('company_id')))
                ->get();
        } else {
            $userProfiles = UserProfile::with('user', 'profile')
                ->whereHas('user', fn($query) => $query->where('id', $user->id))
                ->get();
        }

        $companiesCount = count($userProfiles);

        // User não esta em nenhuma empresa
        if ($companiesCount === 0) {
            return response()->json(['error' => null !== $request->input('company_id') ? __tr('Company is not enabled to user') : __tr('No company enabled to user')], 401);
            // User tem mais de uma empresa
        } else if ($companiesCount > 1) {
            $companies = [];

            $companies = array_map(function ($item) {
                if ($item->profile) {
                    // se a quantidade de assinaturas para minha empresa for mair que 0 não pode trialar
                    // start_trial -> true quando assinaturas = 0 e user for root da empresa
                    $canTrial = $this->canTrial($item->profile);

                    $item->profile->company->canTrial = $canTrial;
                    $item->profile->company->isRoot = $item->profile->is_root;
                    $companyResource = CompanyResource::make($item->profile->company);

                    return $companyResource;
                }
            }, $userProfiles->all());
            return response()->json([
                'error' => __tr('User has more than one associated company'),
                'content' => $companies
            ], 200);
        } else {
            if ($token) {
                // Parse o token
                $token = JWTAuth::getToken() ?? $token;
                logger('$token after getToken', [$token]);

                $user = JWTAuth::authenticate($token);

                // Invalida o token existente
                JWTAuth::invalidate($token);

                // Atualiza as informações do usuário
                $user->company_id = $userProfiles[0]->profile->company_id;
                $user->profile_id = $userProfiles[0]->profile->id;


                JWTAuth::claims([
                    'company_id' => $userProfiles[0]->profile->company_id,
                    'profile_id' => $userProfiles[0]->profile->id
                ]);
                // Cria um novo token com os claims atualizados
                $token = JWTAuth::fromUser($user);
            } else {
                // usuário tem apenas uma empresa. Adicionar company_id ao token
                $token = Auth::claims([
                    'company_id' => $userProfiles[0]->profile->company_id,
                    'profile_id' => $userProfiles[0]->profile->id
                ])->attempt($request->only('email', 'password'));
            }
        }

        $user->load('file');

        $userResource = UserResource::make($user);
        $userResource['is_root'] = $userProfiles[0]->profile->is_root;

        $userProfiles[0]->profile->company->canTrial = $this->canTrial($userProfiles[0]->profile);
        $userProfiles[0]->profile->company->isRoot = $userProfiles[0]->profile->is_root;

        $subscription = Subscription::with('plan')->where([
            ['company_id', '=', $userProfiles[0]->profile->company->id],
            ['status', '=', 'SUCCESS']
        ])->first();

        $companyResource = CompanyResource::make($userProfiles[0]->profile->company);

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->getExpiresAt(),
            'user' => $userResource,
            'company' => $companyResource,
            'subscription' => $subscription,
            'permissions' => $userProfiles[0]->profile->getPermissionsAllowed()
        ]);

        // return response()->json(['error' => __tr('The provided credentials are incorrect')], 401);
    }

    public function login(LoginRequest $request, string $token = null): JsonResponse
    {
        $user = null;
        if ($token) {
            $user = JWTAuth::parseToken()->authenticate();
        }

        // check if email and password is OK
        if ($token || Auth::attempt($request->only('email', 'password'))) {
            return $this->loginCompanyVerification($user ?? Auth::user(), $request, $token);
        }

        return response()->json(['error' => __tr('The provided credentials are incorrect')], 401);
    }


    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json(['message' => __tr('Logged out successfully')], 200);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['message' => __tr('Failed to logout, please try again')], 500);
        }
    }

    private function getExpiresAt()
    {
        $ttl = config('jwt.ttl'); // Tempo de vida do token em minutos
        return Carbon::now()->addMinutes($ttl)->toDateTimeString();
    }

    private function canTrial(Profile $profile): bool
    {
        return $profile->is_root && $profile->company->subscriptions()->count() == 0;
    }

    public function tokenRenew()
    {
        return response()->json([
            'access_token' => JWTAuth::parseToken()->refresh(),
            'token_type' => 'bearer',
            'expires_in' => $this->getExpiresAt()
        ]);
    }

    /**
     * Get list of companies to user logged
     *
     * @param Request $request
     * @return json
     */
    public function companySwitch(Request $request)
    {
        JWTAuth::parseToken()->authenticate();

        $userProfiles = UserProfile::with('user', 'profile')
            ->whereHas('user', fn($query) => $query->where('id', Auth::user()->id))
            ->get();

        $companies = array_map(function ($item) {
            $canTrial = $this->canTrial($item->profile);
            $item->profile->company->canTrial = $canTrial;
            $item->profile->company->isRoot = $item->profile->is_root;
            return CompanyResource::make($item->profile->company);
        }, $userProfiles->all());
        return response()->json([$companies], 200);
    }

    /**
     * Defined a company from token
     *
     * @param Request $request
     * @param integer $id
     * @return json
     */
    public function companySet(Request $request, int $id)
    {
        JWTAuth::parseToken()->authenticate();
        $token = $request->header('Authorization');
        $requestLogin = (new LoginRequest())
            ->merge(['company_id' => (int) $id]);
        return $this->login($requestLogin, $token);
    }
}
