<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Modules\Application\Models\Queue;
use GrogooRestfier\Modules\Auth\Http\Requests\ResetRequest;
use GrogooRestfier\Modules\Auth\Http\Requests\ForgotRequest;

class PasswordResetLinkController extends Controller
{

    public function __construct()
    {
    }
    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function forgot(ForgotRequest $request): JsonResponse
    {
        $email = $request->input('email');
        $status = 400;
        $message = '';

        $user = User::where('email', $email)->firstOrFail();

        $passwordResetToken = DB::table('password_reset_tokens')->where('email', $email)->first();
        $token = rand(111111, 999999);

        if (!$passwordResetToken) {
            DB::table('password_reset_tokens')->insert([
                'email' => $email,
                'token' => (string) $token,
                'created_at' => Carbon::now()
            ]);
        } else {
            // verifico se o token que tava no banco tava expirado
            $createdAtCarbon = Carbon::parse($passwordResetToken->created_at);
            if ($createdAtCarbon->addMinutes(10)->lessThan(Carbon::now())) {
                DB::table('password_reset_tokens')->where('email', $email)->delete();
                DB::table('password_reset_tokens')->insert([
                    'email' => $email,
                    'token' => (string) $token,
                    'created_at' => Carbon::now()
                ]);
            } else {
                $token = $passwordResetToken->token;
                DB::table('password_reset_tokens')->where('email', $email)->update([
                    // 'token' => (string) $token
                    'email' => $email,
                    'token' => $token,
                    'created_at' => Carbon::now(),
                ]);
            }
        }


        // Enviar email
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $user->name,
                'email' => $user->email,
                'subject' => __tr('Password recovery request'),
                'config' => [
                    'template_id' => env('SENDGRID_FORGOT_PASSWORD'),
                    'template_data' => ['name' => $user->name, 'code' => $token]
                ]
            ]
        ]);

        return response()->json(['message' => __tr('Token for password recovery sent. Check email.')]);
    }

    public function reset(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();

        // Atualizar a senha do usuário
        $user->password = Hash::make($request->password);
        $user->save();

        // Remover o token utilizado
        DB::table('password_reset_tokens')->where('email', $request->email)->delete();

        // Enviar email informando alteração de senha
        Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $user->name,
                'email' => $user->email,
                'subject' => __tr('Password changed'),
                'config' => [
                    'template_id' => env('SENDGRID_CHANGE_PASSWORD'),
                    'template_data' => ['name' => $user->name]
                ]
            ]
        ]);

        return response()->json(['message' => __tr('Password successfully updated')], 200);
    }
}
