<?php

namespace GrogooRestfier\Modules\Auth\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modules\Company\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Modules\Company\Models\Company;
use App\Modules\Company\Models\Profile;
use App\Modules\Application\Models\Queue;
use App\Modules\Company\Models\Invite;
use App\Modules\Company\Models\UserProfile;
use GrogooRestfier\Modules\Auth\Events\CompanyCreateEvent;
use GrogooRestfier\Modules\Auth\Http\Requests\ResetRequest;
use GrogooRestfier\Modules\Auth\Http\Requests\RegisterRequest;
use GrogooRestfier\Services\SalesflowService;
use Illuminate\Support\Facades\Crypt;


class RegisterUserController extends Controller
{


    public function __construct()
    {
    }

    public function sendToken(string $email, string $name, string $subject = 'Confirm your account')
    {
        // Enviar email do confirmação
        DB::table('password_reset_tokens')->where('email', $email)->delete();
        $token = rand(100000, 999999);
        DB::table('password_reset_tokens')->insert([
            'email' => $email,
            'token' => (string) $token,
            'created_at' => Carbon::now()
        ]);

        // notification to company
        return Queue::create([
            'type' => 'sendmail',
            'data' => [
                'name' => $name,
                'email' => $email,
                'subject' => __tr($subject),
                'config' => [
                    'template_id' => env('SENDGRID_SIGNUP_CODE'),
                    'template_data' => ['name' => $name, 'code' => $token]
                ]
            ]
        ]);
    }

    // registra um usuario
    public function register(RegisterRequest $request)
    {
        $itens = $request->only('name', 'email');

        $itens['email'] = mb_strtolower($itens['email']);

        $userAtBase = User::where(
            [
                ['email', '=', $itens['email']]
            ]
        )->first();

        if ($userAtBase) {
            if ($userAtBase->email_verified_at === null) {
                $result = $this->sendToken($userAtBase->email, $userAtBase->name);
                return response()->json(
                    $result->id > 0
                        ? ['message' => __tr('User already exists! Check your e-mail to validate your account')]
                        : ['error' => __tr('Error on send code')],
                    $result->id > 0 ? 200 : 400
                );
            }
            return response()->json([
                'error' => __tr('Account has already been verified')
            ], 422);
        }

        $ret = DB::transaction(function () use ($request, $itens) {
            $itens['password'] = 'PENDING';

            $user = User::create($itens);

            SalesflowService::conversion($user, $request->headers->get("origin") ?? " ", "Novo cadastro");

            if ($request->input('invitation') !== null) {
                $profileIdFromInvite = Crypt::decrypt($request->input('invitation'));

                $inviteQuery = Invite::where('email', $itens['email'])
                    ->where('profile_id', $profileIdFromInvite);

                if ($inviteQuery->count() === 0) {
                    throw new \Exception(__tr("Email to register must be the same in the invite"), 409);
                }

                $invite = $inviteQuery->first();

                $invite->update([
                    'status' => 'ACCEPTED'
                ]);

                $userProfile = new UserProfile();
                $userProfile->fill([
                    'user_id' => $user->id,
                    'profile_id' => $profileIdFromInvite,
                ]);

                if (property_exists($userProfile, 'invite_id')) {
                    $userProfile->invite_id = $invite->id;
                }
                $userProfile->save();
            } else {
                $invites = Invite::where('email', $itens['email'])
                    ->where('status', 'PENDING')
                    ->get();

                if (!$invites->isEmpty()) {
                    foreach ($invites as $invite) {
                        $invite->update(['status' => 'ACCEPTED']);
                        $userProfile = new UserProfile();
                        $userProfile->fill([
                            'user_id' => $user->id,
                            'profile_id' => $invite->profile_id,
                        ]);
                        if (property_exists($userProfile, 'invite_id')) {
                            $userProfile->invite_id = $invite->id;
                        }
                        $userProfile->save();
                    }
                }
            }

            $company = $this->createUserCompany($user);

            // Criar profile Root
            $this->createProfileRoot($user, $company);

            return $this->sendToken($user->email, $user->name);
        });

        return response()->json(
            $ret->id > 0
                ? ['message' => __tr('User created! Check your e-mail to validate your account')]
                : ['error' => __tr('Error on send code')],
            $ret->id > 0 ? 200 : 400
        );
    }

    public function resendToken(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        $email = mb_strtolower($request->email);

        $user = User::where('email', '=', (string) $email)
            ->firstOrFail();

        if (!$user) {
            throw new \InvalidArgumentException(__tr('Account not found'), 404);
        }

        if ($user->email_verified_at !== null) {
            throw new \InvalidArgumentException(__tr('Account has already been verified'), 422);
        }

        $ret = $this->sendToken($user->email, $user->name);

        return response()->json(
            $ret->id > 0
                ? ['message' => __tr('Token resent')]
                : ['error' => __tr('Error on send code')],
            $ret->id > 0 ? 200 : 400
        );
    }

    // valida o código enviado por email
    public function confirm(ResetRequest $request)
    {
        // Encontrar o usuário com o email fornecido
        $user = User::where('email', $request->email)->firstOrFail();
        $company = Company::where('email', $request->email)->firstOrFail();

        DB::transaction(function () use ($request, $user, $company) {

            // Atualizar a senha do usuário
            $user->password = Hash::make($request->password);
            $user->email_verified_at = Carbon::now();
            $user->save();

            // Remover o token utilizado
            DB::table('password_reset_tokens')->where('email', $request->email)->delete();

            $url = env('FRONTEND_URL') . '/#/login';
            Queue::create([
                'type' => 'sendmail',
                'data' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'subject' => __tr('Account actived'),
                    'config' => [
                        'template_id' => env('SENDGRID_SIGNUP_ACCOUNT_CREATED'),
                        'template_data' => ['name' => $user->name, 'plan_name' => 'FREE', 'url' => $url]
                    ]
                ]
            ]);
        });

        return response()->json(['message' => __tr('Account activated!')], 200);
    }

    // Cria a empresa e associa ao usuario
    public function createUserCompany(User $user): Company
    {
        $company = Company::create([
            'name' => $user->name,
            'email' => $user->email
        ]);

        return $company;
    }

    // Cria o profile administrador com permissão root
    public function createProfileRoot(User $user, Company $company)
    {

        $profile = Profile::create([
            'name' => 'Testes Unitários',
            'company_id' => $company->id
        ])
            ->forceFill(['is_root' => true]);
        $profile->save();
        $profile->refresh();

        UserProfile::create([
            'user_id' => $user->id,
            'profile_id' => $profile->id
        ]);

        return $profile;
    }
}
