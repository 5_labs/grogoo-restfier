<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('path', 200);
            $table->string('filename', 200);
            $table->string('description', 250)->nullable();
            $table->string('extension', 5)->nullable();
            $table->string('mime', 50)->nullable();
            $table->integer('size', false, true);
            $table->enum('classify', ['PUBLIC', 'PRIVATE', 'INTERNAL'])->default('PRIVATE')->nullable();
            $table->enum('storage', ['local', 'public', 's3', 'gcs'])->default('local')->nullable();
            $table->string('recognition', 50)->nullable();
            $table->text('content')->nullable();
            $table->foreignId('company_id')->constrained('companies')->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['path', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
