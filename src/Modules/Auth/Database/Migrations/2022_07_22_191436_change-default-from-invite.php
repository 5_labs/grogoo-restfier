<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->string('status')->default('ACCEPTED')->change();
        });
    }

    public function down()
    {
        Schema::table('invites', function (Blueprint $table) {
            $table->enum('status', ['PENDING', 'ACCEPTED', 'REJECTED', 'EXPIRED'])->default('PENDING');
        });
    }
};
