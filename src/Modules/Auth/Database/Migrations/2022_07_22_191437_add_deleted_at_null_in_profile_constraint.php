<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropUnique('profiles_name_company_id_unique');
            $table->unique(['name', 'company_id', 'deleted_at'], null, null, 'profiles_name_company_id_unique');
        });
    }
    
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropUnique('profiles_name_company_id_unique');
            $table->unique(['name', 'company_id'], null, null, 'profiles_name_company_id_unique');
        });
    }
};
