<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name', '100')->comments('Name');
            $table->text('description')->nullable();
            $table->boolean('is_root')->default(false);
            $table->foreignId('company_id')->constrained('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['name', 'company_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
