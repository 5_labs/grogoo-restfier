<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone_principal', 50)->nullable();
            $table->string('phone_secondary', 50)->nullable();
            $table->string('phone_other', 50)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('gender')->nullable();
            $table->string('tax_code')->nullable();
            $table->string('occupation')->nullable();
            $table->string('nationality')->nullable();
            $table->string('marital_status')->nullable();
            $table->text('notes')->nullable();
            $table->boolean('active')->default(true);
            $table->string('contact_preferences')->nullable();
            $table->string('referral_source')->nullable();

            $table->unsignedBigInteger('address_id')->nullable();
            $table->foreign('address_id')->references('id')->on('addresses')->onUpdate('cascade')->onDelete('set null');

            $table->foreignId('company_id')->constrained('companies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
