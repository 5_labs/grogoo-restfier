<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('profile_permissions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('permission_id')->constrained('permissions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('profile_id')->constrained('profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['permission_id', 'profile_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('profile_permissions');
    }
};
