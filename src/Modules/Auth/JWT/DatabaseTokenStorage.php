<?php


namespace GrogooRestfier\Modules\Auth\JWT;

use GrogooRestfier\Modules\Auth\Models\JwtToken;
use Tymon\JWTAuth\Contracts\Providers\Storage;

class DatabaseTokenStorage implements Storage
{
    public function add($key, $value, $minutes)
    {
        JwtToken::create(['token' => $key]);
    }

    public function forever($key, $value)
    {
        $this->add($key, $value, 0);
    }

    public function get($key)
    {
        $token = JwtToken::where('token', $key)->first();
        return $token ? ['value' => $token->token, 'valid_until' => null] : null;
    }

    public function destroy($key)
    {
        JwtToken::where('token', $key)->delete();
    }

    public function flush()
    {
        JwtToken::truncate();
    }

    public function getTTL()
    {
        return config('jwt.ttl');
    }
}
