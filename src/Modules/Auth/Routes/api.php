<?php

use Illuminate\Support\Facades\Route;
use GrogooRestfier\Modules\Auth\Http\Controllers\AuthController;
use GrogooRestfier\Modules\Auth\Http\Controllers\InviteReceivedController;
use GrogooRestfier\Modules\Auth\Http\Controllers\InviteSentController;
use GrogooRestfier\Modules\Auth\Http\Controllers\PasswordChangeController;
use GrogooRestfier\Modules\Auth\Http\Controllers\PasswordResetLinkController;
use GrogooRestfier\Modules\Auth\Http\Controllers\RegisterUserController;
use GrogooRestfier\Modules\Auth\Http\Controllers\SocialAuthController;

$limitSeconds = env('APP_ENV') === 'testing' ? 600 : 60;

// Auth modules
Route::middleware([
    "throttle:{$limitSeconds},1",
    \GrogooRestfier\Http\Middleware\ResponseMiddleware::class
])->group(function () {

    Route::get('healthcheck', fn () => response()->json('healthy', 200))->name('app.healthcheck');

    // Login
    Route::prefix('auth')
        ->group(function () {
            Route::post('login', [AuthController::class, 'login'])->name('auth.login');
            Route::post('logout', [AuthController::class, 'logout'])->name('auth.logout');
            Route::post('forgot-password', [PasswordResetLinkController::class, 'forgot'])->name('auth.forgot');
            Route::post('reset-password', [PasswordResetLinkController::class, 'reset'])->name('auth.reset');

            // Google
            Route::post('google', [SocialAuthController::class, 'loginGoogle'])->name('auth.google');
            Route::get('google/callback', [SocialAuthController::class, 'handleGoogleCallback'])->name('auth.google.callback');

            // Renew token
            Route::group(
                ['middleware' => ['Auth.check.token']],
                function () {
                    Route::post('renew', [AuthController::class, 'tokenRenew'])->name('auth.renew');
                    Route::get('switch', [AuthController::class, 'companySwitch'])->name('auth.switch');
                    Route::put('switch/{id}', [AuthController::class, 'companySet'])->name('auth.switchSet');
                    Route::post('change-password', [PasswordChangeController::class, 'handle'])->name('auth.change-password');
                }
            );
        });


    // Create account
    Route::prefix('signup')
        ->group(function () {
            Route::post('', [RegisterUserController::class, 'register'])->name('signup.register');
            Route::post('resend', [RegisterUserController::class, 'resendToken'])->name('signup.resend');
            Route::post('confirm', [RegisterUserController::class, 'confirm'])->name('signup.confirm');
        });
});
