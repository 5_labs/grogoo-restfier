<?php

namespace GrogooRestfier\Modules\Auth\Providers;

use DateTime;
use Illuminate\Support\Arr;
use League\Flysystem\Filesystem;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Config;
use League\Flysystem\GoogleCloudStorage\GoogleCloudStorageAdapter;

class GoogleCloudStorageServiceProvider extends ServiceProvider
{

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $bucket = env('GOOGLE_CLOUD_STORAGE_BUCKET', '');

        if (strlen((string) ($bucket ?? '')) > 1) {

            $factory = $this->app->make('filesystem');

            $factory->extend('gcs', function ($app, $config) {

                $storageClient = $this->createClient($config);

                $bucket = $storageClient->bucket($config['bucket']);

                $pathPrefix = Arr::get($config, 'path_prefix');

                $adapter = new GoogleCloudStorageAdapter($bucket, $pathPrefix);

                $config = Arr::only($config, ['visibility', 'disable_asserts', 'url', 'throw']);

                return new FilesystemAdapter(
                    new Filesystem($adapter, count($config) > 0 ? $config : null),
                    $adapter,
                    $config
                );
            });

            Storage::disk('gcs')->buildTemporaryUrlsUsing(
                function (string $path, DateTime $expiration, array $options) {
                    return Storage::disk('gcs')->getAdapter()->temporaryUrl($path, $expiration, new Config());
                }
            );
        }
    }


    /**
     * Create a new StorageClient
     *
     * @param mixed $config
     * @return \Google\Cloud\Storage\StorageClient
     */
    private function createClient($config)
    {
        $keyFile = Arr::get($config, 'key_file');

        if (is_string($keyFile) && file_exists($keyFile)) {
            return new StorageClient([
                'projectId' => $config['project_id'],
                'keyFilePath' => $keyFile,
            ]);
        }

        if (!is_array($keyFile)) {
            $keyFile = [];
        }

        return new StorageClient([
            'projectId' => $config['project_id'],
            'keyFile' => array_merge(["project_id" => $config['project_id']], $keyFile)
        ]);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        //
    }
}
