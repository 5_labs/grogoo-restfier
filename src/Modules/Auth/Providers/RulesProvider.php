<?php

namespace GrogooRestfier\Modules\Auth\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class RulesProvider extends ServiceProvider
{

    public function boot()
    {
        Validator::extend('cpfcnpj', '\GrogooRestfier\Rules\CPFOrCNPJRule@validate');

        Validator::extend('credit_card_validation', '\GrogooRestfier\Rules\CreditCardValidation@validate');
    }
}
