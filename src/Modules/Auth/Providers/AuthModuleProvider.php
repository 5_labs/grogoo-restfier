<?php

namespace GrogooRestfier\Modules\Auth\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AuthModuleProvider extends ServiceProvider
{

    public function boot()
    {
        // OK!
        Route::prefix('api')->group(function () {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');
        });

        // OK!
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        // $this->publishes([
        //     __DIR__ . '/../Config/company.php' => config_path('company.php'),
        // ], 'company-config');
    }
}
