# 1. Iniciação

<details>
    <summary>Ver itens</summary>

</details>

---

# 2. Planejamento

<details><summary>Ver itens</summary>

## Coletar requisitos e definir escopo

_Esta entrega trás a lista de requisitos funcionais e não funcionais do produto._

**Referência do conceito:** [Link para Requisito Não Funcional](https://pt.wikipedia.org/wiki/Requisito_n%C3%A3o_funcional)

#### Execução

- [ ] Identificar requisitos funcionais
- [ ] Identificar requisitos não funcionais
- [ ] Identificar fronteiras do produto
- [ ] Atualizar documento de projeto
- [ ] Aprovação de interessados

#### Entrega

- [ ] Atualização do TAP com o documento de requisitos

---

## Criar diagramas de caso de uso

_Esta entrega trás a lista de requisitos funcionais, principais entregas e a forma de ações de atores. A saída será uma lista de ações e de atores do sistema._

#### Execução

- [ ] Definir atores
- [ ] Definir ações
- [ ] Associar ações a atores
- [ ] Aprovação de interessados

#### Entrega

- [ ] Diagrama de caso de uso

---

## Planejar testes de qualidade

_Com base nos requisitos, descrição e definições anteriores, propor uma estrutura de validação de sucesso da entrega do projeto e planejamento de riscos. Será utilizado nos testes de qualidade._

#### Execução

- [ ] Definir parâmetros e ações de testes, com margem de acerto e erro
- [ ] Definir riscos conhecidos
- [ ] Planejar resposta a risco
- [ ] Definir equipe de teste inicial

#### Entrega

- [ ] Lista de ações de testes com resultados esperados
- [ ] Relação de riscos conhecidos e a resposta planejada
- [ ] Atualização do TAP

---

## Planejar aquisições

_Com base nas necessidades de projeto apontadas, como recursos humanos e outros, planejar a forma de aquisição._

_Uma simples alocação de um recurso humano ao projeto pode ser considerada uma aquisição, ou seja, aquisição não é comprar ou contratar somente, mas alocar os recursos necessários para a execução do projeto._

#### Execução

- [ ] Criar lista de aquisições com data de término
- [ ] Elencar possíveis fornecedores

#### Entrega

- [ ] Lista de aquisições
- [ ] Atualizar TAP

---

## Protótipo de telas

_Entendimento do protótipo de telas proposta para a etapa_

#### Execução

- [ ] Identificar telas
- [ ] Relacionar com requisitos
- [ ] Relacionar com casos de uso

#### Entrega

- [ ] Telas identificadas
- [ ] Requisitos atendidos
- [ ] Identificação dos principais desafios

---

## Criação de diagramas críticos

_Definir dentre os casos de uso aqueles que necessitam de algum detalhamento maior para entendimento da solicitação._

#### Execução

- [ ] Definir casos de uso críticos
- [ ] Detalhar fluxo do casos de uso
- [ ] Criar diagrama de sequência
- [ ] Criar diagrama de estado (se necessário)

#### Entrega

- [ ] Detalhamento dos processos
- [ ] Entendimento pela equipe técnica de desenvolvimento do requisito
- [ ] Atualização do TAP

---

## Definir as atividades

_Criar a lista de atividades a serem cumpridas para entrega desta fase, gerando o Cronograma de Tarefas._

#### Execução

- [ ] Listar as atividades (backend, frontend, infra, testes, integrações ... )
- [ ] Sequenciar as atividades
- [ ] Definir precedência de tarefas (se possível ou aplicável)
- [ ] Definir recurso
- [ ] Estimar esforço de execução
- [ ] Criação das tarefas

#### Controle

- [ ] Validar necessidade de alocação de recursos extras

#### Entrega

- [ ] Definição do prazo e orçamento
- [ ] Notificação dos envolvidos
- [ ] Cronograma de atividades
- [ ] Atualizar TAP: Prazo, Orçamento

---

## Checkpoint: Aprovação cronograma e custos

_Reunião com os interessados para aprovação e início da etapa._

#### Execução

- [ ] Agendar horário com interessados
- [ ] Expor cronograma com prazo e orçamento previstos
- [ ] Expor possíveis riscos identificados
- [ ] Expor necessidades de aquisição identificadas
- [ ] Validar Users Story Maps

#### Entrega

- [ ] Aprovação e início da execução
- [ ] Atualização do TAP

</details>

---

# 3. Execução

<details>
    <summary>Ver itens</summary>

## Conduzir as aquisições

_Executar as aquisições definidas_

### Execução

- [ ] Realizar aquisições conforme planejado
- [ ] Gerenciar contratos com fornecedores
- [ ] Monitorar o desempenho dos fornecedores
- [ ] Garantir entrega e qualidade dos produtos ou serviços adquiridos

### Entrega

- [ ] Relatórios de status das aquisições
- [ ] Registros de contratos e documentação relacionada
- [ ] Aprovação de entregas de fornecedores
- [ ] Atualização do TAP

---

## Configurar repositório de códigos e tarefas

_Configurar versionamento, repositório e cadastro dos envolvidos no software de gestão de atividades_

### Execução

- [ ] Criar repositório de código no Gitlab
- [ ] Configurar permissões de acesso
- [ ] Cadastrar envolvidos no projeto no Gitlab
- [ ] Definir estrutura de pastas e categorias para tarefas
- [ ] Integrar ferramentas de gestão de tarefas ao Gitlab (se aplicável)

### Entrega

- [ ] Repositório de código configurado
- [ ] Lista de envolvidos cadastrados no Gitlab
- [ ] Estrutura de pastas e categorias definida
- [ ] Integração de ferramentas (se aplicável)
- [ ] Atualização do TAP

---

## Modelagem de banco de dados

_Tarefas de modelagem de banco de dados._

### Execução

- [ ] Identificar requisitos de banco de dados
- [ ] Definir modelo conceitual
- [ ] Criar modelo lógico
- [ ] Desenvolver modelo físico
- [ ] Realizar otimizações de desempenho (se necessário)

### Entrega

- [ ] Documentação dos requisitos de banco de dados
- [ ] Modelo conceitual
- [ ] Modelo lógico
- [ ] Modelo físico
- [ ] Relatórios de otimizações (se aplicável)
- [ ] DER
- [ ] Atualização do TAP

## Tarefas definidas

_Execução das tarefas definidas conforme cronograma_

### Execução

- [ ] Execução das tarefas de backend
- [ ] Execução das tarefas de frontend
- [ ] Execução das tarefas de infra

### Gestão

- [ ] Controle de prazos
- [ ] Controle de riscos
- [ ] Controle da qualidade

### Entrega

- [ ] Versão disponível para homologação

</details>

---

# 4. Gestão

<details>
    <summary>Ver itens</summary>
    
</details>

---

# 5. Entrega

<details>
    <summary>Ver itens</summary>
    
</details>
